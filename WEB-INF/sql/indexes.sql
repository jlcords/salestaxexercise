create index IX_ACD2AC17 on Order_Category (companyId, groupId);
create index IX_DCB3B135 on Order_Category (groupId);
create index IX_DBFE0CFF on Order_Category (uuid_);
create index IX_347031C9 on Order_Category (uuid_, companyId);
create unique index IX_2ABC968B on Order_Category (uuid_, groupId);

create index IX_A4E2883B on Order_FinalItem (orderId);
create index IX_E3535476 on Order_FinalItem (uuid_);
create index IX_8E6C69F2 on Order_FinalItem (uuid_, companyId);
create unique index IX_7F8AC4F4 on Order_FinalItem (uuid_, groupId);

create index IX_279365C6 on Order_InventoryItem (groupId);
create index IX_BDB3B1D0 on Order_InventoryItem (uuid_);
create index IX_B5941058 on Order_InventoryItem (uuid_, companyId);
create unique index IX_E114CCDA on Order_InventoryItem (uuid_, groupId);

create index IX_3B5FC9D on Order_Order (userId);
create index IX_A8E3ECF1 on Order_Order (uuid_);
create index IX_DB674697 on Order_Order (uuid_, companyId);
create unique index IX_F66E86D9 on Order_Order (uuid_, groupId);

create unique index IX_662A1738 on Order_TaxDuty (companyId, groupId);
create unique index IX_D6AFF0B4 on Order_TaxDuty (groupId);
create index IX_B2B2203E on Order_TaxDuty (uuid_);
create index IX_F939F12A on Order_TaxDuty (uuid_, companyId);
create unique index IX_3000BA2C on Order_TaxDuty (uuid_, groupId);