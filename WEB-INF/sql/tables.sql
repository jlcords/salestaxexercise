create table Order_Category (
	uuid_ VARCHAR(75) null,
	categoryId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	taxable BOOLEAN,
	name VARCHAR(75) null
);

create table Order_FinalItem (
	uuid_ VARCHAR(75) null,
	finalItemId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userId LONG,
	orderId LONG,
	categoryId LONG,
	name VARCHAR(75) null,
	basePrice DOUBLE,
	totalPrice DOUBLE,
	quantity INTEGER,
	imported BOOLEAN,
	tax DOUBLE,
	duty DOUBLE
);

create table Order_InventoryItem (
	uuid_ VARCHAR(75) null,
	inventoryItemId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userId LONG,
	categoryId LONG,
	name VARCHAR(75) null,
	basePrice DOUBLE,
	quantity INTEGER,
	imported BOOLEAN
);

create table Order_Order (
	uuid_ VARCHAR(75) null,
	orderId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	userId LONG,
	confirmed BOOLEAN,
	createDate DATE null
);

create table Order_TaxDuty (
	uuid_ VARCHAR(75) null,
	taxDutyId LONG not null primary key,
	companyId LONG,
	groupId LONG,
	tax INTEGER,
	duty INTEGER,
	round INTEGER
);