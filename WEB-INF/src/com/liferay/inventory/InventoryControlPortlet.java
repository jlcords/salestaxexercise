package com.liferay.inventory;


import com.liferay.order.service.InventoryItemLocalServiceUtil;
import com.liferay.order.service.TaxDutyLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.util.bridges.mvc.MVCPortlet;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

/**
 * Portlet implementation class OrderPortlet
 */
public class InventoryControlPortlet extends MVCPortlet {
 
	/**
	 * Creates and adds InventoryItem to database, calls LocalServiceUtil
	 * @param actionRequest
	 * @param actionResponse
	 * @return InventoryItem
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public void addInventoryItem(ActionRequest actionRequest, ActionResponse actionResponse){
		long companyId = ParamUtil.getLong(actionRequest, "companyId");
		long groupId = ParamUtil.getLong(actionRequest, "groupId");
		long userId = ParamUtil.getLong(actionRequest, "userId");
		String inventoryItemName = ParamUtil.getString(actionRequest, "name");
		int quantity = ParamUtil.getInteger(actionRequest, "quantity");
		double basePrice = ParamUtil.getDouble(actionRequest, "basePrice");
		long categoryId = ParamUtil.getLong(actionRequest, "categoryId");
		boolean imported = ParamUtil.getBoolean(actionRequest, "imported");
		
		try {
			InventoryItemLocalServiceUtil.addInventoryItem(companyId, groupId, 
					userId, inventoryItemName, quantity, basePrice, categoryId, imported);
		} catch (SystemException e) {
			_log.error("Unable to create Inventory Item", e);
		}
		
		actionResponse.setRenderParameter("mvcPath",
				"/html/inventorycontrol/view.jsp");
	}
	
	/**
	 * Deletes InventoryItem from database, calls LocalServiceUtil
	 * @param actionRequest
	 * @param actionResponse
	 * @return InventoryItem
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public void deleteInventoryItem(ActionRequest actionRequest, ActionResponse actionResponse){
		
		long inventoryItemId = ParamUtil.getLong(actionRequest, "inventoryItemId");
		
		try {
			InventoryItemLocalServiceUtil.deleteInventoryItem(inventoryItemId);
		} catch (PortalException e) {
			_log.error("Unable to delete Inventory Item " + inventoryItemId, e);
		} catch (SystemException e) {
			_log.error("Unable to delete Inventory Item " + inventoryItemId, e);
		}
	}
	
	/**
	 * Updates InventoryItem in database, calls LocalServiceUtil
	 * @param actionRequest
	 * @param actionResponse
	 * @return InventoryItem
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public void updateInventoryItem(ActionRequest actionRequest, ActionResponse actionResponse){
		long companyId = ParamUtil.getLong(actionRequest, "companyId");
		long groupId = ParamUtil.getLong(actionRequest, "groupId");
		long userId = ParamUtil.getLong(actionRequest, "userId");
		long inventoryItemId = ParamUtil.getLong(actionRequest, "inventoryItemId");
		String inventoryItemName = ParamUtil.getString(actionRequest, "name");
		int quantity = ParamUtil.getInteger(actionRequest, "quantity");
		double basePrice = ParamUtil.getDouble(actionRequest, "basePrice");
		long categoryId = ParamUtil.getLong(actionRequest, "categoryId");
		boolean imported = ParamUtil.getBoolean(actionRequest, "imported");
		
		try {
			InventoryItemLocalServiceUtil.updateInventoryItem(companyId, groupId, 
					userId, inventoryItemId, inventoryItemName, quantity, basePrice, categoryId, imported);
		} catch (SystemException e) {
			_log.error("Unable to update Inventory Item " + inventoryItemId, e);
		}
		
		actionResponse.setRenderParameter("mvcPath",
				"/html/inventorycontrol/view.jsp");
	}
	
	private static Log _log = LogFactoryUtil.getLog(InventoryControlPortlet.class);
	
}
