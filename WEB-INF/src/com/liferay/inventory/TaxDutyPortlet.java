package com.liferay.inventory;

import com.liferay.order.service.TaxDutyLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.util.bridges.mvc.MVCPortlet;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import com.liferay.order.model.TaxDuty;

/**
 * Portlet implementation class TaxDuty
 */
public class TaxDutyPortlet extends MVCPortlet {
 
	/**
	 * Update TaxDuty item in database
	 * @param actionRequest
	 * @param actionResponse
	 * @throws SystemException
	 */
	public void updateTaxDuty(ActionRequest actionRequest, ActionResponse actionResponse) throws SystemException{
		long companyId = ParamUtil.getLong(actionRequest, "companyId");
		long groupId = ParamUtil.getLong(actionRequest, "groupId");
		long taxDutyId = ParamUtil.getLong(actionRequest, "taxDutyId");
		int tax = ParamUtil.getInteger(actionRequest, "tax-percent");
		int duty = ParamUtil.getInteger(actionRequest, "duty-percent");
		int round = ParamUtil.getInteger(actionRequest, "round");
		
		TaxDutyLocalServiceUtil.updateTaxDuty(companyId, groupId, taxDutyId, tax, duty, round);
	}

}
