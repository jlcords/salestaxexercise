package com.liferay.inventory;

import com.liferay.order.service.CategoryLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

/**
 * Portlet implementation class CategoryControlPortlet
 */
public class CategoryControlPortlet extends MVCPortlet {
 
	/**
	 * Creates and adds Category to database, calls LocalServiceUtil
	 * @param actionRequest
	 * @param actionResponse
	 * @return Category
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public void addCategory(ActionRequest actionRequest, ActionResponse actionResponse){
		long companyId = ParamUtil.getLong(actionRequest, "companyId");
		long groupId = ParamUtil.getLong(actionRequest, "groupId");
		String categoryName = ParamUtil.getString(actionRequest, "name");
		boolean taxable = ParamUtil.getBoolean(actionRequest, "taxable");
		
		try {
			CategoryLocalServiceUtil.addCategory(
					companyId, groupId, categoryName, taxable);
		} catch (SystemException e) {
			_log.error("Unable to create Category", e);
		}
		
		actionResponse.setRenderParameter("mvcPath",
				"/html/categorycontrol/view.jsp");
	}
	
	/**
	 * Deletes Category from database, calls LocalServiceUtil
	 * @param actionRequest
	 * @param actionResponse
	 * @return Category
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public void deleteCategory(ActionRequest actionRequest, ActionResponse actionResponse){
		
		long categoryId = ParamUtil.getLong(actionRequest, "categoryId");
		
		try {
			CategoryLocalServiceUtil.deleteCategory(categoryId);
		} catch (PortalException e) {
			_log.error("Unable to delete Category " + categoryId, e);
		} catch (SystemException e) {
			_log.error("Unable to delete Category " + categoryId, e);
		}
	}
	
	/**
	 * Updates Category in database, calls LocalServiceUtil
	 * @param actionRequest
	 * @param actionResponse
	 * @return Category
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public void updateCategory(ActionRequest actionRequest, ActionResponse actionResponse){
		long companyId = ParamUtil.getLong(actionRequest, "companyId");
		long groupId = ParamUtil.getLong(actionRequest, "groupId");
		long categoryId = ParamUtil.getLong(actionRequest, "categoryId");
		String categoryName = ParamUtil.getString(actionRequest, "name");
		boolean taxable = ParamUtil.getBoolean(actionRequest, "taxable");
		
		try {
			CategoryLocalServiceUtil.updateCategory(
					companyId, groupId, categoryId, categoryName, taxable);
		} catch (SystemException e) {
			_log.error("Unable to update Category " + categoryId, e);
		}
		
		actionResponse.setRenderParameter("mvcPath",
				"/html/categorycontrol/view.jsp");
	}


	private static Log _log = LogFactoryUtil.getLog(CategoryControlPortlet.class);
}