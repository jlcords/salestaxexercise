package com.liferay.order;

import com.liferay.order.model.InventoryItem;
import com.liferay.order.service.CategoryLocalServiceUtil;
import com.liferay.order.service.FinalItemLocalServiceUtil;
import com.liferay.order.service.InventoryItemLocalServiceUtil;
import com.liferay.order.service.OrderLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

/**
 * Portlet implementation class OrderPortlet
 */
public class OrderPortlet extends MVCPortlet {
 
	
	/**
	 * Adds FinalItem to database
	 * @param actionRequest
	 * @param actionResponse
	 */
	public void addFinalItem(ActionRequest actionRequest, ActionResponse actionResponse){
		long companyId = ParamUtil.getLong(actionRequest, "companyId");
		long groupId = ParamUtil.getLong(actionRequest, "groupId");
		long userId = ParamUtil.getLong(actionRequest, "userId");
		long orderId = ParamUtil.getLong(actionRequest, "orderId");
		long inventoryItemId = ParamUtil.getLong(actionRequest, "itemList");
		int quantity = ParamUtil.getInteger(actionRequest, "quantity");
		InventoryItem inventoryItem = null;
		
		try {
			inventoryItem = InventoryItemLocalServiceUtil.getInventoryItem(inventoryItemId);
			
			FinalItemLocalServiceUtil.addFinalItem(inventoryItem, companyId, 
					groupId, userId, orderId, quantity, 10d, 5d);
		} catch (PortalException e) {
			_log.error("Unable to create Final Item", e);
		} catch (SystemException e) {
			_log.error("Unable to create Final Item", e);
		}
		
		actionResponse.setRenderParameter("orderId", Long.toString(orderId));
		actionResponse.setRenderParameter("mvcPath", "/html/order/edit_order.jsp");
	}
	
	/**
	 * Updates order
	 * @param actionRequest
	 * @param actionResponse
	 */
	public void confirmOrder(ActionRequest actionRequest, ActionResponse actionResponse){
		
		long orderId = ParamUtil.getLong(actionRequest, "orderId");
		long companyId = ParamUtil.getLong(actionRequest, "companyId");
		long groupId = ParamUtil.getLong(actionRequest, "groupId");
		long userId = ParamUtil.getLong(actionRequest, "userId");
		
		try {
			OrderLocalServiceUtil.updateOrder(companyId, groupId, userId, orderId, true);
		} catch (SystemException e) {
			_log.error("Unable to update Order", e);
		}
	}


	/**
	 * Retrieves InventoryItem price, quantity, category, and imported for AJAX
	 * @param resourceRequest
	 * @param resourceResponse
	 */
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse
			) throws IOException, PortletException {
		
		long inventoryItemId = 0;
		DecimalFormat dollarFormat = new DecimalFormat("$#,###,###.00");
		inventoryItemId = ParamUtil.getLong(resourceRequest, "input");
		
		JSONObject jsonObject = null;
		try {
				InventoryItem item = InventoryItemLocalServiceUtil
						.getInventoryItem(inventoryItemId);
	
				jsonObject = JSONFactoryUtil.createJSONObject(); 
	
				jsonObject.put("price", item.getBasePrice() ); 
				jsonObject.put("quantity", 
						dollarFormat.format(item.getQuantity()) ); 
				jsonObject.put("category", CategoryLocalServiceUtil
						.getCategory(item.getCategoryId()).getName() ); 
				jsonObject.put("imported", item.getImported()); 
				
			} catch (SystemException e) {
				_log.error("Unable to get InventoryItem "+ inventoryItemId, e);
			} catch (PortalException e) {
				_log.error("Unable to get InventoryItem "+ inventoryItemId, e);
			}
		
		PrintWriter writer = resourceResponse.getWriter();
		writer.write(jsonObject.toString());
		writer.flush();
	}

	private static Log _log = LogFactoryUtil.getLog(OrderPortlet.class);
}
