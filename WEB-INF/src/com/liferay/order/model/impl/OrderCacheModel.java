/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.model.impl;

import com.liferay.order.model.Order;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Order in entity cache.
 *
 * @author Joshua Cords
 * @see Order
 * @generated
 */
public class OrderCacheModel implements CacheModel<Order>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", orderId=");
		sb.append(orderId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", confirmed=");
		sb.append(confirmed);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Order toEntityModel() {
		OrderImpl orderImpl = new OrderImpl();

		if (uuid == null) {
			orderImpl.setUuid(StringPool.BLANK);
		}
		else {
			orderImpl.setUuid(uuid);
		}

		orderImpl.setOrderId(orderId);
		orderImpl.setCompanyId(companyId);
		orderImpl.setGroupId(groupId);
		orderImpl.setUserId(userId);
		orderImpl.setConfirmed(confirmed);

		if (createDate == Long.MIN_VALUE) {
			orderImpl.setCreateDate(null);
		}
		else {
			orderImpl.setCreateDate(new Date(createDate));
		}

		orderImpl.resetOriginalValues();

		return orderImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();
		orderId = objectInput.readLong();
		companyId = objectInput.readLong();
		groupId = objectInput.readLong();
		userId = objectInput.readLong();
		confirmed = objectInput.readBoolean();
		createDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(orderId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(userId);
		objectOutput.writeBoolean(confirmed);
		objectOutput.writeLong(createDate);
	}

	public String uuid;
	public long orderId;
	public long companyId;
	public long groupId;
	public long userId;
	public boolean confirmed;
	public long createDate;
}