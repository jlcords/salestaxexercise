/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.model.impl;

import com.liferay.order.model.FinalItem;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing FinalItem in entity cache.
 *
 * @author Joshua Cords
 * @see FinalItem
 * @generated
 */
public class FinalItemCacheModel implements CacheModel<FinalItem>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", finalItemId=");
		sb.append(finalItemId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", orderId=");
		sb.append(orderId);
		sb.append(", categoryId=");
		sb.append(categoryId);
		sb.append(", name=");
		sb.append(name);
		sb.append(", basePrice=");
		sb.append(basePrice);
		sb.append(", totalPrice=");
		sb.append(totalPrice);
		sb.append(", quantity=");
		sb.append(quantity);
		sb.append(", imported=");
		sb.append(imported);
		sb.append(", tax=");
		sb.append(tax);
		sb.append(", duty=");
		sb.append(duty);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public FinalItem toEntityModel() {
		FinalItemImpl finalItemImpl = new FinalItemImpl();

		if (uuid == null) {
			finalItemImpl.setUuid(StringPool.BLANK);
		}
		else {
			finalItemImpl.setUuid(uuid);
		}

		finalItemImpl.setFinalItemId(finalItemId);
		finalItemImpl.setCompanyId(companyId);
		finalItemImpl.setGroupId(groupId);
		finalItemImpl.setUserId(userId);
		finalItemImpl.setOrderId(orderId);
		finalItemImpl.setCategoryId(categoryId);

		if (name == null) {
			finalItemImpl.setName(StringPool.BLANK);
		}
		else {
			finalItemImpl.setName(name);
		}

		finalItemImpl.setBasePrice(basePrice);
		finalItemImpl.setTotalPrice(totalPrice);
		finalItemImpl.setQuantity(quantity);
		finalItemImpl.setImported(imported);
		finalItemImpl.setTax(tax);
		finalItemImpl.setDuty(duty);

		finalItemImpl.resetOriginalValues();

		return finalItemImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();
		finalItemId = objectInput.readLong();
		companyId = objectInput.readLong();
		groupId = objectInput.readLong();
		userId = objectInput.readLong();
		orderId = objectInput.readLong();
		categoryId = objectInput.readLong();
		name = objectInput.readUTF();
		basePrice = objectInput.readDouble();
		totalPrice = objectInput.readDouble();
		quantity = objectInput.readInt();
		imported = objectInput.readBoolean();
		tax = objectInput.readDouble();
		duty = objectInput.readDouble();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(finalItemId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(userId);
		objectOutput.writeLong(orderId);
		objectOutput.writeLong(categoryId);

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		objectOutput.writeDouble(basePrice);
		objectOutput.writeDouble(totalPrice);
		objectOutput.writeInt(quantity);
		objectOutput.writeBoolean(imported);
		objectOutput.writeDouble(tax);
		objectOutput.writeDouble(duty);
	}

	public String uuid;
	public long finalItemId;
	public long companyId;
	public long groupId;
	public long userId;
	public long orderId;
	public long categoryId;
	public String name;
	public double basePrice;
	public double totalPrice;
	public int quantity;
	public boolean imported;
	public double tax;
	public double duty;
}