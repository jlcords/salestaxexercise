/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.model.impl;

import com.liferay.order.model.InventoryItem;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing InventoryItem in entity cache.
 *
 * @author Joshua Cords
 * @see InventoryItem
 * @generated
 */
public class InventoryItemCacheModel implements CacheModel<InventoryItem>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", inventoryItemId=");
		sb.append(inventoryItemId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", categoryId=");
		sb.append(categoryId);
		sb.append(", name=");
		sb.append(name);
		sb.append(", basePrice=");
		sb.append(basePrice);
		sb.append(", quantity=");
		sb.append(quantity);
		sb.append(", imported=");
		sb.append(imported);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public InventoryItem toEntityModel() {
		InventoryItemImpl inventoryItemImpl = new InventoryItemImpl();

		if (uuid == null) {
			inventoryItemImpl.setUuid(StringPool.BLANK);
		}
		else {
			inventoryItemImpl.setUuid(uuid);
		}

		inventoryItemImpl.setInventoryItemId(inventoryItemId);
		inventoryItemImpl.setCompanyId(companyId);
		inventoryItemImpl.setGroupId(groupId);
		inventoryItemImpl.setUserId(userId);
		inventoryItemImpl.setCategoryId(categoryId);

		if (name == null) {
			inventoryItemImpl.setName(StringPool.BLANK);
		}
		else {
			inventoryItemImpl.setName(name);
		}

		inventoryItemImpl.setBasePrice(basePrice);
		inventoryItemImpl.setQuantity(quantity);
		inventoryItemImpl.setImported(imported);

		inventoryItemImpl.resetOriginalValues();

		return inventoryItemImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();
		inventoryItemId = objectInput.readLong();
		companyId = objectInput.readLong();
		groupId = objectInput.readLong();
		userId = objectInput.readLong();
		categoryId = objectInput.readLong();
		name = objectInput.readUTF();
		basePrice = objectInput.readDouble();
		quantity = objectInput.readInt();
		imported = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(inventoryItemId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(userId);
		objectOutput.writeLong(categoryId);

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		objectOutput.writeDouble(basePrice);
		objectOutput.writeInt(quantity);
		objectOutput.writeBoolean(imported);
	}

	public String uuid;
	public long inventoryItemId;
	public long companyId;
	public long groupId;
	public long userId;
	public long categoryId;
	public String name;
	public double basePrice;
	public int quantity;
	public boolean imported;
}