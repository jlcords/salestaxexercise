/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.model.impl;

import com.liferay.order.model.TaxDuty;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing TaxDuty in entity cache.
 *
 * @author Joshua Cords
 * @see TaxDuty
 * @generated
 */
public class TaxDutyCacheModel implements CacheModel<TaxDuty>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", taxDutyId=");
		sb.append(taxDutyId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", tax=");
		sb.append(tax);
		sb.append(", duty=");
		sb.append(duty);
		sb.append(", round=");
		sb.append(round);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public TaxDuty toEntityModel() {
		TaxDutyImpl taxDutyImpl = new TaxDutyImpl();

		if (uuid == null) {
			taxDutyImpl.setUuid(StringPool.BLANK);
		}
		else {
			taxDutyImpl.setUuid(uuid);
		}

		taxDutyImpl.setTaxDutyId(taxDutyId);
		taxDutyImpl.setCompanyId(companyId);
		taxDutyImpl.setGroupId(groupId);
		taxDutyImpl.setTax(tax);
		taxDutyImpl.setDuty(duty);
		taxDutyImpl.setRound(round);

		taxDutyImpl.resetOriginalValues();

		return taxDutyImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();
		taxDutyId = objectInput.readLong();
		companyId = objectInput.readLong();
		groupId = objectInput.readLong();
		tax = objectInput.readInt();
		duty = objectInput.readInt();
		round = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(taxDutyId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(groupId);
		objectOutput.writeInt(tax);
		objectOutput.writeInt(duty);
		objectOutput.writeInt(round);
	}

	public String uuid;
	public long taxDutyId;
	public long companyId;
	public long groupId;
	public int tax;
	public int duty;
	public int round;
}