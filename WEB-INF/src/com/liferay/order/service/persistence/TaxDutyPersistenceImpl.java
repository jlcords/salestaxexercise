/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service.persistence;

import com.liferay.order.NoSuchTaxDutyException;
import com.liferay.order.model.TaxDuty;
import com.liferay.order.model.impl.TaxDutyImpl;
import com.liferay.order.model.impl.TaxDutyModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the tax duty service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Joshua Cords
 * @see TaxDutyPersistence
 * @see TaxDutyUtil
 * @generated
 */
public class TaxDutyPersistenceImpl extends BasePersistenceImpl<TaxDuty>
	implements TaxDutyPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link TaxDutyUtil} to access the tax duty persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = TaxDutyImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
			TaxDutyModelImpl.FINDER_CACHE_ENABLED, TaxDutyImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
			TaxDutyModelImpl.FINDER_CACHE_ENABLED, TaxDutyImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
			TaxDutyModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
			TaxDutyModelImpl.FINDER_CACHE_ENABLED, TaxDutyImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
			TaxDutyModelImpl.FINDER_CACHE_ENABLED, TaxDutyImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			TaxDutyModelImpl.UUID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
			TaxDutyModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the tax duties where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching tax duties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TaxDuty> findByUuid(String uuid) throws SystemException {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the tax duties where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of tax duties
	 * @param end the upper bound of the range of tax duties (not inclusive)
	 * @return the range of matching tax duties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TaxDuty> findByUuid(String uuid, int start, int end)
		throws SystemException {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the tax duties where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of tax duties
	 * @param end the upper bound of the range of tax duties (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tax duties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TaxDuty> findByUuid(String uuid, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<TaxDuty> list = (List<TaxDuty>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (TaxDuty taxDuty : list) {
				if (!Validator.equals(uuid, taxDuty.getUuid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_TAXDUTY_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TaxDutyModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<TaxDuty>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<TaxDuty>(list);
				}
				else {
					list = (List<TaxDuty>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first tax duty in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching tax duty
	 * @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty findByUuid_First(String uuid,
		OrderByComparator orderByComparator)
		throws NoSuchTaxDutyException, SystemException {
		TaxDuty taxDuty = fetchByUuid_First(uuid, orderByComparator);

		if (taxDuty != null) {
			return taxDuty;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTaxDutyException(msg.toString());
	}

	/**
	 * Returns the first tax duty in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching tax duty, or <code>null</code> if a matching tax duty could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty fetchByUuid_First(String uuid,
		OrderByComparator orderByComparator) throws SystemException {
		List<TaxDuty> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last tax duty in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching tax duty
	 * @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty findByUuid_Last(String uuid,
		OrderByComparator orderByComparator)
		throws NoSuchTaxDutyException, SystemException {
		TaxDuty taxDuty = fetchByUuid_Last(uuid, orderByComparator);

		if (taxDuty != null) {
			return taxDuty;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTaxDutyException(msg.toString());
	}

	/**
	 * Returns the last tax duty in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching tax duty, or <code>null</code> if a matching tax duty could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty fetchByUuid_Last(String uuid,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<TaxDuty> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the tax duties before and after the current tax duty in the ordered set where uuid = &#63;.
	 *
	 * @param taxDutyId the primary key of the current tax duty
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next tax duty
	 * @throws com.liferay.order.NoSuchTaxDutyException if a tax duty with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty[] findByUuid_PrevAndNext(long taxDutyId, String uuid,
		OrderByComparator orderByComparator)
		throws NoSuchTaxDutyException, SystemException {
		TaxDuty taxDuty = findByPrimaryKey(taxDutyId);

		Session session = null;

		try {
			session = openSession();

			TaxDuty[] array = new TaxDutyImpl[3];

			array[0] = getByUuid_PrevAndNext(session, taxDuty, uuid,
					orderByComparator, true);

			array[1] = taxDuty;

			array[2] = getByUuid_PrevAndNext(session, taxDuty, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TaxDuty getByUuid_PrevAndNext(Session session, TaxDuty taxDuty,
		String uuid, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TAXDUTY_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TaxDutyModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(taxDuty);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TaxDuty> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the tax duties where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUuid(String uuid) throws SystemException {
		for (TaxDuty taxDuty : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(taxDuty);
		}
	}

	/**
	 * Returns the number of tax duties where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching tax duties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUuid(String uuid) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TAXDUTY_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "taxDuty.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "taxDuty.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(taxDuty.uuid IS NULL OR taxDuty.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
			TaxDutyModelImpl.FINDER_CACHE_ENABLED, TaxDutyImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() },
			TaxDutyModelImpl.UUID_COLUMN_BITMASK |
			TaxDutyModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
			TaxDutyModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns the tax duty where uuid = &#63; and groupId = &#63; or throws a {@link com.liferay.order.NoSuchTaxDutyException} if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching tax duty
	 * @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty findByUUID_G(String uuid, long groupId)
		throws NoSuchTaxDutyException, SystemException {
		TaxDuty taxDuty = fetchByUUID_G(uuid, groupId);

		if (taxDuty == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchTaxDutyException(msg.toString());
		}

		return taxDuty;
	}

	/**
	 * Returns the tax duty where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching tax duty, or <code>null</code> if a matching tax duty could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty fetchByUUID_G(String uuid, long groupId)
		throws SystemException {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the tax duty where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching tax duty, or <code>null</code> if a matching tax duty could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { uuid, groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs, this);
		}

		if (result instanceof TaxDuty) {
			TaxDuty taxDuty = (TaxDuty)result;

			if (!Validator.equals(uuid, taxDuty.getUuid()) ||
					(groupId != taxDuty.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_TAXDUTY_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<TaxDuty> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
						finderArgs, list);
				}
				else {
					TaxDuty taxDuty = list.get(0);

					result = taxDuty;

					cacheResult(taxDuty);

					if ((taxDuty.getUuid() == null) ||
							!taxDuty.getUuid().equals(uuid) ||
							(taxDuty.getGroupId() != groupId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
							finderArgs, taxDuty);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (TaxDuty)result;
		}
	}

	/**
	 * Removes the tax duty where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the tax duty that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty removeByUUID_G(String uuid, long groupId)
		throws NoSuchTaxDutyException, SystemException {
		TaxDuty taxDuty = findByUUID_G(uuid, groupId);

		return remove(taxDuty);
	}

	/**
	 * Returns the number of tax duties where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching tax duties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;

		Object[] finderArgs = new Object[] { uuid, groupId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_TAXDUTY_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "taxDuty.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "taxDuty.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(taxDuty.uuid IS NULL OR taxDuty.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "taxDuty.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
			TaxDutyModelImpl.FINDER_CACHE_ENABLED, TaxDutyImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
			TaxDutyModelImpl.FINDER_CACHE_ENABLED, TaxDutyImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			TaxDutyModelImpl.UUID_COLUMN_BITMASK |
			TaxDutyModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
			TaxDutyModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the tax duties where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching tax duties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TaxDuty> findByUuid_C(String uuid, long companyId)
		throws SystemException {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the tax duties where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of tax duties
	 * @param end the upper bound of the range of tax duties (not inclusive)
	 * @return the range of matching tax duties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TaxDuty> findByUuid_C(String uuid, long companyId, int start,
		int end) throws SystemException {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the tax duties where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of tax duties
	 * @param end the upper bound of the range of tax duties (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching tax duties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TaxDuty> findByUuid_C(String uuid, long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<TaxDuty> list = (List<TaxDuty>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (TaxDuty taxDuty : list) {
				if (!Validator.equals(uuid, taxDuty.getUuid()) ||
						(companyId != taxDuty.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_TAXDUTY_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TaxDutyModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<TaxDuty>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<TaxDuty>(list);
				}
				else {
					list = (List<TaxDuty>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first tax duty in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching tax duty
	 * @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty findByUuid_C_First(String uuid, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchTaxDutyException, SystemException {
		TaxDuty taxDuty = fetchByUuid_C_First(uuid, companyId, orderByComparator);

		if (taxDuty != null) {
			return taxDuty;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTaxDutyException(msg.toString());
	}

	/**
	 * Returns the first tax duty in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching tax duty, or <code>null</code> if a matching tax duty could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<TaxDuty> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last tax duty in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching tax duty
	 * @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchTaxDutyException, SystemException {
		TaxDuty taxDuty = fetchByUuid_C_Last(uuid, companyId, orderByComparator);

		if (taxDuty != null) {
			return taxDuty;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTaxDutyException(msg.toString());
	}

	/**
	 * Returns the last tax duty in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching tax duty, or <code>null</code> if a matching tax duty could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<TaxDuty> list = findByUuid_C(uuid, companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the tax duties before and after the current tax duty in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param taxDutyId the primary key of the current tax duty
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next tax duty
	 * @throws com.liferay.order.NoSuchTaxDutyException if a tax duty with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty[] findByUuid_C_PrevAndNext(long taxDutyId, String uuid,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchTaxDutyException, SystemException {
		TaxDuty taxDuty = findByPrimaryKey(taxDutyId);

		Session session = null;

		try {
			session = openSession();

			TaxDuty[] array = new TaxDutyImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, taxDuty, uuid,
					companyId, orderByComparator, true);

			array[1] = taxDuty;

			array[2] = getByUuid_C_PrevAndNext(session, taxDuty, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TaxDuty getByUuid_C_PrevAndNext(Session session, TaxDuty taxDuty,
		String uuid, long companyId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TAXDUTY_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TaxDutyModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(taxDuty);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TaxDuty> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the tax duties where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId)
		throws SystemException {
		for (TaxDuty taxDuty : findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(taxDuty);
		}
	}

	/**
	 * Returns the number of tax duties where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching tax duties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_TAXDUTY_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "taxDuty.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "taxDuty.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(taxDuty.uuid IS NULL OR taxDuty.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "taxDuty.companyId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_GROUPID = new FinderPath(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
			TaxDutyModelImpl.FINDER_CACHE_ENABLED, TaxDutyImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByGroupId",
			new String[] { Long.class.getName() },
			TaxDutyModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUPID = new FinderPath(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
			TaxDutyModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the tax duty where groupId = &#63; or throws a {@link com.liferay.order.NoSuchTaxDutyException} if it could not be found.
	 *
	 * @param groupId the group ID
	 * @return the matching tax duty
	 * @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty findByGroupId(long groupId)
		throws NoSuchTaxDutyException, SystemException {
		TaxDuty taxDuty = fetchByGroupId(groupId);

		if (taxDuty == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchTaxDutyException(msg.toString());
		}

		return taxDuty;
	}

	/**
	 * Returns the tax duty where groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param groupId the group ID
	 * @return the matching tax duty, or <code>null</code> if a matching tax duty could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty fetchByGroupId(long groupId) throws SystemException {
		return fetchByGroupId(groupId, true);
	}

	/**
	 * Returns the tax duty where groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching tax duty, or <code>null</code> if a matching tax duty could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty fetchByGroupId(long groupId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_GROUPID,
					finderArgs, this);
		}

		if (result instanceof TaxDuty) {
			TaxDuty taxDuty = (TaxDuty)result;

			if ((groupId != taxDuty.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_TAXDUTY_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				List<TaxDuty> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_GROUPID,
						finderArgs, list);
				}
				else {
					TaxDuty taxDuty = list.get(0);

					result = taxDuty;

					cacheResult(taxDuty);

					if ((taxDuty.getGroupId() != groupId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_GROUPID,
							finderArgs, taxDuty);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_GROUPID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (TaxDuty)result;
		}
	}

	/**
	 * Removes the tax duty where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @return the tax duty that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty removeByGroupId(long groupId)
		throws NoSuchTaxDutyException, SystemException {
		TaxDuty taxDuty = findByGroupId(groupId);

		return remove(taxDuty);
	}

	/**
	 * Returns the number of tax duties where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching tax duties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByGroupId(long groupId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GROUPID;

		Object[] finderArgs = new Object[] { groupId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TAXDUTY_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 = "taxDuty.groupId = ?";

	public TaxDutyPersistenceImpl() {
		setModelClass(TaxDuty.class);
	}

	/**
	 * Caches the tax duty in the entity cache if it is enabled.
	 *
	 * @param taxDuty the tax duty
	 */
	@Override
	public void cacheResult(TaxDuty taxDuty) {
		EntityCacheUtil.putResult(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
			TaxDutyImpl.class, taxDuty.getPrimaryKey(), taxDuty);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
			new Object[] { taxDuty.getUuid(), taxDuty.getGroupId() }, taxDuty);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_GROUPID,
			new Object[] { taxDuty.getGroupId() }, taxDuty);

		taxDuty.resetOriginalValues();
	}

	/**
	 * Caches the tax duties in the entity cache if it is enabled.
	 *
	 * @param taxDuties the tax duties
	 */
	@Override
	public void cacheResult(List<TaxDuty> taxDuties) {
		for (TaxDuty taxDuty : taxDuties) {
			if (EntityCacheUtil.getResult(
						TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
						TaxDutyImpl.class, taxDuty.getPrimaryKey()) == null) {
				cacheResult(taxDuty);
			}
			else {
				taxDuty.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all tax duties.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(TaxDutyImpl.class.getName());
		}

		EntityCacheUtil.clearCache(TaxDutyImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the tax duty.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(TaxDuty taxDuty) {
		EntityCacheUtil.removeResult(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
			TaxDutyImpl.class, taxDuty.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(taxDuty);
	}

	@Override
	public void clearCache(List<TaxDuty> taxDuties) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (TaxDuty taxDuty : taxDuties) {
			EntityCacheUtil.removeResult(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
				TaxDutyImpl.class, taxDuty.getPrimaryKey());

			clearUniqueFindersCache(taxDuty);
		}
	}

	protected void cacheUniqueFindersCache(TaxDuty taxDuty) {
		if (taxDuty.isNew()) {
			Object[] args = new Object[] { taxDuty.getUuid(), taxDuty.getGroupId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G, args, taxDuty);

			args = new Object[] { taxDuty.getGroupId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_GROUPID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_GROUPID, args,
				taxDuty);
		}
		else {
			TaxDutyModelImpl taxDutyModelImpl = (TaxDutyModelImpl)taxDuty;

			if ((taxDutyModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						taxDuty.getUuid(), taxDuty.getGroupId()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
					taxDuty);
			}

			if ((taxDutyModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_GROUPID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { taxDuty.getGroupId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_GROUPID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_GROUPID, args,
					taxDuty);
			}
		}
	}

	protected void clearUniqueFindersCache(TaxDuty taxDuty) {
		TaxDutyModelImpl taxDutyModelImpl = (TaxDutyModelImpl)taxDuty;

		Object[] args = new Object[] { taxDuty.getUuid(), taxDuty.getGroupId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);

		if ((taxDutyModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			args = new Object[] {
					taxDutyModelImpl.getOriginalUuid(),
					taxDutyModelImpl.getOriginalGroupId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}

		args = new Object[] { taxDuty.getGroupId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_GROUPID, args);

		if ((taxDutyModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_GROUPID.getColumnBitmask()) != 0) {
			args = new Object[] { taxDutyModelImpl.getOriginalGroupId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_GROUPID, args);
		}
	}

	/**
	 * Creates a new tax duty with the primary key. Does not add the tax duty to the database.
	 *
	 * @param taxDutyId the primary key for the new tax duty
	 * @return the new tax duty
	 */
	@Override
	public TaxDuty create(long taxDutyId) {
		TaxDuty taxDuty = new TaxDutyImpl();

		taxDuty.setNew(true);
		taxDuty.setPrimaryKey(taxDutyId);

		String uuid = PortalUUIDUtil.generate();

		taxDuty.setUuid(uuid);

		return taxDuty;
	}

	/**
	 * Removes the tax duty with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param taxDutyId the primary key of the tax duty
	 * @return the tax duty that was removed
	 * @throws com.liferay.order.NoSuchTaxDutyException if a tax duty with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty remove(long taxDutyId)
		throws NoSuchTaxDutyException, SystemException {
		return remove((Serializable)taxDutyId);
	}

	/**
	 * Removes the tax duty with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the tax duty
	 * @return the tax duty that was removed
	 * @throws com.liferay.order.NoSuchTaxDutyException if a tax duty with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty remove(Serializable primaryKey)
		throws NoSuchTaxDutyException, SystemException {
		Session session = null;

		try {
			session = openSession();

			TaxDuty taxDuty = (TaxDuty)session.get(TaxDutyImpl.class, primaryKey);

			if (taxDuty == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTaxDutyException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(taxDuty);
		}
		catch (NoSuchTaxDutyException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected TaxDuty removeImpl(TaxDuty taxDuty) throws SystemException {
		taxDuty = toUnwrappedModel(taxDuty);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(taxDuty)) {
				taxDuty = (TaxDuty)session.get(TaxDutyImpl.class,
						taxDuty.getPrimaryKeyObj());
			}

			if (taxDuty != null) {
				session.delete(taxDuty);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (taxDuty != null) {
			clearCache(taxDuty);
		}

		return taxDuty;
	}

	@Override
	public TaxDuty updateImpl(com.liferay.order.model.TaxDuty taxDuty)
		throws SystemException {
		taxDuty = toUnwrappedModel(taxDuty);

		boolean isNew = taxDuty.isNew();

		TaxDutyModelImpl taxDutyModelImpl = (TaxDutyModelImpl)taxDuty;

		if (Validator.isNull(taxDuty.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			taxDuty.setUuid(uuid);
		}

		Session session = null;

		try {
			session = openSession();

			if (taxDuty.isNew()) {
				session.save(taxDuty);

				taxDuty.setNew(false);
			}
			else {
				session.merge(taxDuty);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !TaxDutyModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((taxDutyModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { taxDutyModelImpl.getOriginalUuid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { taxDutyModelImpl.getUuid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((taxDutyModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						taxDutyModelImpl.getOriginalUuid(),
						taxDutyModelImpl.getOriginalCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						taxDutyModelImpl.getUuid(),
						taxDutyModelImpl.getCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}
		}

		EntityCacheUtil.putResult(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
			TaxDutyImpl.class, taxDuty.getPrimaryKey(), taxDuty);

		clearUniqueFindersCache(taxDuty);
		cacheUniqueFindersCache(taxDuty);

		return taxDuty;
	}

	protected TaxDuty toUnwrappedModel(TaxDuty taxDuty) {
		if (taxDuty instanceof TaxDutyImpl) {
			return taxDuty;
		}

		TaxDutyImpl taxDutyImpl = new TaxDutyImpl();

		taxDutyImpl.setNew(taxDuty.isNew());
		taxDutyImpl.setPrimaryKey(taxDuty.getPrimaryKey());

		taxDutyImpl.setUuid(taxDuty.getUuid());
		taxDutyImpl.setTaxDutyId(taxDuty.getTaxDutyId());
		taxDutyImpl.setCompanyId(taxDuty.getCompanyId());
		taxDutyImpl.setGroupId(taxDuty.getGroupId());
		taxDutyImpl.setTax(taxDuty.getTax());
		taxDutyImpl.setDuty(taxDuty.getDuty());
		taxDutyImpl.setRound(taxDuty.getRound());

		return taxDutyImpl;
	}

	/**
	 * Returns the tax duty with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the tax duty
	 * @return the tax duty
	 * @throws com.liferay.order.NoSuchTaxDutyException if a tax duty with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty findByPrimaryKey(Serializable primaryKey)
		throws NoSuchTaxDutyException, SystemException {
		TaxDuty taxDuty = fetchByPrimaryKey(primaryKey);

		if (taxDuty == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchTaxDutyException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return taxDuty;
	}

	/**
	 * Returns the tax duty with the primary key or throws a {@link com.liferay.order.NoSuchTaxDutyException} if it could not be found.
	 *
	 * @param taxDutyId the primary key of the tax duty
	 * @return the tax duty
	 * @throws com.liferay.order.NoSuchTaxDutyException if a tax duty with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty findByPrimaryKey(long taxDutyId)
		throws NoSuchTaxDutyException, SystemException {
		return findByPrimaryKey((Serializable)taxDutyId);
	}

	/**
	 * Returns the tax duty with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the tax duty
	 * @return the tax duty, or <code>null</code> if a tax duty with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		TaxDuty taxDuty = (TaxDuty)EntityCacheUtil.getResult(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
				TaxDutyImpl.class, primaryKey);

		if (taxDuty == _nullTaxDuty) {
			return null;
		}

		if (taxDuty == null) {
			Session session = null;

			try {
				session = openSession();

				taxDuty = (TaxDuty)session.get(TaxDutyImpl.class, primaryKey);

				if (taxDuty != null) {
					cacheResult(taxDuty);
				}
				else {
					EntityCacheUtil.putResult(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
						TaxDutyImpl.class, primaryKey, _nullTaxDuty);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(TaxDutyModelImpl.ENTITY_CACHE_ENABLED,
					TaxDutyImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return taxDuty;
	}

	/**
	 * Returns the tax duty with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param taxDutyId the primary key of the tax duty
	 * @return the tax duty, or <code>null</code> if a tax duty with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TaxDuty fetchByPrimaryKey(long taxDutyId) throws SystemException {
		return fetchByPrimaryKey((Serializable)taxDutyId);
	}

	/**
	 * Returns all the tax duties.
	 *
	 * @return the tax duties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TaxDuty> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the tax duties.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tax duties
	 * @param end the upper bound of the range of tax duties (not inclusive)
	 * @return the range of tax duties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TaxDuty> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the tax duties.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tax duties
	 * @param end the upper bound of the range of tax duties (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of tax duties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TaxDuty> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<TaxDuty> list = (List<TaxDuty>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_TAXDUTY);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_TAXDUTY;

				if (pagination) {
					sql = sql.concat(TaxDutyModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<TaxDuty>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<TaxDuty>(list);
				}
				else {
					list = (List<TaxDuty>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the tax duties from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (TaxDuty taxDuty : findAll()) {
			remove(taxDuty);
		}
	}

	/**
	 * Returns the number of tax duties.
	 *
	 * @return the number of tax duties
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_TAXDUTY);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the tax duty persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.liferay.order.model.TaxDuty")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<TaxDuty>> listenersList = new ArrayList<ModelListener<TaxDuty>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<TaxDuty>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(TaxDutyImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_TAXDUTY = "SELECT taxDuty FROM TaxDuty taxDuty";
	private static final String _SQL_SELECT_TAXDUTY_WHERE = "SELECT taxDuty FROM TaxDuty taxDuty WHERE ";
	private static final String _SQL_COUNT_TAXDUTY = "SELECT COUNT(taxDuty) FROM TaxDuty taxDuty";
	private static final String _SQL_COUNT_TAXDUTY_WHERE = "SELECT COUNT(taxDuty) FROM TaxDuty taxDuty WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "taxDuty.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No TaxDuty exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No TaxDuty exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(TaxDutyPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
	private static TaxDuty _nullTaxDuty = new TaxDutyImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<TaxDuty> toCacheModel() {
				return _nullTaxDutyCacheModel;
			}
		};

	private static CacheModel<TaxDuty> _nullTaxDutyCacheModel = new CacheModel<TaxDuty>() {
			@Override
			public TaxDuty toEntityModel() {
				return _nullTaxDuty;
			}
		};
}