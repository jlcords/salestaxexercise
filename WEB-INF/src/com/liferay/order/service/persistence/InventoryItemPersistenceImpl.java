/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service.persistence;

import com.liferay.order.NoSuchInventoryItemException;
import com.liferay.order.model.InventoryItem;
import com.liferay.order.model.impl.InventoryItemImpl;
import com.liferay.order.model.impl.InventoryItemModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the inventory item service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Joshua Cords
 * @see InventoryItemPersistence
 * @see InventoryItemUtil
 * @generated
 */
public class InventoryItemPersistenceImpl extends BasePersistenceImpl<InventoryItem>
	implements InventoryItemPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link InventoryItemUtil} to access the inventory item persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = InventoryItemImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
			InventoryItemModelImpl.FINDER_CACHE_ENABLED,
			InventoryItemImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
			InventoryItemModelImpl.FINDER_CACHE_ENABLED,
			InventoryItemImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
			InventoryItemModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
			InventoryItemModelImpl.FINDER_CACHE_ENABLED,
			InventoryItemImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
			InventoryItemModelImpl.FINDER_CACHE_ENABLED,
			InventoryItemImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUuid", new String[] { String.class.getName() },
			InventoryItemModelImpl.UUID_COLUMN_BITMASK |
			InventoryItemModelImpl.NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
			InventoryItemModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the inventory items where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching inventory items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InventoryItem> findByUuid(String uuid)
		throws SystemException {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the inventory items where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of inventory items
	 * @param end the upper bound of the range of inventory items (not inclusive)
	 * @return the range of matching inventory items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InventoryItem> findByUuid(String uuid, int start, int end)
		throws SystemException {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the inventory items where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of inventory items
	 * @param end the upper bound of the range of inventory items (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching inventory items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InventoryItem> findByUuid(String uuid, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<InventoryItem> list = (List<InventoryItem>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (InventoryItem inventoryItem : list) {
				if (!Validator.equals(uuid, inventoryItem.getUuid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_INVENTORYITEM_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(InventoryItemModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<InventoryItem>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<InventoryItem>(list);
				}
				else {
					list = (List<InventoryItem>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first inventory item in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching inventory item
	 * @throws com.liferay.order.NoSuchInventoryItemException if a matching inventory item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem findByUuid_First(String uuid,
		OrderByComparator orderByComparator)
		throws NoSuchInventoryItemException, SystemException {
		InventoryItem inventoryItem = fetchByUuid_First(uuid, orderByComparator);

		if (inventoryItem != null) {
			return inventoryItem;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchInventoryItemException(msg.toString());
	}

	/**
	 * Returns the first inventory item in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching inventory item, or <code>null</code> if a matching inventory item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem fetchByUuid_First(String uuid,
		OrderByComparator orderByComparator) throws SystemException {
		List<InventoryItem> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last inventory item in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching inventory item
	 * @throws com.liferay.order.NoSuchInventoryItemException if a matching inventory item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem findByUuid_Last(String uuid,
		OrderByComparator orderByComparator)
		throws NoSuchInventoryItemException, SystemException {
		InventoryItem inventoryItem = fetchByUuid_Last(uuid, orderByComparator);

		if (inventoryItem != null) {
			return inventoryItem;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchInventoryItemException(msg.toString());
	}

	/**
	 * Returns the last inventory item in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching inventory item, or <code>null</code> if a matching inventory item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem fetchByUuid_Last(String uuid,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<InventoryItem> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the inventory items before and after the current inventory item in the ordered set where uuid = &#63;.
	 *
	 * @param inventoryItemId the primary key of the current inventory item
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next inventory item
	 * @throws com.liferay.order.NoSuchInventoryItemException if a inventory item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem[] findByUuid_PrevAndNext(long inventoryItemId,
		String uuid, OrderByComparator orderByComparator)
		throws NoSuchInventoryItemException, SystemException {
		InventoryItem inventoryItem = findByPrimaryKey(inventoryItemId);

		Session session = null;

		try {
			session = openSession();

			InventoryItem[] array = new InventoryItemImpl[3];

			array[0] = getByUuid_PrevAndNext(session, inventoryItem, uuid,
					orderByComparator, true);

			array[1] = inventoryItem;

			array[2] = getByUuid_PrevAndNext(session, inventoryItem, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected InventoryItem getByUuid_PrevAndNext(Session session,
		InventoryItem inventoryItem, String uuid,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_INVENTORYITEM_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(InventoryItemModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(inventoryItem);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<InventoryItem> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the inventory items where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUuid(String uuid) throws SystemException {
		for (InventoryItem inventoryItem : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(inventoryItem);
		}
	}

	/**
	 * Returns the number of inventory items where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching inventory items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUuid(String uuid) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_INVENTORYITEM_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "inventoryItem.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "inventoryItem.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(inventoryItem.uuid IS NULL OR inventoryItem.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
			InventoryItemModelImpl.FINDER_CACHE_ENABLED,
			InventoryItemImpl.class, FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() },
			InventoryItemModelImpl.UUID_COLUMN_BITMASK |
			InventoryItemModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
			InventoryItemModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns the inventory item where uuid = &#63; and groupId = &#63; or throws a {@link com.liferay.order.NoSuchInventoryItemException} if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching inventory item
	 * @throws com.liferay.order.NoSuchInventoryItemException if a matching inventory item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem findByUUID_G(String uuid, long groupId)
		throws NoSuchInventoryItemException, SystemException {
		InventoryItem inventoryItem = fetchByUUID_G(uuid, groupId);

		if (inventoryItem == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchInventoryItemException(msg.toString());
		}

		return inventoryItem;
	}

	/**
	 * Returns the inventory item where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching inventory item, or <code>null</code> if a matching inventory item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem fetchByUUID_G(String uuid, long groupId)
		throws SystemException {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the inventory item where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching inventory item, or <code>null</code> if a matching inventory item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { uuid, groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs, this);
		}

		if (result instanceof InventoryItem) {
			InventoryItem inventoryItem = (InventoryItem)result;

			if (!Validator.equals(uuid, inventoryItem.getUuid()) ||
					(groupId != inventoryItem.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_INVENTORYITEM_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<InventoryItem> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
						finderArgs, list);
				}
				else {
					InventoryItem inventoryItem = list.get(0);

					result = inventoryItem;

					cacheResult(inventoryItem);

					if ((inventoryItem.getUuid() == null) ||
							!inventoryItem.getUuid().equals(uuid) ||
							(inventoryItem.getGroupId() != groupId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
							finderArgs, inventoryItem);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (InventoryItem)result;
		}
	}

	/**
	 * Removes the inventory item where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the inventory item that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem removeByUUID_G(String uuid, long groupId)
		throws NoSuchInventoryItemException, SystemException {
		InventoryItem inventoryItem = findByUUID_G(uuid, groupId);

		return remove(inventoryItem);
	}

	/**
	 * Returns the number of inventory items where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching inventory items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;

		Object[] finderArgs = new Object[] { uuid, groupId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_INVENTORYITEM_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "inventoryItem.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "inventoryItem.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(inventoryItem.uuid IS NULL OR inventoryItem.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "inventoryItem.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
			InventoryItemModelImpl.FINDER_CACHE_ENABLED,
			InventoryItemImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
			InventoryItemModelImpl.FINDER_CACHE_ENABLED,
			InventoryItemImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			InventoryItemModelImpl.UUID_COLUMN_BITMASK |
			InventoryItemModelImpl.COMPANYID_COLUMN_BITMASK |
			InventoryItemModelImpl.NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
			InventoryItemModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the inventory items where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching inventory items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InventoryItem> findByUuid_C(String uuid, long companyId)
		throws SystemException {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the inventory items where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of inventory items
	 * @param end the upper bound of the range of inventory items (not inclusive)
	 * @return the range of matching inventory items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InventoryItem> findByUuid_C(String uuid, long companyId,
		int start, int end) throws SystemException {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the inventory items where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of inventory items
	 * @param end the upper bound of the range of inventory items (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching inventory items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InventoryItem> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<InventoryItem> list = (List<InventoryItem>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (InventoryItem inventoryItem : list) {
				if (!Validator.equals(uuid, inventoryItem.getUuid()) ||
						(companyId != inventoryItem.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_INVENTORYITEM_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(InventoryItemModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<InventoryItem>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<InventoryItem>(list);
				}
				else {
					list = (List<InventoryItem>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first inventory item in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching inventory item
	 * @throws com.liferay.order.NoSuchInventoryItemException if a matching inventory item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem findByUuid_C_First(String uuid, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchInventoryItemException, SystemException {
		InventoryItem inventoryItem = fetchByUuid_C_First(uuid, companyId,
				orderByComparator);

		if (inventoryItem != null) {
			return inventoryItem;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchInventoryItemException(msg.toString());
	}

	/**
	 * Returns the first inventory item in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching inventory item, or <code>null</code> if a matching inventory item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<InventoryItem> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last inventory item in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching inventory item
	 * @throws com.liferay.order.NoSuchInventoryItemException if a matching inventory item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchInventoryItemException, SystemException {
		InventoryItem inventoryItem = fetchByUuid_C_Last(uuid, companyId,
				orderByComparator);

		if (inventoryItem != null) {
			return inventoryItem;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchInventoryItemException(msg.toString());
	}

	/**
	 * Returns the last inventory item in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching inventory item, or <code>null</code> if a matching inventory item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<InventoryItem> list = findByUuid_C(uuid, companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the inventory items before and after the current inventory item in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param inventoryItemId the primary key of the current inventory item
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next inventory item
	 * @throws com.liferay.order.NoSuchInventoryItemException if a inventory item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem[] findByUuid_C_PrevAndNext(long inventoryItemId,
		String uuid, long companyId, OrderByComparator orderByComparator)
		throws NoSuchInventoryItemException, SystemException {
		InventoryItem inventoryItem = findByPrimaryKey(inventoryItemId);

		Session session = null;

		try {
			session = openSession();

			InventoryItem[] array = new InventoryItemImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, inventoryItem, uuid,
					companyId, orderByComparator, true);

			array[1] = inventoryItem;

			array[2] = getByUuid_C_PrevAndNext(session, inventoryItem, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected InventoryItem getByUuid_C_PrevAndNext(Session session,
		InventoryItem inventoryItem, String uuid, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_INVENTORYITEM_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(InventoryItemModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(inventoryItem);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<InventoryItem> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the inventory items where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId)
		throws SystemException {
		for (InventoryItem inventoryItem : findByUuid_C(uuid, companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(inventoryItem);
		}
	}

	/**
	 * Returns the number of inventory items where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching inventory items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_INVENTORYITEM_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "inventoryItem.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "inventoryItem.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(inventoryItem.uuid IS NULL OR inventoryItem.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "inventoryItem.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID = new FinderPath(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
			InventoryItemModelImpl.FINDER_CACHE_ENABLED,
			InventoryItemImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByGroupId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID =
		new FinderPath(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
			InventoryItemModelImpl.FINDER_CACHE_ENABLED,
			InventoryItemImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByGroupId", new String[] { Long.class.getName() },
			InventoryItemModelImpl.GROUPID_COLUMN_BITMASK |
			InventoryItemModelImpl.NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUPID = new FinderPath(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
			InventoryItemModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the inventory items where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching inventory items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InventoryItem> findByGroupId(long groupId)
		throws SystemException {
		return findByGroupId(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the inventory items where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of inventory items
	 * @param end the upper bound of the range of inventory items (not inclusive)
	 * @return the range of matching inventory items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InventoryItem> findByGroupId(long groupId, int start, int end)
		throws SystemException {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the inventory items where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of inventory items
	 * @param end the upper bound of the range of inventory items (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching inventory items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InventoryItem> findByGroupId(long groupId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId, start, end, orderByComparator };
		}

		List<InventoryItem> list = (List<InventoryItem>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (InventoryItem inventoryItem : list) {
				if ((groupId != inventoryItem.getGroupId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_INVENTORYITEM_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(InventoryItemModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (!pagination) {
					list = (List<InventoryItem>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<InventoryItem>(list);
				}
				else {
					list = (List<InventoryItem>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first inventory item in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching inventory item
	 * @throws com.liferay.order.NoSuchInventoryItemException if a matching inventory item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem findByGroupId_First(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchInventoryItemException, SystemException {
		InventoryItem inventoryItem = fetchByGroupId_First(groupId,
				orderByComparator);

		if (inventoryItem != null) {
			return inventoryItem;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchInventoryItemException(msg.toString());
	}

	/**
	 * Returns the first inventory item in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching inventory item, or <code>null</code> if a matching inventory item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem fetchByGroupId_First(long groupId,
		OrderByComparator orderByComparator) throws SystemException {
		List<InventoryItem> list = findByGroupId(groupId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last inventory item in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching inventory item
	 * @throws com.liferay.order.NoSuchInventoryItemException if a matching inventory item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem findByGroupId_Last(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchInventoryItemException, SystemException {
		InventoryItem inventoryItem = fetchByGroupId_Last(groupId,
				orderByComparator);

		if (inventoryItem != null) {
			return inventoryItem;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchInventoryItemException(msg.toString());
	}

	/**
	 * Returns the last inventory item in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching inventory item, or <code>null</code> if a matching inventory item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem fetchByGroupId_Last(long groupId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<InventoryItem> list = findByGroupId(groupId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the inventory items before and after the current inventory item in the ordered set where groupId = &#63;.
	 *
	 * @param inventoryItemId the primary key of the current inventory item
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next inventory item
	 * @throws com.liferay.order.NoSuchInventoryItemException if a inventory item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem[] findByGroupId_PrevAndNext(long inventoryItemId,
		long groupId, OrderByComparator orderByComparator)
		throws NoSuchInventoryItemException, SystemException {
		InventoryItem inventoryItem = findByPrimaryKey(inventoryItemId);

		Session session = null;

		try {
			session = openSession();

			InventoryItem[] array = new InventoryItemImpl[3];

			array[0] = getByGroupId_PrevAndNext(session, inventoryItem,
					groupId, orderByComparator, true);

			array[1] = inventoryItem;

			array[2] = getByGroupId_PrevAndNext(session, inventoryItem,
					groupId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected InventoryItem getByGroupId_PrevAndNext(Session session,
		InventoryItem inventoryItem, long groupId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_INVENTORYITEM_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(InventoryItemModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(inventoryItem);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<InventoryItem> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the inventory items where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByGroupId(long groupId) throws SystemException {
		for (InventoryItem inventoryItem : findByGroupId(groupId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(inventoryItem);
		}
	}

	/**
	 * Returns the number of inventory items where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching inventory items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByGroupId(long groupId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GROUPID;

		Object[] finderArgs = new Object[] { groupId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_INVENTORYITEM_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 = "inventoryItem.groupId = ?";

	public InventoryItemPersistenceImpl() {
		setModelClass(InventoryItem.class);
	}

	/**
	 * Caches the inventory item in the entity cache if it is enabled.
	 *
	 * @param inventoryItem the inventory item
	 */
	@Override
	public void cacheResult(InventoryItem inventoryItem) {
		EntityCacheUtil.putResult(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
			InventoryItemImpl.class, inventoryItem.getPrimaryKey(),
			inventoryItem);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
			new Object[] { inventoryItem.getUuid(), inventoryItem.getGroupId() },
			inventoryItem);

		inventoryItem.resetOriginalValues();
	}

	/**
	 * Caches the inventory items in the entity cache if it is enabled.
	 *
	 * @param inventoryItems the inventory items
	 */
	@Override
	public void cacheResult(List<InventoryItem> inventoryItems) {
		for (InventoryItem inventoryItem : inventoryItems) {
			if (EntityCacheUtil.getResult(
						InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
						InventoryItemImpl.class, inventoryItem.getPrimaryKey()) == null) {
				cacheResult(inventoryItem);
			}
			else {
				inventoryItem.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all inventory items.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(InventoryItemImpl.class.getName());
		}

		EntityCacheUtil.clearCache(InventoryItemImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the inventory item.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(InventoryItem inventoryItem) {
		EntityCacheUtil.removeResult(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
			InventoryItemImpl.class, inventoryItem.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(inventoryItem);
	}

	@Override
	public void clearCache(List<InventoryItem> inventoryItems) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (InventoryItem inventoryItem : inventoryItems) {
			EntityCacheUtil.removeResult(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
				InventoryItemImpl.class, inventoryItem.getPrimaryKey());

			clearUniqueFindersCache(inventoryItem);
		}
	}

	protected void cacheUniqueFindersCache(InventoryItem inventoryItem) {
		if (inventoryItem.isNew()) {
			Object[] args = new Object[] {
					inventoryItem.getUuid(), inventoryItem.getGroupId()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
				inventoryItem);
		}
		else {
			InventoryItemModelImpl inventoryItemModelImpl = (InventoryItemModelImpl)inventoryItem;

			if ((inventoryItemModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						inventoryItem.getUuid(), inventoryItem.getGroupId()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
					inventoryItem);
			}
		}
	}

	protected void clearUniqueFindersCache(InventoryItem inventoryItem) {
		InventoryItemModelImpl inventoryItemModelImpl = (InventoryItemModelImpl)inventoryItem;

		Object[] args = new Object[] {
				inventoryItem.getUuid(), inventoryItem.getGroupId()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);

		if ((inventoryItemModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			args = new Object[] {
					inventoryItemModelImpl.getOriginalUuid(),
					inventoryItemModelImpl.getOriginalGroupId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}
	}

	/**
	 * Creates a new inventory item with the primary key. Does not add the inventory item to the database.
	 *
	 * @param inventoryItemId the primary key for the new inventory item
	 * @return the new inventory item
	 */
	@Override
	public InventoryItem create(long inventoryItemId) {
		InventoryItem inventoryItem = new InventoryItemImpl();

		inventoryItem.setNew(true);
		inventoryItem.setPrimaryKey(inventoryItemId);

		String uuid = PortalUUIDUtil.generate();

		inventoryItem.setUuid(uuid);

		return inventoryItem;
	}

	/**
	 * Removes the inventory item with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param inventoryItemId the primary key of the inventory item
	 * @return the inventory item that was removed
	 * @throws com.liferay.order.NoSuchInventoryItemException if a inventory item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem remove(long inventoryItemId)
		throws NoSuchInventoryItemException, SystemException {
		return remove((Serializable)inventoryItemId);
	}

	/**
	 * Removes the inventory item with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the inventory item
	 * @return the inventory item that was removed
	 * @throws com.liferay.order.NoSuchInventoryItemException if a inventory item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem remove(Serializable primaryKey)
		throws NoSuchInventoryItemException, SystemException {
		Session session = null;

		try {
			session = openSession();

			InventoryItem inventoryItem = (InventoryItem)session.get(InventoryItemImpl.class,
					primaryKey);

			if (inventoryItem == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchInventoryItemException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(inventoryItem);
		}
		catch (NoSuchInventoryItemException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected InventoryItem removeImpl(InventoryItem inventoryItem)
		throws SystemException {
		inventoryItem = toUnwrappedModel(inventoryItem);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(inventoryItem)) {
				inventoryItem = (InventoryItem)session.get(InventoryItemImpl.class,
						inventoryItem.getPrimaryKeyObj());
			}

			if (inventoryItem != null) {
				session.delete(inventoryItem);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (inventoryItem != null) {
			clearCache(inventoryItem);
		}

		return inventoryItem;
	}

	@Override
	public InventoryItem updateImpl(
		com.liferay.order.model.InventoryItem inventoryItem)
		throws SystemException {
		inventoryItem = toUnwrappedModel(inventoryItem);

		boolean isNew = inventoryItem.isNew();

		InventoryItemModelImpl inventoryItemModelImpl = (InventoryItemModelImpl)inventoryItem;

		if (Validator.isNull(inventoryItem.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			inventoryItem.setUuid(uuid);
		}

		Session session = null;

		try {
			session = openSession();

			if (inventoryItem.isNew()) {
				session.save(inventoryItem);

				inventoryItem.setNew(false);
			}
			else {
				session.merge(inventoryItem);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !InventoryItemModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((inventoryItemModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						inventoryItemModelImpl.getOriginalUuid()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { inventoryItemModelImpl.getUuid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((inventoryItemModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						inventoryItemModelImpl.getOriginalUuid(),
						inventoryItemModelImpl.getOriginalCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						inventoryItemModelImpl.getUuid(),
						inventoryItemModelImpl.getCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((inventoryItemModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						inventoryItemModelImpl.getOriginalGroupId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);

				args = new Object[] { inventoryItemModelImpl.getGroupId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);
			}
		}

		EntityCacheUtil.putResult(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
			InventoryItemImpl.class, inventoryItem.getPrimaryKey(),
			inventoryItem);

		clearUniqueFindersCache(inventoryItem);
		cacheUniqueFindersCache(inventoryItem);

		return inventoryItem;
	}

	protected InventoryItem toUnwrappedModel(InventoryItem inventoryItem) {
		if (inventoryItem instanceof InventoryItemImpl) {
			return inventoryItem;
		}

		InventoryItemImpl inventoryItemImpl = new InventoryItemImpl();

		inventoryItemImpl.setNew(inventoryItem.isNew());
		inventoryItemImpl.setPrimaryKey(inventoryItem.getPrimaryKey());

		inventoryItemImpl.setUuid(inventoryItem.getUuid());
		inventoryItemImpl.setInventoryItemId(inventoryItem.getInventoryItemId());
		inventoryItemImpl.setCompanyId(inventoryItem.getCompanyId());
		inventoryItemImpl.setGroupId(inventoryItem.getGroupId());
		inventoryItemImpl.setUserId(inventoryItem.getUserId());
		inventoryItemImpl.setCategoryId(inventoryItem.getCategoryId());
		inventoryItemImpl.setName(inventoryItem.getName());
		inventoryItemImpl.setBasePrice(inventoryItem.getBasePrice());
		inventoryItemImpl.setQuantity(inventoryItem.getQuantity());
		inventoryItemImpl.setImported(inventoryItem.isImported());

		return inventoryItemImpl;
	}

	/**
	 * Returns the inventory item with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the inventory item
	 * @return the inventory item
	 * @throws com.liferay.order.NoSuchInventoryItemException if a inventory item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem findByPrimaryKey(Serializable primaryKey)
		throws NoSuchInventoryItemException, SystemException {
		InventoryItem inventoryItem = fetchByPrimaryKey(primaryKey);

		if (inventoryItem == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchInventoryItemException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return inventoryItem;
	}

	/**
	 * Returns the inventory item with the primary key or throws a {@link com.liferay.order.NoSuchInventoryItemException} if it could not be found.
	 *
	 * @param inventoryItemId the primary key of the inventory item
	 * @return the inventory item
	 * @throws com.liferay.order.NoSuchInventoryItemException if a inventory item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem findByPrimaryKey(long inventoryItemId)
		throws NoSuchInventoryItemException, SystemException {
		return findByPrimaryKey((Serializable)inventoryItemId);
	}

	/**
	 * Returns the inventory item with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the inventory item
	 * @return the inventory item, or <code>null</code> if a inventory item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		InventoryItem inventoryItem = (InventoryItem)EntityCacheUtil.getResult(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
				InventoryItemImpl.class, primaryKey);

		if (inventoryItem == _nullInventoryItem) {
			return null;
		}

		if (inventoryItem == null) {
			Session session = null;

			try {
				session = openSession();

				inventoryItem = (InventoryItem)session.get(InventoryItemImpl.class,
						primaryKey);

				if (inventoryItem != null) {
					cacheResult(inventoryItem);
				}
				else {
					EntityCacheUtil.putResult(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
						InventoryItemImpl.class, primaryKey, _nullInventoryItem);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(InventoryItemModelImpl.ENTITY_CACHE_ENABLED,
					InventoryItemImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return inventoryItem;
	}

	/**
	 * Returns the inventory item with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param inventoryItemId the primary key of the inventory item
	 * @return the inventory item, or <code>null</code> if a inventory item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InventoryItem fetchByPrimaryKey(long inventoryItemId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)inventoryItemId);
	}

	/**
	 * Returns all the inventory items.
	 *
	 * @return the inventory items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InventoryItem> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the inventory items.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of inventory items
	 * @param end the upper bound of the range of inventory items (not inclusive)
	 * @return the range of inventory items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InventoryItem> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the inventory items.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of inventory items
	 * @param end the upper bound of the range of inventory items (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of inventory items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InventoryItem> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<InventoryItem> list = (List<InventoryItem>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_INVENTORYITEM);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_INVENTORYITEM;

				if (pagination) {
					sql = sql.concat(InventoryItemModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<InventoryItem>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<InventoryItem>(list);
				}
				else {
					list = (List<InventoryItem>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the inventory items from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (InventoryItem inventoryItem : findAll()) {
			remove(inventoryItem);
		}
	}

	/**
	 * Returns the number of inventory items.
	 *
	 * @return the number of inventory items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_INVENTORYITEM);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the inventory item persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.liferay.order.model.InventoryItem")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<InventoryItem>> listenersList = new ArrayList<ModelListener<InventoryItem>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<InventoryItem>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(InventoryItemImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_INVENTORYITEM = "SELECT inventoryItem FROM InventoryItem inventoryItem";
	private static final String _SQL_SELECT_INVENTORYITEM_WHERE = "SELECT inventoryItem FROM InventoryItem inventoryItem WHERE ";
	private static final String _SQL_COUNT_INVENTORYITEM = "SELECT COUNT(inventoryItem) FROM InventoryItem inventoryItem";
	private static final String _SQL_COUNT_INVENTORYITEM_WHERE = "SELECT COUNT(inventoryItem) FROM InventoryItem inventoryItem WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "inventoryItem.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No InventoryItem exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No InventoryItem exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(InventoryItemPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
	private static InventoryItem _nullInventoryItem = new InventoryItemImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<InventoryItem> toCacheModel() {
				return _nullInventoryItemCacheModel;
			}
		};

	private static CacheModel<InventoryItem> _nullInventoryItemCacheModel = new CacheModel<InventoryItem>() {
			@Override
			public InventoryItem toEntityModel() {
				return _nullInventoryItem;
			}
		};
}