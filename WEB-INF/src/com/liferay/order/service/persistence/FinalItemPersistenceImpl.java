/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service.persistence;

import com.liferay.order.NoSuchFinalItemException;
import com.liferay.order.model.FinalItem;
import com.liferay.order.model.impl.FinalItemImpl;
import com.liferay.order.model.impl.FinalItemModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the final item service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Joshua Cords
 * @see FinalItemPersistence
 * @see FinalItemUtil
 * @generated
 */
public class FinalItemPersistenceImpl extends BasePersistenceImpl<FinalItem>
	implements FinalItemPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link FinalItemUtil} to access the final item persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = FinalItemImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
			FinalItemModelImpl.FINDER_CACHE_ENABLED, FinalItemImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
			FinalItemModelImpl.FINDER_CACHE_ENABLED, FinalItemImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
			FinalItemModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
			FinalItemModelImpl.FINDER_CACHE_ENABLED, FinalItemImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
			FinalItemModelImpl.FINDER_CACHE_ENABLED, FinalItemImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			FinalItemModelImpl.UUID_COLUMN_BITMASK |
			FinalItemModelImpl.NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
			FinalItemModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the final items where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching final items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FinalItem> findByUuid(String uuid) throws SystemException {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the final items where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of final items
	 * @param end the upper bound of the range of final items (not inclusive)
	 * @return the range of matching final items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FinalItem> findByUuid(String uuid, int start, int end)
		throws SystemException {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the final items where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of final items
	 * @param end the upper bound of the range of final items (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching final items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FinalItem> findByUuid(String uuid, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<FinalItem> list = (List<FinalItem>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (FinalItem finalItem : list) {
				if (!Validator.equals(uuid, finalItem.getUuid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_FINALITEM_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(FinalItemModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<FinalItem>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<FinalItem>(list);
				}
				else {
					list = (List<FinalItem>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first final item in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching final item
	 * @throws com.liferay.order.NoSuchFinalItemException if a matching final item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem findByUuid_First(String uuid,
		OrderByComparator orderByComparator)
		throws NoSuchFinalItemException, SystemException {
		FinalItem finalItem = fetchByUuid_First(uuid, orderByComparator);

		if (finalItem != null) {
			return finalItem;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchFinalItemException(msg.toString());
	}

	/**
	 * Returns the first final item in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching final item, or <code>null</code> if a matching final item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem fetchByUuid_First(String uuid,
		OrderByComparator orderByComparator) throws SystemException {
		List<FinalItem> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last final item in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching final item
	 * @throws com.liferay.order.NoSuchFinalItemException if a matching final item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem findByUuid_Last(String uuid,
		OrderByComparator orderByComparator)
		throws NoSuchFinalItemException, SystemException {
		FinalItem finalItem = fetchByUuid_Last(uuid, orderByComparator);

		if (finalItem != null) {
			return finalItem;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchFinalItemException(msg.toString());
	}

	/**
	 * Returns the last final item in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching final item, or <code>null</code> if a matching final item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem fetchByUuid_Last(String uuid,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<FinalItem> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the final items before and after the current final item in the ordered set where uuid = &#63;.
	 *
	 * @param finalItemId the primary key of the current final item
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next final item
	 * @throws com.liferay.order.NoSuchFinalItemException if a final item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem[] findByUuid_PrevAndNext(long finalItemId, String uuid,
		OrderByComparator orderByComparator)
		throws NoSuchFinalItemException, SystemException {
		FinalItem finalItem = findByPrimaryKey(finalItemId);

		Session session = null;

		try {
			session = openSession();

			FinalItem[] array = new FinalItemImpl[3];

			array[0] = getByUuid_PrevAndNext(session, finalItem, uuid,
					orderByComparator, true);

			array[1] = finalItem;

			array[2] = getByUuid_PrevAndNext(session, finalItem, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected FinalItem getByUuid_PrevAndNext(Session session,
		FinalItem finalItem, String uuid, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_FINALITEM_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(FinalItemModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(finalItem);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<FinalItem> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the final items where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUuid(String uuid) throws SystemException {
		for (FinalItem finalItem : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(finalItem);
		}
	}

	/**
	 * Returns the number of final items where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching final items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUuid(String uuid) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_FINALITEM_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "finalItem.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "finalItem.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(finalItem.uuid IS NULL OR finalItem.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
			FinalItemModelImpl.FINDER_CACHE_ENABLED, FinalItemImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() },
			FinalItemModelImpl.UUID_COLUMN_BITMASK |
			FinalItemModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
			FinalItemModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns the final item where uuid = &#63; and groupId = &#63; or throws a {@link com.liferay.order.NoSuchFinalItemException} if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching final item
	 * @throws com.liferay.order.NoSuchFinalItemException if a matching final item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem findByUUID_G(String uuid, long groupId)
		throws NoSuchFinalItemException, SystemException {
		FinalItem finalItem = fetchByUUID_G(uuid, groupId);

		if (finalItem == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchFinalItemException(msg.toString());
		}

		return finalItem;
	}

	/**
	 * Returns the final item where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching final item, or <code>null</code> if a matching final item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem fetchByUUID_G(String uuid, long groupId)
		throws SystemException {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the final item where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching final item, or <code>null</code> if a matching final item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { uuid, groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs, this);
		}

		if (result instanceof FinalItem) {
			FinalItem finalItem = (FinalItem)result;

			if (!Validator.equals(uuid, finalItem.getUuid()) ||
					(groupId != finalItem.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_FINALITEM_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<FinalItem> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
						finderArgs, list);
				}
				else {
					FinalItem finalItem = list.get(0);

					result = finalItem;

					cacheResult(finalItem);

					if ((finalItem.getUuid() == null) ||
							!finalItem.getUuid().equals(uuid) ||
							(finalItem.getGroupId() != groupId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
							finderArgs, finalItem);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (FinalItem)result;
		}
	}

	/**
	 * Removes the final item where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the final item that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem removeByUUID_G(String uuid, long groupId)
		throws NoSuchFinalItemException, SystemException {
		FinalItem finalItem = findByUUID_G(uuid, groupId);

		return remove(finalItem);
	}

	/**
	 * Returns the number of final items where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching final items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;

		Object[] finderArgs = new Object[] { uuid, groupId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_FINALITEM_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "finalItem.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "finalItem.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(finalItem.uuid IS NULL OR finalItem.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "finalItem.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
			FinalItemModelImpl.FINDER_CACHE_ENABLED, FinalItemImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
			FinalItemModelImpl.FINDER_CACHE_ENABLED, FinalItemImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			FinalItemModelImpl.UUID_COLUMN_BITMASK |
			FinalItemModelImpl.COMPANYID_COLUMN_BITMASK |
			FinalItemModelImpl.NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
			FinalItemModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the final items where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching final items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FinalItem> findByUuid_C(String uuid, long companyId)
		throws SystemException {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the final items where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of final items
	 * @param end the upper bound of the range of final items (not inclusive)
	 * @return the range of matching final items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FinalItem> findByUuid_C(String uuid, long companyId, int start,
		int end) throws SystemException {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the final items where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of final items
	 * @param end the upper bound of the range of final items (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching final items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FinalItem> findByUuid_C(String uuid, long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<FinalItem> list = (List<FinalItem>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (FinalItem finalItem : list) {
				if (!Validator.equals(uuid, finalItem.getUuid()) ||
						(companyId != finalItem.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_FINALITEM_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(FinalItemModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<FinalItem>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<FinalItem>(list);
				}
				else {
					list = (List<FinalItem>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first final item in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching final item
	 * @throws com.liferay.order.NoSuchFinalItemException if a matching final item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem findByUuid_C_First(String uuid, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchFinalItemException, SystemException {
		FinalItem finalItem = fetchByUuid_C_First(uuid, companyId,
				orderByComparator);

		if (finalItem != null) {
			return finalItem;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchFinalItemException(msg.toString());
	}

	/**
	 * Returns the first final item in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching final item, or <code>null</code> if a matching final item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<FinalItem> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last final item in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching final item
	 * @throws com.liferay.order.NoSuchFinalItemException if a matching final item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchFinalItemException, SystemException {
		FinalItem finalItem = fetchByUuid_C_Last(uuid, companyId,
				orderByComparator);

		if (finalItem != null) {
			return finalItem;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchFinalItemException(msg.toString());
	}

	/**
	 * Returns the last final item in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching final item, or <code>null</code> if a matching final item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<FinalItem> list = findByUuid_C(uuid, companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the final items before and after the current final item in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param finalItemId the primary key of the current final item
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next final item
	 * @throws com.liferay.order.NoSuchFinalItemException if a final item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem[] findByUuid_C_PrevAndNext(long finalItemId, String uuid,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchFinalItemException, SystemException {
		FinalItem finalItem = findByPrimaryKey(finalItemId);

		Session session = null;

		try {
			session = openSession();

			FinalItem[] array = new FinalItemImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, finalItem, uuid,
					companyId, orderByComparator, true);

			array[1] = finalItem;

			array[2] = getByUuid_C_PrevAndNext(session, finalItem, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected FinalItem getByUuid_C_PrevAndNext(Session session,
		FinalItem finalItem, String uuid, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_FINALITEM_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(FinalItemModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(finalItem);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<FinalItem> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the final items where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId)
		throws SystemException {
		for (FinalItem finalItem : findByUuid_C(uuid, companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(finalItem);
		}
	}

	/**
	 * Returns the number of final items where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching final items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_FINALITEM_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "finalItem.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "finalItem.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(finalItem.uuid IS NULL OR finalItem.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "finalItem.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORDERID = new FinderPath(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
			FinalItemModelImpl.FINDER_CACHE_ENABLED, FinalItemImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByOrderId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERID =
		new FinderPath(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
			FinalItemModelImpl.FINDER_CACHE_ENABLED, FinalItemImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByOrderId",
			new String[] { Long.class.getName() },
			FinalItemModelImpl.ORDERID_COLUMN_BITMASK |
			FinalItemModelImpl.NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORDERID = new FinderPath(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
			FinalItemModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOrderId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the final items where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @return the matching final items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FinalItem> findByOrderId(long orderId)
		throws SystemException {
		return findByOrderId(orderId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the final items where orderId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param orderId the order ID
	 * @param start the lower bound of the range of final items
	 * @param end the upper bound of the range of final items (not inclusive)
	 * @return the range of matching final items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FinalItem> findByOrderId(long orderId, int start, int end)
		throws SystemException {
		return findByOrderId(orderId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the final items where orderId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param orderId the order ID
	 * @param start the lower bound of the range of final items
	 * @param end the upper bound of the range of final items (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching final items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FinalItem> findByOrderId(long orderId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERID;
			finderArgs = new Object[] { orderId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORDERID;
			finderArgs = new Object[] { orderId, start, end, orderByComparator };
		}

		List<FinalItem> list = (List<FinalItem>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (FinalItem finalItem : list) {
				if ((orderId != finalItem.getOrderId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_FINALITEM_WHERE);

			query.append(_FINDER_COLUMN_ORDERID_ORDERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(FinalItemModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(orderId);

				if (!pagination) {
					list = (List<FinalItem>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<FinalItem>(list);
				}
				else {
					list = (List<FinalItem>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first final item in the ordered set where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching final item
	 * @throws com.liferay.order.NoSuchFinalItemException if a matching final item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem findByOrderId_First(long orderId,
		OrderByComparator orderByComparator)
		throws NoSuchFinalItemException, SystemException {
		FinalItem finalItem = fetchByOrderId_First(orderId, orderByComparator);

		if (finalItem != null) {
			return finalItem;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("orderId=");
		msg.append(orderId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchFinalItemException(msg.toString());
	}

	/**
	 * Returns the first final item in the ordered set where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching final item, or <code>null</code> if a matching final item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem fetchByOrderId_First(long orderId,
		OrderByComparator orderByComparator) throws SystemException {
		List<FinalItem> list = findByOrderId(orderId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last final item in the ordered set where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching final item
	 * @throws com.liferay.order.NoSuchFinalItemException if a matching final item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem findByOrderId_Last(long orderId,
		OrderByComparator orderByComparator)
		throws NoSuchFinalItemException, SystemException {
		FinalItem finalItem = fetchByOrderId_Last(orderId, orderByComparator);

		if (finalItem != null) {
			return finalItem;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("orderId=");
		msg.append(orderId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchFinalItemException(msg.toString());
	}

	/**
	 * Returns the last final item in the ordered set where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching final item, or <code>null</code> if a matching final item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem fetchByOrderId_Last(long orderId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByOrderId(orderId);

		if (count == 0) {
			return null;
		}

		List<FinalItem> list = findByOrderId(orderId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the final items before and after the current final item in the ordered set where orderId = &#63;.
	 *
	 * @param finalItemId the primary key of the current final item
	 * @param orderId the order ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next final item
	 * @throws com.liferay.order.NoSuchFinalItemException if a final item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem[] findByOrderId_PrevAndNext(long finalItemId,
		long orderId, OrderByComparator orderByComparator)
		throws NoSuchFinalItemException, SystemException {
		FinalItem finalItem = findByPrimaryKey(finalItemId);

		Session session = null;

		try {
			session = openSession();

			FinalItem[] array = new FinalItemImpl[3];

			array[0] = getByOrderId_PrevAndNext(session, finalItem, orderId,
					orderByComparator, true);

			array[1] = finalItem;

			array[2] = getByOrderId_PrevAndNext(session, finalItem, orderId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected FinalItem getByOrderId_PrevAndNext(Session session,
		FinalItem finalItem, long orderId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_FINALITEM_WHERE);

		query.append(_FINDER_COLUMN_ORDERID_ORDERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(FinalItemModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(orderId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(finalItem);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<FinalItem> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the final items where orderId = &#63; from the database.
	 *
	 * @param orderId the order ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByOrderId(long orderId) throws SystemException {
		for (FinalItem finalItem : findByOrderId(orderId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(finalItem);
		}
	}

	/**
	 * Returns the number of final items where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @return the number of matching final items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByOrderId(long orderId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ORDERID;

		Object[] finderArgs = new Object[] { orderId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_FINALITEM_WHERE);

			query.append(_FINDER_COLUMN_ORDERID_ORDERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(orderId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ORDERID_ORDERID_2 = "finalItem.orderId = ?";

	public FinalItemPersistenceImpl() {
		setModelClass(FinalItem.class);
	}

	/**
	 * Caches the final item in the entity cache if it is enabled.
	 *
	 * @param finalItem the final item
	 */
	@Override
	public void cacheResult(FinalItem finalItem) {
		EntityCacheUtil.putResult(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
			FinalItemImpl.class, finalItem.getPrimaryKey(), finalItem);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
			new Object[] { finalItem.getUuid(), finalItem.getGroupId() },
			finalItem);

		finalItem.resetOriginalValues();
	}

	/**
	 * Caches the final items in the entity cache if it is enabled.
	 *
	 * @param finalItems the final items
	 */
	@Override
	public void cacheResult(List<FinalItem> finalItems) {
		for (FinalItem finalItem : finalItems) {
			if (EntityCacheUtil.getResult(
						FinalItemModelImpl.ENTITY_CACHE_ENABLED,
						FinalItemImpl.class, finalItem.getPrimaryKey()) == null) {
				cacheResult(finalItem);
			}
			else {
				finalItem.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all final items.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(FinalItemImpl.class.getName());
		}

		EntityCacheUtil.clearCache(FinalItemImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the final item.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(FinalItem finalItem) {
		EntityCacheUtil.removeResult(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
			FinalItemImpl.class, finalItem.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(finalItem);
	}

	@Override
	public void clearCache(List<FinalItem> finalItems) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (FinalItem finalItem : finalItems) {
			EntityCacheUtil.removeResult(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
				FinalItemImpl.class, finalItem.getPrimaryKey());

			clearUniqueFindersCache(finalItem);
		}
	}

	protected void cacheUniqueFindersCache(FinalItem finalItem) {
		if (finalItem.isNew()) {
			Object[] args = new Object[] {
					finalItem.getUuid(), finalItem.getGroupId()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
				finalItem);
		}
		else {
			FinalItemModelImpl finalItemModelImpl = (FinalItemModelImpl)finalItem;

			if ((finalItemModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						finalItem.getUuid(), finalItem.getGroupId()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
					finalItem);
			}
		}
	}

	protected void clearUniqueFindersCache(FinalItem finalItem) {
		FinalItemModelImpl finalItemModelImpl = (FinalItemModelImpl)finalItem;

		Object[] args = new Object[] { finalItem.getUuid(), finalItem.getGroupId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);

		if ((finalItemModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			args = new Object[] {
					finalItemModelImpl.getOriginalUuid(),
					finalItemModelImpl.getOriginalGroupId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}
	}

	/**
	 * Creates a new final item with the primary key. Does not add the final item to the database.
	 *
	 * @param finalItemId the primary key for the new final item
	 * @return the new final item
	 */
	@Override
	public FinalItem create(long finalItemId) {
		FinalItem finalItem = new FinalItemImpl();

		finalItem.setNew(true);
		finalItem.setPrimaryKey(finalItemId);

		String uuid = PortalUUIDUtil.generate();

		finalItem.setUuid(uuid);

		return finalItem;
	}

	/**
	 * Removes the final item with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param finalItemId the primary key of the final item
	 * @return the final item that was removed
	 * @throws com.liferay.order.NoSuchFinalItemException if a final item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem remove(long finalItemId)
		throws NoSuchFinalItemException, SystemException {
		return remove((Serializable)finalItemId);
	}

	/**
	 * Removes the final item with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the final item
	 * @return the final item that was removed
	 * @throws com.liferay.order.NoSuchFinalItemException if a final item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem remove(Serializable primaryKey)
		throws NoSuchFinalItemException, SystemException {
		Session session = null;

		try {
			session = openSession();

			FinalItem finalItem = (FinalItem)session.get(FinalItemImpl.class,
					primaryKey);

			if (finalItem == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchFinalItemException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(finalItem);
		}
		catch (NoSuchFinalItemException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected FinalItem removeImpl(FinalItem finalItem)
		throws SystemException {
		finalItem = toUnwrappedModel(finalItem);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(finalItem)) {
				finalItem = (FinalItem)session.get(FinalItemImpl.class,
						finalItem.getPrimaryKeyObj());
			}

			if (finalItem != null) {
				session.delete(finalItem);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (finalItem != null) {
			clearCache(finalItem);
		}

		return finalItem;
	}

	@Override
	public FinalItem updateImpl(com.liferay.order.model.FinalItem finalItem)
		throws SystemException {
		finalItem = toUnwrappedModel(finalItem);

		boolean isNew = finalItem.isNew();

		FinalItemModelImpl finalItemModelImpl = (FinalItemModelImpl)finalItem;

		if (Validator.isNull(finalItem.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			finalItem.setUuid(uuid);
		}

		Session session = null;

		try {
			session = openSession();

			if (finalItem.isNew()) {
				session.save(finalItem);

				finalItem.setNew(false);
			}
			else {
				session.merge(finalItem);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !FinalItemModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((finalItemModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						finalItemModelImpl.getOriginalUuid()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { finalItemModelImpl.getUuid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((finalItemModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						finalItemModelImpl.getOriginalUuid(),
						finalItemModelImpl.getOriginalCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						finalItemModelImpl.getUuid(),
						finalItemModelImpl.getCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((finalItemModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						finalItemModelImpl.getOriginalOrderId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORDERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERID,
					args);

				args = new Object[] { finalItemModelImpl.getOrderId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORDERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERID,
					args);
			}
		}

		EntityCacheUtil.putResult(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
			FinalItemImpl.class, finalItem.getPrimaryKey(), finalItem);

		clearUniqueFindersCache(finalItem);
		cacheUniqueFindersCache(finalItem);

		return finalItem;
	}

	protected FinalItem toUnwrappedModel(FinalItem finalItem) {
		if (finalItem instanceof FinalItemImpl) {
			return finalItem;
		}

		FinalItemImpl finalItemImpl = new FinalItemImpl();

		finalItemImpl.setNew(finalItem.isNew());
		finalItemImpl.setPrimaryKey(finalItem.getPrimaryKey());

		finalItemImpl.setUuid(finalItem.getUuid());
		finalItemImpl.setFinalItemId(finalItem.getFinalItemId());
		finalItemImpl.setCompanyId(finalItem.getCompanyId());
		finalItemImpl.setGroupId(finalItem.getGroupId());
		finalItemImpl.setUserId(finalItem.getUserId());
		finalItemImpl.setOrderId(finalItem.getOrderId());
		finalItemImpl.setCategoryId(finalItem.getCategoryId());
		finalItemImpl.setName(finalItem.getName());
		finalItemImpl.setBasePrice(finalItem.getBasePrice());
		finalItemImpl.setTotalPrice(finalItem.getTotalPrice());
		finalItemImpl.setQuantity(finalItem.getQuantity());
		finalItemImpl.setImported(finalItem.isImported());
		finalItemImpl.setTax(finalItem.getTax());
		finalItemImpl.setDuty(finalItem.getDuty());

		return finalItemImpl;
	}

	/**
	 * Returns the final item with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the final item
	 * @return the final item
	 * @throws com.liferay.order.NoSuchFinalItemException if a final item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem findByPrimaryKey(Serializable primaryKey)
		throws NoSuchFinalItemException, SystemException {
		FinalItem finalItem = fetchByPrimaryKey(primaryKey);

		if (finalItem == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchFinalItemException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return finalItem;
	}

	/**
	 * Returns the final item with the primary key or throws a {@link com.liferay.order.NoSuchFinalItemException} if it could not be found.
	 *
	 * @param finalItemId the primary key of the final item
	 * @return the final item
	 * @throws com.liferay.order.NoSuchFinalItemException if a final item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem findByPrimaryKey(long finalItemId)
		throws NoSuchFinalItemException, SystemException {
		return findByPrimaryKey((Serializable)finalItemId);
	}

	/**
	 * Returns the final item with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the final item
	 * @return the final item, or <code>null</code> if a final item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		FinalItem finalItem = (FinalItem)EntityCacheUtil.getResult(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
				FinalItemImpl.class, primaryKey);

		if (finalItem == _nullFinalItem) {
			return null;
		}

		if (finalItem == null) {
			Session session = null;

			try {
				session = openSession();

				finalItem = (FinalItem)session.get(FinalItemImpl.class,
						primaryKey);

				if (finalItem != null) {
					cacheResult(finalItem);
				}
				else {
					EntityCacheUtil.putResult(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
						FinalItemImpl.class, primaryKey, _nullFinalItem);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(FinalItemModelImpl.ENTITY_CACHE_ENABLED,
					FinalItemImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return finalItem;
	}

	/**
	 * Returns the final item with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param finalItemId the primary key of the final item
	 * @return the final item, or <code>null</code> if a final item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FinalItem fetchByPrimaryKey(long finalItemId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)finalItemId);
	}

	/**
	 * Returns all the final items.
	 *
	 * @return the final items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FinalItem> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the final items.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of final items
	 * @param end the upper bound of the range of final items (not inclusive)
	 * @return the range of final items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FinalItem> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the final items.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of final items
	 * @param end the upper bound of the range of final items (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of final items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FinalItem> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<FinalItem> list = (List<FinalItem>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_FINALITEM);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_FINALITEM;

				if (pagination) {
					sql = sql.concat(FinalItemModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<FinalItem>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<FinalItem>(list);
				}
				else {
					list = (List<FinalItem>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the final items from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (FinalItem finalItem : findAll()) {
			remove(finalItem);
		}
	}

	/**
	 * Returns the number of final items.
	 *
	 * @return the number of final items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_FINALITEM);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the final item persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.liferay.order.model.FinalItem")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<FinalItem>> listenersList = new ArrayList<ModelListener<FinalItem>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<FinalItem>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(FinalItemImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_FINALITEM = "SELECT finalItem FROM FinalItem finalItem";
	private static final String _SQL_SELECT_FINALITEM_WHERE = "SELECT finalItem FROM FinalItem finalItem WHERE ";
	private static final String _SQL_COUNT_FINALITEM = "SELECT COUNT(finalItem) FROM FinalItem finalItem";
	private static final String _SQL_COUNT_FINALITEM_WHERE = "SELECT COUNT(finalItem) FROM FinalItem finalItem WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "finalItem.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No FinalItem exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No FinalItem exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(FinalItemPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
	private static FinalItem _nullFinalItem = new FinalItemImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<FinalItem> toCacheModel() {
				return _nullFinalItemCacheModel;
			}
		};

	private static CacheModel<FinalItem> _nullFinalItemCacheModel = new CacheModel<FinalItem>() {
			@Override
			public FinalItem toEntityModel() {
				return _nullFinalItem;
			}
		};
}