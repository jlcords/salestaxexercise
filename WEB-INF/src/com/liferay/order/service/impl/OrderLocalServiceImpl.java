/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service.impl;

import com.liferay.order.model.InventoryItem;
import com.liferay.order.model.Order;
import com.liferay.order.service.InventoryItemLocalServiceUtil;
import com.liferay.order.service.OrderLocalServiceUtil;
import com.liferay.order.service.base.OrderLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.service.ContactLocalServiceUtil;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * The implementation of the order local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.liferay.order.service.OrderLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Joshua Cords
 * @see com.liferay.order.service.base.OrderLocalServiceBaseImpl
 * @see com.liferay.order.service.OrderLocalServiceUtil
 */
public class OrderLocalServiceImpl extends OrderLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.liferay.order.service.OrderLocalServiceUtil} to access the order local service.
	 */
	
	/**
	 * Adds new Order to database
	 * @param companyId
	 * @param groupId
	 * @param userId
	 * @param contactId
	 * @param userName
	 * @param street1
	 * @param street2
	 * @param street3
	 * @param city
	 * @param regionId
	 * @param countryId
	 * @param zip
	 * @param confirmed
	 * @throws IOException
	 * 
	 */
	public Order addOrder(
			long companyId, long groupId, long userId, boolean confirmed
			) throws com.liferay.portal.kernel.exception.SystemException {

		Order order = 
				OrderLocalServiceUtil.createOrder(0);
		long orderId = 
				counterLocalService.increment(Order.class.getName());

		order.setOrderId(orderId);
		order.setCompanyId(companyId);
		order.setGroupId(groupId);
		order.setUserId(userId);
		order.setConfirmed(confirmed);
		order.setCreateDate(new Date());

		return super.addOrder(order);
	}

	/**
	 * Creates a Order with a new orderId without other data
	 * @return EmptyOrder with new orderId
	 */
	public Order createOrder(){
		Order order = 
				OrderLocalServiceUtil.createOrder(0);
		long orderId;
		try {
			orderId = counterLocalService.increment(Order.class.getName());
			order.setOrderId(orderId);
		} catch (SystemException e) {
			_log.error("Failed to create empty Order", e);
			return null;
		}
		return order;
	}

	/**
	 * Updates Order in database
	 * @param companyId
	 * @param groupId
	 * @param userId
	 * @param orderId
	 * @param contactId
	 * @param userName
	 * @param street1
	 * @param street2
	 * @param street3
	 * @param city
	 * @param regionId
	 * @param countryId
	 * @param zip
	 * @param confirmed
	 * @throws IOException
	 * 
	 */
	public Order updateOrder(
			long companyId, long groupId, long userId, long orderId, boolean confirmed
			) throws com.liferay.portal.kernel.exception.SystemException {

		Order order = null;
		
		order = OrderLocalServiceUtil.fetchOrder(orderId);
		order.setOrderId(orderId);
		order.setCompanyId(companyId);
		order.setGroupId(groupId);
		order.setUserId(userId);
		order.setConfirmed(confirmed);
		order.setCreateDate(new Date());
		
		return super.updateOrder(order);
	}
	
	/**
	 * Gets a list with a range of Orders from a user
	 *
	 */
	public List<Order> getOrdersByUserId(long userId, int start, int end
			) throws SystemException {
		
		return orderPersistence.findByUserId(userId, start, end);
	} 
	
	/**
	 * Gets the number of Orders from a user
	 *
	 */
	public int getOrdersCountByUserId(long userId) throws SystemException {
		
		return orderPersistence.countByUserId(userId);
	}
	
	private static Log _log = LogFactoryUtil
			.getLog(OrderLocalServiceImpl.class);
	
}