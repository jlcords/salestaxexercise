/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service.impl;

import com.liferay.order.model.Category;
import com.liferay.order.model.InventoryItem;
import com.liferay.order.service.CategoryLocalServiceUtil;
import com.liferay.order.service.base.CategoryLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.List;

/**
 * The implementation of the category local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.liferay.order.service.CategoryLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Joshua Cords
 * @see com.liferay.order.service.base.CategoryLocalServiceBaseImpl
 * @see com.liferay.order.service.CategoryLocalServiceUtil
 */
public class CategoryLocalServiceImpl extends CategoryLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.liferay.order.service.CategoryLocalServiceUtil} to access the category local service.
	 */
	
	/**
	 * Creates and adds Category to database
	 * @param companyId
	 * @param groupId
	 * @param name
	 * @param taxable
	 * @return Category
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public Category addCategory(
			long companyId, long groupId, String name, boolean taxable
			) throws com.liferay.portal.kernel.exception.SystemException {

		Category category = 
				CategoryLocalServiceUtil.createCategory(0);

		long inventoryItemId = 
				counterLocalService.increment(InventoryItem.class.getName());

		category.setCategoryId(inventoryItemId);
		category.setCompanyId(companyId);
		category.setGroupId(groupId);
		category.setName(name);
		category.setTaxable(taxable);

		return super.addCategory(category);
	}

	/**
	 * Updates Category in database
	 * @param companyId
	 * @param groupId
	 * @param categoryId
	 * @param name
	 * @param taxable
	 * @return Category
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public Category updateCategory(
			long companyId, long groupId, long categoryId, 
			String name, boolean taxable
			) throws com.liferay.portal.kernel.exception.SystemException {

		Category category = null;
		
		if(categoryId > 0){
			try {
				category = CategoryLocalServiceUtil
						.fetchCategory(categoryId);
				
				category.setCompanyId(companyId);
				category.setGroupId(groupId);
				category.setName(name);
				category.setTaxable(taxable);				
			} catch (SystemException e) {
				_log.error("Failed to fetch Category with id " + categoryId, e);
				return null;
			}
		}
		return super.updateCategory(category);
	}
	
	/**
	 * Gets a list with all the Categories in a company and group
	 *
	 */
	public List<Category> getCategoriesByGroupId(long groupId) throws SystemException {
		
		return categoryPersistence.findByGroupId(groupId);
	}

	/**
	 * Gets a list with a range of Categories from a company and group
	 *
	 */
	public List<Category> getCategoriesByGroupId(long groupId, int start, int end) throws SystemException {
		
		return categoryPersistence.findByGroupId(groupId, start, end);
	}

	/**
	 * Gets the number of Categories in a company and group
	 *
	 */
	public int getCategoriesCountByGroupId(long groupId) throws SystemException {
		
		return categoryPersistence.countByGroupId(groupId);
	}
	
	private static Log _log = LogFactoryUtil.getLog(CategoryLocalServiceImpl.class);
}