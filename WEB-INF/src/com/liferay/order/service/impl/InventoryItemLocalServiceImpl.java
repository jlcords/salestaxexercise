/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service.impl;


import com.liferay.order.model.InventoryItem;
import com.liferay.order.service.InventoryItemLocalServiceUtil;
import com.liferay.order.service.base.InventoryItemLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.List;

/**
 * The implementation of the inventory item local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.liferay.order.service.InventoryItemLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Joshua Cords
 * @see com.liferay.order.service.base.InventoryItemLocalServiceBaseImpl
 * @see com.liferay.order.service.InventoryItemLocalServiceUtil
 */
public class InventoryItemLocalServiceImpl
	extends InventoryItemLocalServiceBaseImpl {
	
	/**
	 * Creates and adds InventoryItem to database
	 * @param companyId
	 * @param groupId
	 * @param userId
	 * @param name
	 * @param quantity
	 * @param basePrice
	 * @param categoryId
	 * @param imported
	 * @return InventoryItem
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public InventoryItem addInventoryItem(
			long companyId, long groupId, long userId, String name, 
			int quantity, double basePrice, long categoryId, boolean imported
			) throws com.liferay.portal.kernel.exception.SystemException {

		InventoryItem inventoryItem = 
				InventoryItemLocalServiceUtil.createInventoryItem(0);

		long inventoryItemId = 
				counterLocalService.increment(InventoryItem.class.getName());

		inventoryItem.setInventoryItemId(inventoryItemId);
		inventoryItem.setCompanyId(companyId);
		inventoryItem.setGroupId(groupId);
		inventoryItem.setUserId(userId);
		inventoryItem.setName(name);
		inventoryItem.setBasePrice(basePrice);
		inventoryItem.setQuantity(quantity);
		inventoryItem.setCategoryId(categoryId);
		inventoryItem.setImported(imported);

		return super.addInventoryItem(inventoryItem);
	}

	/**
	 * Updates InventoryItem in database
	 * @param inventoryItem
	 * @return InventoryItem
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public InventoryItem updateInventoryItem(
			long companyId, long groupId, long userId, long inventoryItemId,
			String name, int quantity, double basePrice, long categoryId, 
			boolean imported
			) throws com.liferay.portal.kernel.exception.SystemException {

		InventoryItem inventoryItem = null;
		
		if(inventoryItemId > 0){
			try {
				inventoryItem = InventoryItemLocalServiceUtil
						.fetchInventoryItem(inventoryItemId);
				inventoryItem.setCompanyId(companyId);
				inventoryItem.setGroupId(groupId);
				inventoryItem.setUserId(userId);
				inventoryItem.setName(name);
				inventoryItem.setBasePrice(basePrice);
				inventoryItem.setQuantity(quantity);
				inventoryItem.setCategoryId(categoryId);
				inventoryItem.setImported(imported);
				
			} catch (SystemException e) {
				_log.error("Failed to fetch item with id " + inventoryItemId, e);
				return null;				
			}
		}
		
		return super.updateInventoryItem(inventoryItem);
	}
	
	/**
	 * Gets a list with all the InventoryItems in a group
	 * @param groupId
	 * @return List<InventoryItem>
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public List<InventoryItem> getInventoryItemsByGroupId(long groupId
			) throws SystemException {
		
		return inventoryItemPersistence.findByGroupId(groupId);
	}

	/**
	 * Gets a list with a range of InventoryItems from a group
	 * @param groupId
	 * @param start
	 * @param end
	 * @return List<InventoryItem>
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public List<InventoryItem> getInventoryItemsByGroupId(
			long groupId, int start, int end) throws SystemException {
		return inventoryItemPersistence.findByGroupId(groupId, start, end);
	}

	/**
	 * Gets the number of InventoryItems in a group
	 * @param groupId
	 * @return int
	 */
	public int getInventoryItemsCountByGroupId(long groupId
			) throws SystemException {
		return inventoryItemPersistence.countByGroupId(groupId);
	}
	
	private static Log _log = LogFactoryUtil
			.getLog(InventoryItemLocalServiceImpl.class);
	
}