/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service.impl;

import com.liferay.order.NoSuchTaxDutyException;
import com.liferay.order.model.FinalItem;
import com.liferay.order.model.InventoryItem;
import com.liferay.order.model.TaxDuty;
import com.liferay.order.service.CategoryLocalServiceUtil;
import com.liferay.order.service.FinalItemLocalServiceUtil;
import com.liferay.order.service.TaxDutyLocalServiceUtil;
import com.liferay.order.service.base.FinalItemLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.List;

/**
 * The implementation of the final item local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.liferay.order.service.FinalItemLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Joshua Cords
 * @see com.liferay.order.service.base.FinalItemLocalServiceBaseImpl
 * @see com.liferay.order.service.FinalItemLocalServiceUtil
 */
public class FinalItemLocalServiceImpl extends FinalItemLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.liferay.order.service.FinalItemLocalServiceUtil} to access the final item local service.
	 */
	
	/**
	 * Creates and adds FinalItem to database based on an InventoryItem
	 * @param companyId
	 * @param groupId
	 * @param userId
	 * @param name
	 * @param quantity
	 * @param basePrice
	 * @param categoryId
	 * @param imported
	 * @return InventoryItem
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public FinalItem addFinalItem(
			InventoryItem inventoryItem, long companyId, long groupId, 
			long userId, long orderId, int quantity, double taxPercent, 
			double dutyPercent
			) throws com.liferay.portal.kernel.exception.SystemException {
		
		FinalItem finalItem = 
				FinalItemLocalServiceUtil.createFinalItem(0);
		
		TaxDuty taxDuty = null;
		taxDuty = TaxDutyLocalServiceUtil.getTaxDutybyGroupId(groupId);

		long finalItemId = 
				counterLocalService.increment(FinalItem.class.getName());

		finalItem.setFinalItemId(finalItemId);
		finalItem.setCompanyId(companyId);
		finalItem.setGroupId(groupId);
		finalItem.setUserId(userId);
		finalItem.setOrderId(orderId);
		finalItem.setQuantity(quantity);
		convertItem(inventoryItem, finalItem);
		calculateTaxDuty(finalItem,taxDuty);
		setTotalPrice(finalItem);
		return super.addFinalItem(finalItem);
	}
	
	/**
	 * Returns all items in an Order
	 * @param orderId
	 * @param start
	 * @param end
	 * @return
	 * @throws SystemException
	 */
	public List<FinalItem> getFinalItemsbyOrderId(
			long orderId) throws SystemException{
		return finalItemPersistence.findByOrderId(orderId);
	}
	
	/**
	 * Returns all items in an Order
	 * @param orderId
	 * @param start
	 * @param end
	 * @return
	 * @throws SystemException
	 */
	public List<FinalItem> getFinalItemsbyOrderId(
			long orderId, int start, int end) throws SystemException{
		return finalItemPersistence.findByOrderId(orderId, start, end);
	}
	
	/**
	 * Return number of items in an Order
	 * @param orderId
	 * @return
	 * @throws SystemException
	 */
	public int getFinalItemsCountbyOrderId(long orderId) throws SystemException{
		return finalItemPersistence.countByOrderId(orderId);
	}
	
	/**
	 * Gets dollar amount of Duty for entire Order
	 * @param orderId
	 * @return
	 * @throws SystemException
	 */
	public double getDutySumbyOrderId(long orderId) throws SystemException{
		List<FinalItem> items = finalItemLocalService.getFinalItemsbyOrderId(orderId);
		double sum = 0;
		for(FinalItem item: items){
			sum += item.getDuty();
		}
		return sum;
	}

	/**
	 * Gets dollar amount of Taxes for entire Order
	 * @param orderId
	 * @return
	 * @throws SystemException
	 */
	public double getTaxSumbyOrderId(long orderId) throws SystemException{
		List<FinalItem> items = finalItemLocalService.getFinalItemsbyOrderId(orderId);
		double sum = 0;
		for(FinalItem item: items){
			sum += item.getTax();
		}
		return sum;
	}
	
	/**
	 * Gets total price for entire Order
	 * @param orderId
	 * @return
	 * @throws SystemException
	 */
	public double getTotalPriceSumbyOrderId(long orderId) throws SystemException{
		List<FinalItem> items = finalItemLocalService.getFinalItemsbyOrderId(orderId);
		double sum = 0;
		for(FinalItem item: items){
			sum += item.getTotalPrice();
		}
		return sum;
	}
	
	/**
	 * Pulls all relevant data from InventoryItem to set the FinalItem
	 * @param inventoryItem
	 * @param finalItem
	 */
	private void convertItem(InventoryItem inventoryItem,
			FinalItem finalItem) {
		finalItem.setBasePrice(inventoryItem.getBasePrice());
		finalItem.setName(inventoryItem.getName());
		finalItem.setCategoryId(inventoryItem.getCategoryId());
		finalItem.setBasePrice(inventoryItem.getBasePrice());
		finalItem.setImported(inventoryItem.getImported());
		return;
	}
	
	/**
	 * Calculates and rounds up tax and duty
	 * @param finalItem
	 * @param taxPercent
	 * @param dutyPercent
	 */
	private void calculateTaxDuty(FinalItem finalItem, TaxDuty taxDuty){
		
		if(finalItem.isImported()){
			finalItem.setDuty(
					round(taxDuty, finalItem.getBasePrice()*taxDuty.getDuty())
					/100);
		} else {
			finalItem.setDuty(0);
		}
		
		try {
			if(CategoryLocalServiceUtil
					.getCategory(finalItem.getCategoryId()).getTaxable())
			{
				finalItem.setTax(
						round(taxDuty, finalItem.getBasePrice()*taxDuty.getTax())
						/100);
			} else {
				finalItem.setTax(0);
			}
		} catch (PortalException e) {
			_log.error("Cannot get Category.getTaxable for "
								+ finalItem.getCategoryId());
		} catch (SystemException e) {
			_log.error("Cannot get Category.getTaxable for "
					+ finalItem.getCategoryId());
		}
	}
	
	/**
	 * Sets totalPrice in FinalItem
	 * @param finalItem
	 */
	private void setTotalPrice(FinalItem finalItem){
		double totalPrice = finalItem.getBasePrice()+finalItem.getDuty()+finalItem.getTax();
		totalPrice *= finalItem.getQuantity();
		finalItem.setTotalPrice(totalPrice);
	}
	
	/**
	 * Rounds input number based on taxDuty
	 * @param taxDuty
	 * @param num
	 * @return
	 */
	private double round(TaxDuty taxDuty, double num)
	{
		num = Math.ceil(num);
		int mod = (int)(Math.ceil(num)%(int)(taxDuty.getRound()));
		if(mod == 0)
		{
			return num;
		}
		return num + (taxDuty.getRound() - mod);
	}
	
	private static Log _log = LogFactoryUtil.getLog(FinalItemLocalServiceImpl.class);
}