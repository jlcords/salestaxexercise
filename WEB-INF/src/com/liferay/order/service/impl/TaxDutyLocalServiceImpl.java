/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service.impl;

import com.liferay.order.NoSuchTaxDutyException;
import com.liferay.order.model.FinalItem;
import com.liferay.order.model.InventoryItem;
import com.liferay.order.model.TaxDuty;
import com.liferay.order.service.TaxDutyLocalServiceUtil;
import com.liferay.order.service.base.TaxDutyLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.List;

/**
 * The implementation of the tax duty local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.liferay.order.service.TaxDutyLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Joshua Cords
 * @see com.liferay.order.service.base.TaxDutyLocalServiceBaseImpl
 * @see com.liferay.order.service.TaxDutyLocalServiceUtil
 */
public class TaxDutyLocalServiceImpl extends TaxDutyLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.liferay.order.service.TaxDutyLocalServiceUtil} to access the tax duty local service.
	 */
	/**
	 * Adds TaxDuty Item to database
	 * @param companyId
	 * @param groupId
	 * @param taxDutyId
	 * @param tax
	 * @param duty
	 * @param round
	 * @return
	 * @throws SystemException
	 */
	public TaxDuty addTaxDuty(
			long companyId, long groupId, int tax, int duty, int round
			) throws SystemException{
		TaxDuty taxDuty = TaxDutyLocalServiceUtil.createTaxDuty(0);
		long taxDutyId = 
				counterLocalService.increment(TaxDuty.class.getName());
		taxDuty.setCompanyId(companyId);
		taxDuty.setGroupId(groupId);
		taxDuty.setTaxDutyId(taxDutyId);
		taxDuty.setTax(tax);
		taxDuty.setDuty(duty);
		taxDuty.setRound(round);
		return TaxDutyLocalServiceUtil.addTaxDuty(taxDuty);
	}
	
	/**
	 * Returns TaxDuty for GroupId or creates a new TaxDuty
	 * @param companyId
	 * @param groupId
	 * @return TaxDuty
	 */
	public TaxDuty getTaxDutybyGroupId(long groupId){
		try {
			return taxDutyPersistence.findByGroupId(groupId);
		} catch (Exception e) {			
			_log.error("Failed to find TaxDuty with groupId " + groupId, e);
			
			try {
				return TaxDutyLocalServiceUtil.addTaxDuty(0, groupId, 0, 0, 0);
			} catch (SystemException e1) {
				_log.error("Failed to find create TaxDuty with groupId "
						+ groupId, e1);
			}
		}
		return null;
	}
	
	/**
	 * Updates TaxDuty Item in database
	 * @param companyId
	 * @param groupId
	 * @param taxDutyId
	 * @param tax
	 * @param duty
	 * @param round
	 * @return
	 * @throws SystemException
	 */
	public TaxDuty updateTaxDuty(
			long companyId, long groupId, long taxDutyId, int tax, int duty, 
			int round) throws SystemException{
		TaxDuty taxDuty = TaxDutyLocalServiceUtil.getTaxDutybyGroupId(groupId);
		taxDuty.setCompanyId(companyId);
		taxDuty.setGroupId(groupId);
		taxDuty.setTaxDutyId(taxDutyId);
		taxDuty.setTax(tax);
		taxDuty.setDuty(duty);
		taxDuty.setRound(round);
		return TaxDutyLocalServiceUtil.updateTaxDuty(taxDuty);
	}
	
	private static Log _log = LogFactoryUtil
			.getLog(TaxDutyLocalServiceImpl.class);
	
}