/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Joshua Cords
 * @generated
 */
public class FinalItemSoap implements Serializable {
	public static FinalItemSoap toSoapModel(FinalItem model) {
		FinalItemSoap soapModel = new FinalItemSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setFinalItemId(model.getFinalItemId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setUserId(model.getUserId());
		soapModel.setOrderId(model.getOrderId());
		soapModel.setCategoryId(model.getCategoryId());
		soapModel.setName(model.getName());
		soapModel.setBasePrice(model.getBasePrice());
		soapModel.setTotalPrice(model.getTotalPrice());
		soapModel.setQuantity(model.getQuantity());
		soapModel.setImported(model.getImported());
		soapModel.setTax(model.getTax());
		soapModel.setDuty(model.getDuty());

		return soapModel;
	}

	public static FinalItemSoap[] toSoapModels(FinalItem[] models) {
		FinalItemSoap[] soapModels = new FinalItemSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static FinalItemSoap[][] toSoapModels(FinalItem[][] models) {
		FinalItemSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new FinalItemSoap[models.length][models[0].length];
		}
		else {
			soapModels = new FinalItemSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static FinalItemSoap[] toSoapModels(List<FinalItem> models) {
		List<FinalItemSoap> soapModels = new ArrayList<FinalItemSoap>(models.size());

		for (FinalItem model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new FinalItemSoap[soapModels.size()]);
	}

	public FinalItemSoap() {
	}

	public long getPrimaryKey() {
		return _finalItemId;
	}

	public void setPrimaryKey(long pk) {
		setFinalItemId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getFinalItemId() {
		return _finalItemId;
	}

	public void setFinalItemId(long finalItemId) {
		_finalItemId = finalItemId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public long getOrderId() {
		return _orderId;
	}

	public void setOrderId(long orderId) {
		_orderId = orderId;
	}

	public long getCategoryId() {
		return _categoryId;
	}

	public void setCategoryId(long categoryId) {
		_categoryId = categoryId;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public double getBasePrice() {
		return _basePrice;
	}

	public void setBasePrice(double basePrice) {
		_basePrice = basePrice;
	}

	public double getTotalPrice() {
		return _totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		_totalPrice = totalPrice;
	}

	public int getQuantity() {
		return _quantity;
	}

	public void setQuantity(int quantity) {
		_quantity = quantity;
	}

	public boolean getImported() {
		return _imported;
	}

	public boolean isImported() {
		return _imported;
	}

	public void setImported(boolean imported) {
		_imported = imported;
	}

	public double getTax() {
		return _tax;
	}

	public void setTax(double tax) {
		_tax = tax;
	}

	public double getDuty() {
		return _duty;
	}

	public void setDuty(double duty) {
		_duty = duty;
	}

	private String _uuid;
	private long _finalItemId;
	private long _companyId;
	private long _groupId;
	private long _userId;
	private long _orderId;
	private long _categoryId;
	private String _name;
	private double _basePrice;
	private double _totalPrice;
	private int _quantity;
	private boolean _imported;
	private double _tax;
	private double _duty;
}