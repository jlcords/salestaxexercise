/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Order}.
 * </p>
 *
 * @author Joshua Cords
 * @see Order
 * @generated
 */
public class OrderWrapper implements Order, ModelWrapper<Order> {
	public OrderWrapper(Order order) {
		_order = order;
	}

	@Override
	public Class<?> getModelClass() {
		return Order.class;
	}

	@Override
	public String getModelClassName() {
		return Order.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("orderId", getOrderId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userId", getUserId());
		attributes.put("confirmed", getConfirmed());
		attributes.put("createDate", getCreateDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long orderId = (Long)attributes.get("orderId");

		if (orderId != null) {
			setOrderId(orderId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Boolean confirmed = (Boolean)attributes.get("confirmed");

		if (confirmed != null) {
			setConfirmed(confirmed);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}
	}

	/**
	* Returns the primary key of this order.
	*
	* @return the primary key of this order
	*/
	@Override
	public long getPrimaryKey() {
		return _order.getPrimaryKey();
	}

	/**
	* Sets the primary key of this order.
	*
	* @param primaryKey the primary key of this order
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_order.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the uuid of this order.
	*
	* @return the uuid of this order
	*/
	@Override
	public java.lang.String getUuid() {
		return _order.getUuid();
	}

	/**
	* Sets the uuid of this order.
	*
	* @param uuid the uuid of this order
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_order.setUuid(uuid);
	}

	/**
	* Returns the order ID of this order.
	*
	* @return the order ID of this order
	*/
	@Override
	public long getOrderId() {
		return _order.getOrderId();
	}

	/**
	* Sets the order ID of this order.
	*
	* @param orderId the order ID of this order
	*/
	@Override
	public void setOrderId(long orderId) {
		_order.setOrderId(orderId);
	}

	/**
	* Returns the company ID of this order.
	*
	* @return the company ID of this order
	*/
	@Override
	public long getCompanyId() {
		return _order.getCompanyId();
	}

	/**
	* Sets the company ID of this order.
	*
	* @param companyId the company ID of this order
	*/
	@Override
	public void setCompanyId(long companyId) {
		_order.setCompanyId(companyId);
	}

	/**
	* Returns the group ID of this order.
	*
	* @return the group ID of this order
	*/
	@Override
	public long getGroupId() {
		return _order.getGroupId();
	}

	/**
	* Sets the group ID of this order.
	*
	* @param groupId the group ID of this order
	*/
	@Override
	public void setGroupId(long groupId) {
		_order.setGroupId(groupId);
	}

	/**
	* Returns the user ID of this order.
	*
	* @return the user ID of this order
	*/
	@Override
	public long getUserId() {
		return _order.getUserId();
	}

	/**
	* Sets the user ID of this order.
	*
	* @param userId the user ID of this order
	*/
	@Override
	public void setUserId(long userId) {
		_order.setUserId(userId);
	}

	/**
	* Returns the user uuid of this order.
	*
	* @return the user uuid of this order
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _order.getUserUuid();
	}

	/**
	* Sets the user uuid of this order.
	*
	* @param userUuid the user uuid of this order
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_order.setUserUuid(userUuid);
	}

	/**
	* Returns the confirmed of this order.
	*
	* @return the confirmed of this order
	*/
	@Override
	public boolean getConfirmed() {
		return _order.getConfirmed();
	}

	/**
	* Returns <code>true</code> if this order is confirmed.
	*
	* @return <code>true</code> if this order is confirmed; <code>false</code> otherwise
	*/
	@Override
	public boolean isConfirmed() {
		return _order.isConfirmed();
	}

	/**
	* Sets whether this order is confirmed.
	*
	* @param confirmed the confirmed of this order
	*/
	@Override
	public void setConfirmed(boolean confirmed) {
		_order.setConfirmed(confirmed);
	}

	/**
	* Returns the create date of this order.
	*
	* @return the create date of this order
	*/
	@Override
	public java.util.Date getCreateDate() {
		return _order.getCreateDate();
	}

	/**
	* Sets the create date of this order.
	*
	* @param createDate the create date of this order
	*/
	@Override
	public void setCreateDate(java.util.Date createDate) {
		_order.setCreateDate(createDate);
	}

	@Override
	public boolean isNew() {
		return _order.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_order.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _order.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_order.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _order.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _order.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_order.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _order.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_order.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_order.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_order.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new OrderWrapper((Order)_order.clone());
	}

	@Override
	public int compareTo(com.liferay.order.model.Order order) {
		return _order.compareTo(order);
	}

	@Override
	public int hashCode() {
		return _order.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.liferay.order.model.Order> toCacheModel() {
		return _order.toCacheModel();
	}

	@Override
	public com.liferay.order.model.Order toEscapedModel() {
		return new OrderWrapper(_order.toEscapedModel());
	}

	@Override
	public com.liferay.order.model.Order toUnescapedModel() {
		return new OrderWrapper(_order.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _order.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _order.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_order.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof OrderWrapper)) {
			return false;
		}

		OrderWrapper orderWrapper = (OrderWrapper)obj;

		if (Validator.equals(_order, orderWrapper._order)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public Order getWrappedOrder() {
		return _order;
	}

	@Override
	public Order getWrappedModel() {
		return _order;
	}

	@Override
	public void resetOriginalValues() {
		_order.resetOriginalValues();
	}

	private Order _order;
}