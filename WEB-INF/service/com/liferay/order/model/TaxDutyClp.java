/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.model;

import com.liferay.order.service.ClpSerializer;
import com.liferay.order.service.TaxDutyLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Joshua Cords
 */
public class TaxDutyClp extends BaseModelImpl<TaxDuty> implements TaxDuty {
	public TaxDutyClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return TaxDuty.class;
	}

	@Override
	public String getModelClassName() {
		return TaxDuty.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _taxDutyId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setTaxDutyId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _taxDutyId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("taxDutyId", getTaxDutyId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("tax", getTax());
		attributes.put("duty", getDuty());
		attributes.put("round", getRound());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long taxDutyId = (Long)attributes.get("taxDutyId");

		if (taxDutyId != null) {
			setTaxDutyId(taxDutyId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Integer tax = (Integer)attributes.get("tax");

		if (tax != null) {
			setTax(tax);
		}

		Integer duty = (Integer)attributes.get("duty");

		if (duty != null) {
			setDuty(duty);
		}

		Integer round = (Integer)attributes.get("round");

		if (round != null) {
			setRound(round);
		}
	}

	@Override
	public String getUuid() {
		return _uuid;
	}

	@Override
	public void setUuid(String uuid) {
		_uuid = uuid;

		if (_taxDutyRemoteModel != null) {
			try {
				Class<?> clazz = _taxDutyRemoteModel.getClass();

				Method method = clazz.getMethod("setUuid", String.class);

				method.invoke(_taxDutyRemoteModel, uuid);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getTaxDutyId() {
		return _taxDutyId;
	}

	@Override
	public void setTaxDutyId(long taxDutyId) {
		_taxDutyId = taxDutyId;

		if (_taxDutyRemoteModel != null) {
			try {
				Class<?> clazz = _taxDutyRemoteModel.getClass();

				Method method = clazz.getMethod("setTaxDutyId", long.class);

				method.invoke(_taxDutyRemoteModel, taxDutyId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		_companyId = companyId;

		if (_taxDutyRemoteModel != null) {
			try {
				Class<?> clazz = _taxDutyRemoteModel.getClass();

				Method method = clazz.getMethod("setCompanyId", long.class);

				method.invoke(_taxDutyRemoteModel, companyId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getGroupId() {
		return _groupId;
	}

	@Override
	public void setGroupId(long groupId) {
		_groupId = groupId;

		if (_taxDutyRemoteModel != null) {
			try {
				Class<?> clazz = _taxDutyRemoteModel.getClass();

				Method method = clazz.getMethod("setGroupId", long.class);

				method.invoke(_taxDutyRemoteModel, groupId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getTax() {
		return _tax;
	}

	@Override
	public void setTax(int tax) {
		_tax = tax;

		if (_taxDutyRemoteModel != null) {
			try {
				Class<?> clazz = _taxDutyRemoteModel.getClass();

				Method method = clazz.getMethod("setTax", int.class);

				method.invoke(_taxDutyRemoteModel, tax);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getDuty() {
		return _duty;
	}

	@Override
	public void setDuty(int duty) {
		_duty = duty;

		if (_taxDutyRemoteModel != null) {
			try {
				Class<?> clazz = _taxDutyRemoteModel.getClass();

				Method method = clazz.getMethod("setDuty", int.class);

				method.invoke(_taxDutyRemoteModel, duty);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getRound() {
		return _round;
	}

	@Override
	public void setRound(int round) {
		_round = round;

		if (_taxDutyRemoteModel != null) {
			try {
				Class<?> clazz = _taxDutyRemoteModel.getClass();

				Method method = clazz.getMethod("setRound", int.class);

				method.invoke(_taxDutyRemoteModel, round);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getTaxDutyRemoteModel() {
		return _taxDutyRemoteModel;
	}

	public void setTaxDutyRemoteModel(BaseModel<?> taxDutyRemoteModel) {
		_taxDutyRemoteModel = taxDutyRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _taxDutyRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_taxDutyRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			TaxDutyLocalServiceUtil.addTaxDuty(this);
		}
		else {
			TaxDutyLocalServiceUtil.updateTaxDuty(this);
		}
	}

	@Override
	public TaxDuty toEscapedModel() {
		return (TaxDuty)ProxyUtil.newProxyInstance(TaxDuty.class.getClassLoader(),
			new Class[] { TaxDuty.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		TaxDutyClp clone = new TaxDutyClp();

		clone.setUuid(getUuid());
		clone.setTaxDutyId(getTaxDutyId());
		clone.setCompanyId(getCompanyId());
		clone.setGroupId(getGroupId());
		clone.setTax(getTax());
		clone.setDuty(getDuty());
		clone.setRound(getRound());

		return clone;
	}

	@Override
	public int compareTo(TaxDuty taxDuty) {
		long primaryKey = taxDuty.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TaxDutyClp)) {
			return false;
		}

		TaxDutyClp taxDuty = (TaxDutyClp)obj;

		long primaryKey = taxDuty.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{uuid=");
		sb.append(getUuid());
		sb.append(", taxDutyId=");
		sb.append(getTaxDutyId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", groupId=");
		sb.append(getGroupId());
		sb.append(", tax=");
		sb.append(getTax());
		sb.append(", duty=");
		sb.append(getDuty());
		sb.append(", round=");
		sb.append(getRound());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(25);

		sb.append("<model><model-name>");
		sb.append("com.liferay.order.model.TaxDuty");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>uuid</column-name><column-value><![CDATA[");
		sb.append(getUuid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>taxDutyId</column-name><column-value><![CDATA[");
		sb.append(getTaxDutyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>groupId</column-name><column-value><![CDATA[");
		sb.append(getGroupId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>tax</column-name><column-value><![CDATA[");
		sb.append(getTax());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>duty</column-name><column-value><![CDATA[");
		sb.append(getDuty());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>round</column-name><column-value><![CDATA[");
		sb.append(getRound());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _uuid;
	private long _taxDutyId;
	private long _companyId;
	private long _groupId;
	private int _tax;
	private int _duty;
	private int _round;
	private BaseModel<?> _taxDutyRemoteModel;
	private Class<?> _clpSerializerClass = com.liferay.order.service.ClpSerializer.class;
}