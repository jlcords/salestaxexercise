/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link TaxDuty}.
 * </p>
 *
 * @author Joshua Cords
 * @see TaxDuty
 * @generated
 */
public class TaxDutyWrapper implements TaxDuty, ModelWrapper<TaxDuty> {
	public TaxDutyWrapper(TaxDuty taxDuty) {
		_taxDuty = taxDuty;
	}

	@Override
	public Class<?> getModelClass() {
		return TaxDuty.class;
	}

	@Override
	public String getModelClassName() {
		return TaxDuty.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("taxDutyId", getTaxDutyId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("tax", getTax());
		attributes.put("duty", getDuty());
		attributes.put("round", getRound());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long taxDutyId = (Long)attributes.get("taxDutyId");

		if (taxDutyId != null) {
			setTaxDutyId(taxDutyId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Integer tax = (Integer)attributes.get("tax");

		if (tax != null) {
			setTax(tax);
		}

		Integer duty = (Integer)attributes.get("duty");

		if (duty != null) {
			setDuty(duty);
		}

		Integer round = (Integer)attributes.get("round");

		if (round != null) {
			setRound(round);
		}
	}

	/**
	* Returns the primary key of this tax duty.
	*
	* @return the primary key of this tax duty
	*/
	@Override
	public long getPrimaryKey() {
		return _taxDuty.getPrimaryKey();
	}

	/**
	* Sets the primary key of this tax duty.
	*
	* @param primaryKey the primary key of this tax duty
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_taxDuty.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the uuid of this tax duty.
	*
	* @return the uuid of this tax duty
	*/
	@Override
	public java.lang.String getUuid() {
		return _taxDuty.getUuid();
	}

	/**
	* Sets the uuid of this tax duty.
	*
	* @param uuid the uuid of this tax duty
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_taxDuty.setUuid(uuid);
	}

	/**
	* Returns the tax duty ID of this tax duty.
	*
	* @return the tax duty ID of this tax duty
	*/
	@Override
	public long getTaxDutyId() {
		return _taxDuty.getTaxDutyId();
	}

	/**
	* Sets the tax duty ID of this tax duty.
	*
	* @param taxDutyId the tax duty ID of this tax duty
	*/
	@Override
	public void setTaxDutyId(long taxDutyId) {
		_taxDuty.setTaxDutyId(taxDutyId);
	}

	/**
	* Returns the company ID of this tax duty.
	*
	* @return the company ID of this tax duty
	*/
	@Override
	public long getCompanyId() {
		return _taxDuty.getCompanyId();
	}

	/**
	* Sets the company ID of this tax duty.
	*
	* @param companyId the company ID of this tax duty
	*/
	@Override
	public void setCompanyId(long companyId) {
		_taxDuty.setCompanyId(companyId);
	}

	/**
	* Returns the group ID of this tax duty.
	*
	* @return the group ID of this tax duty
	*/
	@Override
	public long getGroupId() {
		return _taxDuty.getGroupId();
	}

	/**
	* Sets the group ID of this tax duty.
	*
	* @param groupId the group ID of this tax duty
	*/
	@Override
	public void setGroupId(long groupId) {
		_taxDuty.setGroupId(groupId);
	}

	/**
	* Returns the tax of this tax duty.
	*
	* @return the tax of this tax duty
	*/
	@Override
	public int getTax() {
		return _taxDuty.getTax();
	}

	/**
	* Sets the tax of this tax duty.
	*
	* @param tax the tax of this tax duty
	*/
	@Override
	public void setTax(int tax) {
		_taxDuty.setTax(tax);
	}

	/**
	* Returns the duty of this tax duty.
	*
	* @return the duty of this tax duty
	*/
	@Override
	public int getDuty() {
		return _taxDuty.getDuty();
	}

	/**
	* Sets the duty of this tax duty.
	*
	* @param duty the duty of this tax duty
	*/
	@Override
	public void setDuty(int duty) {
		_taxDuty.setDuty(duty);
	}

	/**
	* Returns the round of this tax duty.
	*
	* @return the round of this tax duty
	*/
	@Override
	public int getRound() {
		return _taxDuty.getRound();
	}

	/**
	* Sets the round of this tax duty.
	*
	* @param round the round of this tax duty
	*/
	@Override
	public void setRound(int round) {
		_taxDuty.setRound(round);
	}

	@Override
	public boolean isNew() {
		return _taxDuty.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_taxDuty.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _taxDuty.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_taxDuty.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _taxDuty.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _taxDuty.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_taxDuty.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _taxDuty.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_taxDuty.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_taxDuty.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_taxDuty.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new TaxDutyWrapper((TaxDuty)_taxDuty.clone());
	}

	@Override
	public int compareTo(com.liferay.order.model.TaxDuty taxDuty) {
		return _taxDuty.compareTo(taxDuty);
	}

	@Override
	public int hashCode() {
		return _taxDuty.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.liferay.order.model.TaxDuty> toCacheModel() {
		return _taxDuty.toCacheModel();
	}

	@Override
	public com.liferay.order.model.TaxDuty toEscapedModel() {
		return new TaxDutyWrapper(_taxDuty.toEscapedModel());
	}

	@Override
	public com.liferay.order.model.TaxDuty toUnescapedModel() {
		return new TaxDutyWrapper(_taxDuty.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _taxDuty.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _taxDuty.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_taxDuty.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TaxDutyWrapper)) {
			return false;
		}

		TaxDutyWrapper taxDutyWrapper = (TaxDutyWrapper)obj;

		if (Validator.equals(_taxDuty, taxDutyWrapper._taxDuty)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public TaxDuty getWrappedTaxDuty() {
		return _taxDuty;
	}

	@Override
	public TaxDuty getWrappedModel() {
		return _taxDuty;
	}

	@Override
	public void resetOriginalValues() {
		_taxDuty.resetOriginalValues();
	}

	private TaxDuty _taxDuty;
}