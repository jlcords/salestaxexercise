/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Joshua Cords
 * @generated
 */
public class InventoryItemSoap implements Serializable {
	public static InventoryItemSoap toSoapModel(InventoryItem model) {
		InventoryItemSoap soapModel = new InventoryItemSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setInventoryItemId(model.getInventoryItemId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setUserId(model.getUserId());
		soapModel.setCategoryId(model.getCategoryId());
		soapModel.setName(model.getName());
		soapModel.setBasePrice(model.getBasePrice());
		soapModel.setQuantity(model.getQuantity());
		soapModel.setImported(model.getImported());

		return soapModel;
	}

	public static InventoryItemSoap[] toSoapModels(InventoryItem[] models) {
		InventoryItemSoap[] soapModels = new InventoryItemSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static InventoryItemSoap[][] toSoapModels(InventoryItem[][] models) {
		InventoryItemSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new InventoryItemSoap[models.length][models[0].length];
		}
		else {
			soapModels = new InventoryItemSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static InventoryItemSoap[] toSoapModels(List<InventoryItem> models) {
		List<InventoryItemSoap> soapModels = new ArrayList<InventoryItemSoap>(models.size());

		for (InventoryItem model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new InventoryItemSoap[soapModels.size()]);
	}

	public InventoryItemSoap() {
	}

	public long getPrimaryKey() {
		return _inventoryItemId;
	}

	public void setPrimaryKey(long pk) {
		setInventoryItemId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getInventoryItemId() {
		return _inventoryItemId;
	}

	public void setInventoryItemId(long inventoryItemId) {
		_inventoryItemId = inventoryItemId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public long getCategoryId() {
		return _categoryId;
	}

	public void setCategoryId(long categoryId) {
		_categoryId = categoryId;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public double getBasePrice() {
		return _basePrice;
	}

	public void setBasePrice(double basePrice) {
		_basePrice = basePrice;
	}

	public int getQuantity() {
		return _quantity;
	}

	public void setQuantity(int quantity) {
		_quantity = quantity;
	}

	public boolean getImported() {
		return _imported;
	}

	public boolean isImported() {
		return _imported;
	}

	public void setImported(boolean imported) {
		_imported = imported;
	}

	private String _uuid;
	private long _inventoryItemId;
	private long _companyId;
	private long _groupId;
	private long _userId;
	private long _categoryId;
	private String _name;
	private double _basePrice;
	private int _quantity;
	private boolean _imported;
}