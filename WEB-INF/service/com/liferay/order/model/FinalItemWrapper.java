/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link FinalItem}.
 * </p>
 *
 * @author Joshua Cords
 * @see FinalItem
 * @generated
 */
public class FinalItemWrapper implements FinalItem, ModelWrapper<FinalItem> {
	public FinalItemWrapper(FinalItem finalItem) {
		_finalItem = finalItem;
	}

	@Override
	public Class<?> getModelClass() {
		return FinalItem.class;
	}

	@Override
	public String getModelClassName() {
		return FinalItem.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("finalItemId", getFinalItemId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userId", getUserId());
		attributes.put("orderId", getOrderId());
		attributes.put("categoryId", getCategoryId());
		attributes.put("name", getName());
		attributes.put("basePrice", getBasePrice());
		attributes.put("totalPrice", getTotalPrice());
		attributes.put("quantity", getQuantity());
		attributes.put("imported", getImported());
		attributes.put("tax", getTax());
		attributes.put("duty", getDuty());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long finalItemId = (Long)attributes.get("finalItemId");

		if (finalItemId != null) {
			setFinalItemId(finalItemId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long orderId = (Long)attributes.get("orderId");

		if (orderId != null) {
			setOrderId(orderId);
		}

		Long categoryId = (Long)attributes.get("categoryId");

		if (categoryId != null) {
			setCategoryId(categoryId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Double basePrice = (Double)attributes.get("basePrice");

		if (basePrice != null) {
			setBasePrice(basePrice);
		}

		Double totalPrice = (Double)attributes.get("totalPrice");

		if (totalPrice != null) {
			setTotalPrice(totalPrice);
		}

		Integer quantity = (Integer)attributes.get("quantity");

		if (quantity != null) {
			setQuantity(quantity);
		}

		Boolean imported = (Boolean)attributes.get("imported");

		if (imported != null) {
			setImported(imported);
		}

		Double tax = (Double)attributes.get("tax");

		if (tax != null) {
			setTax(tax);
		}

		Double duty = (Double)attributes.get("duty");

		if (duty != null) {
			setDuty(duty);
		}
	}

	/**
	* Returns the primary key of this final item.
	*
	* @return the primary key of this final item
	*/
	@Override
	public long getPrimaryKey() {
		return _finalItem.getPrimaryKey();
	}

	/**
	* Sets the primary key of this final item.
	*
	* @param primaryKey the primary key of this final item
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_finalItem.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the uuid of this final item.
	*
	* @return the uuid of this final item
	*/
	@Override
	public java.lang.String getUuid() {
		return _finalItem.getUuid();
	}

	/**
	* Sets the uuid of this final item.
	*
	* @param uuid the uuid of this final item
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_finalItem.setUuid(uuid);
	}

	/**
	* Returns the final item ID of this final item.
	*
	* @return the final item ID of this final item
	*/
	@Override
	public long getFinalItemId() {
		return _finalItem.getFinalItemId();
	}

	/**
	* Sets the final item ID of this final item.
	*
	* @param finalItemId the final item ID of this final item
	*/
	@Override
	public void setFinalItemId(long finalItemId) {
		_finalItem.setFinalItemId(finalItemId);
	}

	/**
	* Returns the company ID of this final item.
	*
	* @return the company ID of this final item
	*/
	@Override
	public long getCompanyId() {
		return _finalItem.getCompanyId();
	}

	/**
	* Sets the company ID of this final item.
	*
	* @param companyId the company ID of this final item
	*/
	@Override
	public void setCompanyId(long companyId) {
		_finalItem.setCompanyId(companyId);
	}

	/**
	* Returns the group ID of this final item.
	*
	* @return the group ID of this final item
	*/
	@Override
	public long getGroupId() {
		return _finalItem.getGroupId();
	}

	/**
	* Sets the group ID of this final item.
	*
	* @param groupId the group ID of this final item
	*/
	@Override
	public void setGroupId(long groupId) {
		_finalItem.setGroupId(groupId);
	}

	/**
	* Returns the user ID of this final item.
	*
	* @return the user ID of this final item
	*/
	@Override
	public long getUserId() {
		return _finalItem.getUserId();
	}

	/**
	* Sets the user ID of this final item.
	*
	* @param userId the user ID of this final item
	*/
	@Override
	public void setUserId(long userId) {
		_finalItem.setUserId(userId);
	}

	/**
	* Returns the user uuid of this final item.
	*
	* @return the user uuid of this final item
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItem.getUserUuid();
	}

	/**
	* Sets the user uuid of this final item.
	*
	* @param userUuid the user uuid of this final item
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_finalItem.setUserUuid(userUuid);
	}

	/**
	* Returns the order ID of this final item.
	*
	* @return the order ID of this final item
	*/
	@Override
	public long getOrderId() {
		return _finalItem.getOrderId();
	}

	/**
	* Sets the order ID of this final item.
	*
	* @param orderId the order ID of this final item
	*/
	@Override
	public void setOrderId(long orderId) {
		_finalItem.setOrderId(orderId);
	}

	/**
	* Returns the category ID of this final item.
	*
	* @return the category ID of this final item
	*/
	@Override
	public long getCategoryId() {
		return _finalItem.getCategoryId();
	}

	/**
	* Sets the category ID of this final item.
	*
	* @param categoryId the category ID of this final item
	*/
	@Override
	public void setCategoryId(long categoryId) {
		_finalItem.setCategoryId(categoryId);
	}

	/**
	* Returns the name of this final item.
	*
	* @return the name of this final item
	*/
	@Override
	public java.lang.String getName() {
		return _finalItem.getName();
	}

	/**
	* Sets the name of this final item.
	*
	* @param name the name of this final item
	*/
	@Override
	public void setName(java.lang.String name) {
		_finalItem.setName(name);
	}

	/**
	* Returns the base price of this final item.
	*
	* @return the base price of this final item
	*/
	@Override
	public double getBasePrice() {
		return _finalItem.getBasePrice();
	}

	/**
	* Sets the base price of this final item.
	*
	* @param basePrice the base price of this final item
	*/
	@Override
	public void setBasePrice(double basePrice) {
		_finalItem.setBasePrice(basePrice);
	}

	/**
	* Returns the total price of this final item.
	*
	* @return the total price of this final item
	*/
	@Override
	public double getTotalPrice() {
		return _finalItem.getTotalPrice();
	}

	/**
	* Sets the total price of this final item.
	*
	* @param totalPrice the total price of this final item
	*/
	@Override
	public void setTotalPrice(double totalPrice) {
		_finalItem.setTotalPrice(totalPrice);
	}

	/**
	* Returns the quantity of this final item.
	*
	* @return the quantity of this final item
	*/
	@Override
	public int getQuantity() {
		return _finalItem.getQuantity();
	}

	/**
	* Sets the quantity of this final item.
	*
	* @param quantity the quantity of this final item
	*/
	@Override
	public void setQuantity(int quantity) {
		_finalItem.setQuantity(quantity);
	}

	/**
	* Returns the imported of this final item.
	*
	* @return the imported of this final item
	*/
	@Override
	public boolean getImported() {
		return _finalItem.getImported();
	}

	/**
	* Returns <code>true</code> if this final item is imported.
	*
	* @return <code>true</code> if this final item is imported; <code>false</code> otherwise
	*/
	@Override
	public boolean isImported() {
		return _finalItem.isImported();
	}

	/**
	* Sets whether this final item is imported.
	*
	* @param imported the imported of this final item
	*/
	@Override
	public void setImported(boolean imported) {
		_finalItem.setImported(imported);
	}

	/**
	* Returns the tax of this final item.
	*
	* @return the tax of this final item
	*/
	@Override
	public double getTax() {
		return _finalItem.getTax();
	}

	/**
	* Sets the tax of this final item.
	*
	* @param tax the tax of this final item
	*/
	@Override
	public void setTax(double tax) {
		_finalItem.setTax(tax);
	}

	/**
	* Returns the duty of this final item.
	*
	* @return the duty of this final item
	*/
	@Override
	public double getDuty() {
		return _finalItem.getDuty();
	}

	/**
	* Sets the duty of this final item.
	*
	* @param duty the duty of this final item
	*/
	@Override
	public void setDuty(double duty) {
		_finalItem.setDuty(duty);
	}

	@Override
	public boolean isNew() {
		return _finalItem.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_finalItem.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _finalItem.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_finalItem.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _finalItem.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _finalItem.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_finalItem.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _finalItem.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_finalItem.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_finalItem.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_finalItem.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new FinalItemWrapper((FinalItem)_finalItem.clone());
	}

	@Override
	public int compareTo(com.liferay.order.model.FinalItem finalItem) {
		return _finalItem.compareTo(finalItem);
	}

	@Override
	public int hashCode() {
		return _finalItem.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.liferay.order.model.FinalItem> toCacheModel() {
		return _finalItem.toCacheModel();
	}

	@Override
	public com.liferay.order.model.FinalItem toEscapedModel() {
		return new FinalItemWrapper(_finalItem.toEscapedModel());
	}

	@Override
	public com.liferay.order.model.FinalItem toUnescapedModel() {
		return new FinalItemWrapper(_finalItem.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _finalItem.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _finalItem.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_finalItem.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof FinalItemWrapper)) {
			return false;
		}

		FinalItemWrapper finalItemWrapper = (FinalItemWrapper)obj;

		if (Validator.equals(_finalItem, finalItemWrapper._finalItem)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public FinalItem getWrappedFinalItem() {
		return _finalItem;
	}

	@Override
	public FinalItem getWrappedModel() {
		return _finalItem;
	}

	@Override
	public void resetOriginalValues() {
		_finalItem.resetOriginalValues();
	}

	private FinalItem _finalItem;
}