/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link InventoryItem}.
 * </p>
 *
 * @author Joshua Cords
 * @see InventoryItem
 * @generated
 */
public class InventoryItemWrapper implements InventoryItem,
	ModelWrapper<InventoryItem> {
	public InventoryItemWrapper(InventoryItem inventoryItem) {
		_inventoryItem = inventoryItem;
	}

	@Override
	public Class<?> getModelClass() {
		return InventoryItem.class;
	}

	@Override
	public String getModelClassName() {
		return InventoryItem.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("inventoryItemId", getInventoryItemId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userId", getUserId());
		attributes.put("categoryId", getCategoryId());
		attributes.put("name", getName());
		attributes.put("basePrice", getBasePrice());
		attributes.put("quantity", getQuantity());
		attributes.put("imported", getImported());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long inventoryItemId = (Long)attributes.get("inventoryItemId");

		if (inventoryItemId != null) {
			setInventoryItemId(inventoryItemId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long categoryId = (Long)attributes.get("categoryId");

		if (categoryId != null) {
			setCategoryId(categoryId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Double basePrice = (Double)attributes.get("basePrice");

		if (basePrice != null) {
			setBasePrice(basePrice);
		}

		Integer quantity = (Integer)attributes.get("quantity");

		if (quantity != null) {
			setQuantity(quantity);
		}

		Boolean imported = (Boolean)attributes.get("imported");

		if (imported != null) {
			setImported(imported);
		}
	}

	/**
	* Returns the primary key of this inventory item.
	*
	* @return the primary key of this inventory item
	*/
	@Override
	public long getPrimaryKey() {
		return _inventoryItem.getPrimaryKey();
	}

	/**
	* Sets the primary key of this inventory item.
	*
	* @param primaryKey the primary key of this inventory item
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_inventoryItem.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the uuid of this inventory item.
	*
	* @return the uuid of this inventory item
	*/
	@Override
	public java.lang.String getUuid() {
		return _inventoryItem.getUuid();
	}

	/**
	* Sets the uuid of this inventory item.
	*
	* @param uuid the uuid of this inventory item
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_inventoryItem.setUuid(uuid);
	}

	/**
	* Returns the inventory item ID of this inventory item.
	*
	* @return the inventory item ID of this inventory item
	*/
	@Override
	public long getInventoryItemId() {
		return _inventoryItem.getInventoryItemId();
	}

	/**
	* Sets the inventory item ID of this inventory item.
	*
	* @param inventoryItemId the inventory item ID of this inventory item
	*/
	@Override
	public void setInventoryItemId(long inventoryItemId) {
		_inventoryItem.setInventoryItemId(inventoryItemId);
	}

	/**
	* Returns the company ID of this inventory item.
	*
	* @return the company ID of this inventory item
	*/
	@Override
	public long getCompanyId() {
		return _inventoryItem.getCompanyId();
	}

	/**
	* Sets the company ID of this inventory item.
	*
	* @param companyId the company ID of this inventory item
	*/
	@Override
	public void setCompanyId(long companyId) {
		_inventoryItem.setCompanyId(companyId);
	}

	/**
	* Returns the group ID of this inventory item.
	*
	* @return the group ID of this inventory item
	*/
	@Override
	public long getGroupId() {
		return _inventoryItem.getGroupId();
	}

	/**
	* Sets the group ID of this inventory item.
	*
	* @param groupId the group ID of this inventory item
	*/
	@Override
	public void setGroupId(long groupId) {
		_inventoryItem.setGroupId(groupId);
	}

	/**
	* Returns the user ID of this inventory item.
	*
	* @return the user ID of this inventory item
	*/
	@Override
	public long getUserId() {
		return _inventoryItem.getUserId();
	}

	/**
	* Sets the user ID of this inventory item.
	*
	* @param userId the user ID of this inventory item
	*/
	@Override
	public void setUserId(long userId) {
		_inventoryItem.setUserId(userId);
	}

	/**
	* Returns the user uuid of this inventory item.
	*
	* @return the user uuid of this inventory item
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItem.getUserUuid();
	}

	/**
	* Sets the user uuid of this inventory item.
	*
	* @param userUuid the user uuid of this inventory item
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_inventoryItem.setUserUuid(userUuid);
	}

	/**
	* Returns the category ID of this inventory item.
	*
	* @return the category ID of this inventory item
	*/
	@Override
	public long getCategoryId() {
		return _inventoryItem.getCategoryId();
	}

	/**
	* Sets the category ID of this inventory item.
	*
	* @param categoryId the category ID of this inventory item
	*/
	@Override
	public void setCategoryId(long categoryId) {
		_inventoryItem.setCategoryId(categoryId);
	}

	/**
	* Returns the name of this inventory item.
	*
	* @return the name of this inventory item
	*/
	@Override
	public java.lang.String getName() {
		return _inventoryItem.getName();
	}

	/**
	* Sets the name of this inventory item.
	*
	* @param name the name of this inventory item
	*/
	@Override
	public void setName(java.lang.String name) {
		_inventoryItem.setName(name);
	}

	/**
	* Returns the base price of this inventory item.
	*
	* @return the base price of this inventory item
	*/
	@Override
	public double getBasePrice() {
		return _inventoryItem.getBasePrice();
	}

	/**
	* Sets the base price of this inventory item.
	*
	* @param basePrice the base price of this inventory item
	*/
	@Override
	public void setBasePrice(double basePrice) {
		_inventoryItem.setBasePrice(basePrice);
	}

	/**
	* Returns the quantity of this inventory item.
	*
	* @return the quantity of this inventory item
	*/
	@Override
	public int getQuantity() {
		return _inventoryItem.getQuantity();
	}

	/**
	* Sets the quantity of this inventory item.
	*
	* @param quantity the quantity of this inventory item
	*/
	@Override
	public void setQuantity(int quantity) {
		_inventoryItem.setQuantity(quantity);
	}

	/**
	* Returns the imported of this inventory item.
	*
	* @return the imported of this inventory item
	*/
	@Override
	public boolean getImported() {
		return _inventoryItem.getImported();
	}

	/**
	* Returns <code>true</code> if this inventory item is imported.
	*
	* @return <code>true</code> if this inventory item is imported; <code>false</code> otherwise
	*/
	@Override
	public boolean isImported() {
		return _inventoryItem.isImported();
	}

	/**
	* Sets whether this inventory item is imported.
	*
	* @param imported the imported of this inventory item
	*/
	@Override
	public void setImported(boolean imported) {
		_inventoryItem.setImported(imported);
	}

	@Override
	public boolean isNew() {
		return _inventoryItem.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_inventoryItem.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _inventoryItem.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_inventoryItem.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _inventoryItem.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _inventoryItem.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_inventoryItem.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _inventoryItem.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_inventoryItem.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_inventoryItem.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_inventoryItem.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new InventoryItemWrapper((InventoryItem)_inventoryItem.clone());
	}

	@Override
	public int compareTo(com.liferay.order.model.InventoryItem inventoryItem) {
		return _inventoryItem.compareTo(inventoryItem);
	}

	@Override
	public int hashCode() {
		return _inventoryItem.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.liferay.order.model.InventoryItem> toCacheModel() {
		return _inventoryItem.toCacheModel();
	}

	@Override
	public com.liferay.order.model.InventoryItem toEscapedModel() {
		return new InventoryItemWrapper(_inventoryItem.toEscapedModel());
	}

	@Override
	public com.liferay.order.model.InventoryItem toUnescapedModel() {
		return new InventoryItemWrapper(_inventoryItem.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _inventoryItem.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _inventoryItem.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_inventoryItem.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof InventoryItemWrapper)) {
			return false;
		}

		InventoryItemWrapper inventoryItemWrapper = (InventoryItemWrapper)obj;

		if (Validator.equals(_inventoryItem, inventoryItemWrapper._inventoryItem)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public InventoryItem getWrappedInventoryItem() {
		return _inventoryItem;
	}

	@Override
	public InventoryItem getWrappedModel() {
		return _inventoryItem;
	}

	@Override
	public void resetOriginalValues() {
		_inventoryItem.resetOriginalValues();
	}

	private InventoryItem _inventoryItem;
}