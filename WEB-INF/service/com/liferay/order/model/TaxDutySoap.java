/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Joshua Cords
 * @generated
 */
public class TaxDutySoap implements Serializable {
	public static TaxDutySoap toSoapModel(TaxDuty model) {
		TaxDutySoap soapModel = new TaxDutySoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setTaxDutyId(model.getTaxDutyId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setTax(model.getTax());
		soapModel.setDuty(model.getDuty());
		soapModel.setRound(model.getRound());

		return soapModel;
	}

	public static TaxDutySoap[] toSoapModels(TaxDuty[] models) {
		TaxDutySoap[] soapModels = new TaxDutySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TaxDutySoap[][] toSoapModels(TaxDuty[][] models) {
		TaxDutySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TaxDutySoap[models.length][models[0].length];
		}
		else {
			soapModels = new TaxDutySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TaxDutySoap[] toSoapModels(List<TaxDuty> models) {
		List<TaxDutySoap> soapModels = new ArrayList<TaxDutySoap>(models.size());

		for (TaxDuty model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TaxDutySoap[soapModels.size()]);
	}

	public TaxDutySoap() {
	}

	public long getPrimaryKey() {
		return _taxDutyId;
	}

	public void setPrimaryKey(long pk) {
		setTaxDutyId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getTaxDutyId() {
		return _taxDutyId;
	}

	public void setTaxDutyId(long taxDutyId) {
		_taxDutyId = taxDutyId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public int getTax() {
		return _tax;
	}

	public void setTax(int tax) {
		_tax = tax;
	}

	public int getDuty() {
		return _duty;
	}

	public void setDuty(int duty) {
		_duty = duty;
	}

	public int getRound() {
		return _round;
	}

	public void setRound(int round) {
		_round = round;
	}

	private String _uuid;
	private long _taxDutyId;
	private long _companyId;
	private long _groupId;
	private int _tax;
	private int _duty;
	private int _round;
}