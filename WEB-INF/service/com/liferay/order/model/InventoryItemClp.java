/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.model;

import com.liferay.order.service.ClpSerializer;
import com.liferay.order.service.InventoryItemLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Joshua Cords
 */
public class InventoryItemClp extends BaseModelImpl<InventoryItem>
	implements InventoryItem {
	public InventoryItemClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return InventoryItem.class;
	}

	@Override
	public String getModelClassName() {
		return InventoryItem.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _inventoryItemId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setInventoryItemId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _inventoryItemId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("inventoryItemId", getInventoryItemId());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userId", getUserId());
		attributes.put("categoryId", getCategoryId());
		attributes.put("name", getName());
		attributes.put("basePrice", getBasePrice());
		attributes.put("quantity", getQuantity());
		attributes.put("imported", getImported());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long inventoryItemId = (Long)attributes.get("inventoryItemId");

		if (inventoryItemId != null) {
			setInventoryItemId(inventoryItemId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long categoryId = (Long)attributes.get("categoryId");

		if (categoryId != null) {
			setCategoryId(categoryId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Double basePrice = (Double)attributes.get("basePrice");

		if (basePrice != null) {
			setBasePrice(basePrice);
		}

		Integer quantity = (Integer)attributes.get("quantity");

		if (quantity != null) {
			setQuantity(quantity);
		}

		Boolean imported = (Boolean)attributes.get("imported");

		if (imported != null) {
			setImported(imported);
		}
	}

	@Override
	public String getUuid() {
		return _uuid;
	}

	@Override
	public void setUuid(String uuid) {
		_uuid = uuid;

		if (_inventoryItemRemoteModel != null) {
			try {
				Class<?> clazz = _inventoryItemRemoteModel.getClass();

				Method method = clazz.getMethod("setUuid", String.class);

				method.invoke(_inventoryItemRemoteModel, uuid);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getInventoryItemId() {
		return _inventoryItemId;
	}

	@Override
	public void setInventoryItemId(long inventoryItemId) {
		_inventoryItemId = inventoryItemId;

		if (_inventoryItemRemoteModel != null) {
			try {
				Class<?> clazz = _inventoryItemRemoteModel.getClass();

				Method method = clazz.getMethod("setInventoryItemId", long.class);

				method.invoke(_inventoryItemRemoteModel, inventoryItemId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		_companyId = companyId;

		if (_inventoryItemRemoteModel != null) {
			try {
				Class<?> clazz = _inventoryItemRemoteModel.getClass();

				Method method = clazz.getMethod("setCompanyId", long.class);

				method.invoke(_inventoryItemRemoteModel, companyId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getGroupId() {
		return _groupId;
	}

	@Override
	public void setGroupId(long groupId) {
		_groupId = groupId;

		if (_inventoryItemRemoteModel != null) {
			try {
				Class<?> clazz = _inventoryItemRemoteModel.getClass();

				Method method = clazz.getMethod("setGroupId", long.class);

				method.invoke(_inventoryItemRemoteModel, groupId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_inventoryItemRemoteModel != null) {
			try {
				Class<?> clazz = _inventoryItemRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_inventoryItemRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public long getCategoryId() {
		return _categoryId;
	}

	@Override
	public void setCategoryId(long categoryId) {
		_categoryId = categoryId;

		if (_inventoryItemRemoteModel != null) {
			try {
				Class<?> clazz = _inventoryItemRemoteModel.getClass();

				Method method = clazz.getMethod("setCategoryId", long.class);

				method.invoke(_inventoryItemRemoteModel, categoryId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getName() {
		return _name;
	}

	@Override
	public void setName(String name) {
		_name = name;

		if (_inventoryItemRemoteModel != null) {
			try {
				Class<?> clazz = _inventoryItemRemoteModel.getClass();

				Method method = clazz.getMethod("setName", String.class);

				method.invoke(_inventoryItemRemoteModel, name);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getBasePrice() {
		return _basePrice;
	}

	@Override
	public void setBasePrice(double basePrice) {
		_basePrice = basePrice;

		if (_inventoryItemRemoteModel != null) {
			try {
				Class<?> clazz = _inventoryItemRemoteModel.getClass();

				Method method = clazz.getMethod("setBasePrice", double.class);

				method.invoke(_inventoryItemRemoteModel, basePrice);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getQuantity() {
		return _quantity;
	}

	@Override
	public void setQuantity(int quantity) {
		_quantity = quantity;

		if (_inventoryItemRemoteModel != null) {
			try {
				Class<?> clazz = _inventoryItemRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantity", int.class);

				method.invoke(_inventoryItemRemoteModel, quantity);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getImported() {
		return _imported;
	}

	@Override
	public boolean isImported() {
		return _imported;
	}

	@Override
	public void setImported(boolean imported) {
		_imported = imported;

		if (_inventoryItemRemoteModel != null) {
			try {
				Class<?> clazz = _inventoryItemRemoteModel.getClass();

				Method method = clazz.getMethod("setImported", boolean.class);

				method.invoke(_inventoryItemRemoteModel, imported);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getInventoryItemRemoteModel() {
		return _inventoryItemRemoteModel;
	}

	public void setInventoryItemRemoteModel(
		BaseModel<?> inventoryItemRemoteModel) {
		_inventoryItemRemoteModel = inventoryItemRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _inventoryItemRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_inventoryItemRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			InventoryItemLocalServiceUtil.addInventoryItem(this);
		}
		else {
			InventoryItemLocalServiceUtil.updateInventoryItem(this);
		}
	}

	@Override
	public InventoryItem toEscapedModel() {
		return (InventoryItem)ProxyUtil.newProxyInstance(InventoryItem.class.getClassLoader(),
			new Class[] { InventoryItem.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		InventoryItemClp clone = new InventoryItemClp();

		clone.setUuid(getUuid());
		clone.setInventoryItemId(getInventoryItemId());
		clone.setCompanyId(getCompanyId());
		clone.setGroupId(getGroupId());
		clone.setUserId(getUserId());
		clone.setCategoryId(getCategoryId());
		clone.setName(getName());
		clone.setBasePrice(getBasePrice());
		clone.setQuantity(getQuantity());
		clone.setImported(getImported());

		return clone;
	}

	@Override
	public int compareTo(InventoryItem inventoryItem) {
		int value = 0;

		value = getName().compareTo(inventoryItem.getName());

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof InventoryItemClp)) {
			return false;
		}

		InventoryItemClp inventoryItem = (InventoryItemClp)obj;

		long primaryKey = inventoryItem.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{uuid=");
		sb.append(getUuid());
		sb.append(", inventoryItemId=");
		sb.append(getInventoryItemId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", groupId=");
		sb.append(getGroupId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", categoryId=");
		sb.append(getCategoryId());
		sb.append(", name=");
		sb.append(getName());
		sb.append(", basePrice=");
		sb.append(getBasePrice());
		sb.append(", quantity=");
		sb.append(getQuantity());
		sb.append(", imported=");
		sb.append(getImported());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(34);

		sb.append("<model><model-name>");
		sb.append("com.liferay.order.model.InventoryItem");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>uuid</column-name><column-value><![CDATA[");
		sb.append(getUuid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>inventoryItemId</column-name><column-value><![CDATA[");
		sb.append(getInventoryItemId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>groupId</column-name><column-value><![CDATA[");
		sb.append(getGroupId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>categoryId</column-name><column-value><![CDATA[");
		sb.append(getCategoryId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>name</column-name><column-value><![CDATA[");
		sb.append(getName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>basePrice</column-name><column-value><![CDATA[");
		sb.append(getBasePrice());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantity</column-name><column-value><![CDATA[");
		sb.append(getQuantity());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>imported</column-name><column-value><![CDATA[");
		sb.append(getImported());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _uuid;
	private long _inventoryItemId;
	private long _companyId;
	private long _groupId;
	private long _userId;
	private String _userUuid;
	private long _categoryId;
	private String _name;
	private double _basePrice;
	private int _quantity;
	private boolean _imported;
	private BaseModel<?> _inventoryItemRemoteModel;
	private Class<?> _clpSerializerClass = com.liferay.order.service.ClpSerializer.class;
}