/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link OrderLocalService}.
 *
 * @author Joshua Cords
 * @see OrderLocalService
 * @generated
 */
public class OrderLocalServiceWrapper implements OrderLocalService,
	ServiceWrapper<OrderLocalService> {
	public OrderLocalServiceWrapper(OrderLocalService orderLocalService) {
		_orderLocalService = orderLocalService;
	}

	/**
	* Adds the order to the database. Also notifies the appropriate model listeners.
	*
	* @param order the order
	* @return the order that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.Order addOrder(
		com.liferay.order.model.Order order)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.addOrder(order);
	}

	/**
	* Creates a new order with the primary key. Does not add the order to the database.
	*
	* @param orderId the primary key for the new order
	* @return the new order
	*/
	@Override
	public com.liferay.order.model.Order createOrder(long orderId) {
		return _orderLocalService.createOrder(orderId);
	}

	/**
	* Deletes the order with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param orderId the primary key of the order
	* @return the order that was removed
	* @throws PortalException if a order with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.Order deleteOrder(long orderId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.deleteOrder(orderId);
	}

	/**
	* Deletes the order from the database. Also notifies the appropriate model listeners.
	*
	* @param order the order
	* @return the order that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.Order deleteOrder(
		com.liferay.order.model.Order order)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.deleteOrder(order);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _orderLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.liferay.order.model.Order fetchOrder(long orderId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.fetchOrder(orderId);
	}

	/**
	* Returns the order with the matching UUID and company.
	*
	* @param uuid the order's UUID
	* @param companyId the primary key of the company
	* @return the matching order, or <code>null</code> if a matching order could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.Order fetchOrderByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.fetchOrderByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns the order matching the UUID and group.
	*
	* @param uuid the order's UUID
	* @param groupId the primary key of the group
	* @return the matching order, or <code>null</code> if a matching order could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.Order fetchOrderByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.fetchOrderByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns the order with the primary key.
	*
	* @param orderId the primary key of the order
	* @return the order
	* @throws PortalException if a order with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.Order getOrder(long orderId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.getOrder(orderId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the order with the matching UUID and company.
	*
	* @param uuid the order's UUID
	* @param companyId the primary key of the company
	* @return the matching order
	* @throws PortalException if a matching order could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.Order getOrderByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.getOrderByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns the order matching the UUID and group.
	*
	* @param uuid the order's UUID
	* @param groupId the primary key of the group
	* @return the matching order
	* @throws PortalException if a matching order could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.Order getOrderByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.getOrderByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns a range of all the orders.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of orders
	* @param end the upper bound of the range of orders (not inclusive)
	* @return the range of orders
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.liferay.order.model.Order> getOrders(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.getOrders(start, end);
	}

	/**
	* Returns the number of orders.
	*
	* @return the number of orders
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getOrdersCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.getOrdersCount();
	}

	/**
	* Updates the order in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param order the order
	* @return the order that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.Order updateOrder(
		com.liferay.order.model.Order order)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.updateOrder(order);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _orderLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_orderLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _orderLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	/**
	* Adds new Order to database
	*
	* @param companyId
	* @param groupId
	* @param userId
	* @param contactId
	* @param userName
	* @param street1
	* @param street2
	* @param street3
	* @param city
	* @param regionId
	* @param countryId
	* @param zip
	* @param confirmed
	* @throws IOException
	*/
	@Override
	public com.liferay.order.model.Order addOrder(long companyId, long groupId,
		long userId, boolean confirmed)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.addOrder(companyId, groupId, userId, confirmed);
	}

	/**
	* Updates Order in database
	*
	* @param companyId
	* @param groupId
	* @param userId
	* @param orderId
	* @param contactId
	* @param userName
	* @param street1
	* @param street2
	* @param street3
	* @param city
	* @param regionId
	* @param countryId
	* @param zip
	* @param confirmed
	* @throws IOException
	*/
	@Override
	public com.liferay.order.model.Order updateOrder(long companyId,
		long groupId, long userId, long orderId, boolean confirmed)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.updateOrder(companyId, groupId, userId,
			orderId, confirmed);
	}

	/**
	* Creates a Order with a new orderId without other data
	*
	* @return EmptyOrder with new orderId
	*/
	@Override
	public com.liferay.order.model.Order createOrder() {
		return _orderLocalService.createOrder();
	}

	/**
	* Gets a list with a range of Orders from a user
	*/
	@Override
	public java.util.List<com.liferay.order.model.Order> getOrdersByUserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.getOrdersByUserId(userId, start, end);
	}

	/**
	* Gets the number of Orders from a user
	*/
	@Override
	public int getOrdersCountByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _orderLocalService.getOrdersCountByUserId(userId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public OrderLocalService getWrappedOrderLocalService() {
		return _orderLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedOrderLocalService(OrderLocalService orderLocalService) {
		_orderLocalService = orderLocalService;
	}

	@Override
	public OrderLocalService getWrappedService() {
		return _orderLocalService;
	}

	@Override
	public void setWrappedService(OrderLocalService orderLocalService) {
		_orderLocalService = orderLocalService;
	}

	private OrderLocalService _orderLocalService;
}