/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link InventoryItemLocalService}.
 *
 * @author Joshua Cords
 * @see InventoryItemLocalService
 * @generated
 */
public class InventoryItemLocalServiceWrapper
	implements InventoryItemLocalService,
		ServiceWrapper<InventoryItemLocalService> {
	public InventoryItemLocalServiceWrapper(
		InventoryItemLocalService inventoryItemLocalService) {
		_inventoryItemLocalService = inventoryItemLocalService;
	}

	/**
	* Adds the inventory item to the database. Also notifies the appropriate model listeners.
	*
	* @param inventoryItem the inventory item
	* @return the inventory item that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.InventoryItem addInventoryItem(
		com.liferay.order.model.InventoryItem inventoryItem)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.addInventoryItem(inventoryItem);
	}

	/**
	* Creates a new inventory item with the primary key. Does not add the inventory item to the database.
	*
	* @param inventoryItemId the primary key for the new inventory item
	* @return the new inventory item
	*/
	@Override
	public com.liferay.order.model.InventoryItem createInventoryItem(
		long inventoryItemId) {
		return _inventoryItemLocalService.createInventoryItem(inventoryItemId);
	}

	/**
	* Deletes the inventory item with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param inventoryItemId the primary key of the inventory item
	* @return the inventory item that was removed
	* @throws PortalException if a inventory item with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.InventoryItem deleteInventoryItem(
		long inventoryItemId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.deleteInventoryItem(inventoryItemId);
	}

	/**
	* Deletes the inventory item from the database. Also notifies the appropriate model listeners.
	*
	* @param inventoryItem the inventory item
	* @return the inventory item that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.InventoryItem deleteInventoryItem(
		com.liferay.order.model.InventoryItem inventoryItem)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.deleteInventoryItem(inventoryItem);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _inventoryItemLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public com.liferay.order.model.InventoryItem fetchInventoryItem(
		long inventoryItemId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.fetchInventoryItem(inventoryItemId);
	}

	/**
	* Returns the inventory item with the matching UUID and company.
	*
	* @param uuid the inventory item's UUID
	* @param companyId the primary key of the company
	* @return the matching inventory item, or <code>null</code> if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.InventoryItem fetchInventoryItemByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.fetchInventoryItemByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns the inventory item matching the UUID and group.
	*
	* @param uuid the inventory item's UUID
	* @param groupId the primary key of the group
	* @return the matching inventory item, or <code>null</code> if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.InventoryItem fetchInventoryItemByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.fetchInventoryItemByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Returns the inventory item with the primary key.
	*
	* @param inventoryItemId the primary key of the inventory item
	* @return the inventory item
	* @throws PortalException if a inventory item with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.InventoryItem getInventoryItem(
		long inventoryItemId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.getInventoryItem(inventoryItemId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the inventory item with the matching UUID and company.
	*
	* @param uuid the inventory item's UUID
	* @param companyId the primary key of the company
	* @return the matching inventory item
	* @throws PortalException if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.InventoryItem getInventoryItemByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.getInventoryItemByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns the inventory item matching the UUID and group.
	*
	* @param uuid the inventory item's UUID
	* @param groupId the primary key of the group
	* @return the matching inventory item
	* @throws PortalException if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.InventoryItem getInventoryItemByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.getInventoryItemByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Returns a range of all the inventory items.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of inventory items
	* @param end the upper bound of the range of inventory items (not inclusive)
	* @return the range of inventory items
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.liferay.order.model.InventoryItem> getInventoryItems(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.getInventoryItems(start, end);
	}

	/**
	* Returns the number of inventory items.
	*
	* @return the number of inventory items
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getInventoryItemsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.getInventoryItemsCount();
	}

	/**
	* Updates the inventory item in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param inventoryItem the inventory item
	* @return the inventory item that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.InventoryItem updateInventoryItem(
		com.liferay.order.model.InventoryItem inventoryItem)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.updateInventoryItem(inventoryItem);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _inventoryItemLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_inventoryItemLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _inventoryItemLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	* Creates and adds InventoryItem to database
	*
	* @param companyId
	* @param groupId
	* @param userId
	* @param name
	* @param quantity
	* @param basePrice
	* @param categoryId
	* @param imported
	* @return InventoryItem
	* @throws com.liferay.portal.kernel.exception.SystemException
	*/
	@Override
	public com.liferay.order.model.InventoryItem addInventoryItem(
		long companyId, long groupId, long userId, java.lang.String name,
		int quantity, double basePrice, long categoryId, boolean imported)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.addInventoryItem(companyId, groupId,
			userId, name, quantity, basePrice, categoryId, imported);
	}

	/**
	* Updates InventoryItem in database
	*
	* @param inventoryItem
	* @return InventoryItem
	* @throws com.liferay.portal.kernel.exception.SystemException
	*/
	@Override
	public com.liferay.order.model.InventoryItem updateInventoryItem(
		long companyId, long groupId, long userId, long inventoryItemId,
		java.lang.String name, int quantity, double basePrice, long categoryId,
		boolean imported)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.updateInventoryItem(companyId,
			groupId, userId, inventoryItemId, name, quantity, basePrice,
			categoryId, imported);
	}

	/**
	* Gets a list with all the InventoryItems in a group
	*/
	@Override
	public java.util.List<com.liferay.order.model.InventoryItem> getInventoryItemsByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.getInventoryItemsByGroupId(groupId);
	}

	/**
	* Gets a list with a range of InventoryItems from a group
	*/
	@Override
	public java.util.List<com.liferay.order.model.InventoryItem> getInventoryItemsByGroupId(
		long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.getInventoryItemsByGroupId(groupId,
			start, end);
	}

	/**
	* Gets the number of InventoryItems in a group
	*/
	@Override
	public int getInventoryItemsCountByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _inventoryItemLocalService.getInventoryItemsCountByGroupId(groupId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public InventoryItemLocalService getWrappedInventoryItemLocalService() {
		return _inventoryItemLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedInventoryItemLocalService(
		InventoryItemLocalService inventoryItemLocalService) {
		_inventoryItemLocalService = inventoryItemLocalService;
	}

	@Override
	public InventoryItemLocalService getWrappedService() {
		return _inventoryItemLocalService;
	}

	@Override
	public void setWrappedService(
		InventoryItemLocalService inventoryItemLocalService) {
		_inventoryItemLocalService = inventoryItemLocalService;
	}

	private InventoryItemLocalService _inventoryItemLocalService;
}