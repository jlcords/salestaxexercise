/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link TaxDutyLocalService}.
 *
 * @author Joshua Cords
 * @see TaxDutyLocalService
 * @generated
 */
public class TaxDutyLocalServiceWrapper implements TaxDutyLocalService,
	ServiceWrapper<TaxDutyLocalService> {
	public TaxDutyLocalServiceWrapper(TaxDutyLocalService taxDutyLocalService) {
		_taxDutyLocalService = taxDutyLocalService;
	}

	/**
	* Adds the tax duty to the database. Also notifies the appropriate model listeners.
	*
	* @param taxDuty the tax duty
	* @return the tax duty that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.TaxDuty addTaxDuty(
		com.liferay.order.model.TaxDuty taxDuty)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.addTaxDuty(taxDuty);
	}

	/**
	* Creates a new tax duty with the primary key. Does not add the tax duty to the database.
	*
	* @param taxDutyId the primary key for the new tax duty
	* @return the new tax duty
	*/
	@Override
	public com.liferay.order.model.TaxDuty createTaxDuty(long taxDutyId) {
		return _taxDutyLocalService.createTaxDuty(taxDutyId);
	}

	/**
	* Deletes the tax duty with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param taxDutyId the primary key of the tax duty
	* @return the tax duty that was removed
	* @throws PortalException if a tax duty with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.TaxDuty deleteTaxDuty(long taxDutyId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.deleteTaxDuty(taxDutyId);
	}

	/**
	* Deletes the tax duty from the database. Also notifies the appropriate model listeners.
	*
	* @param taxDuty the tax duty
	* @return the tax duty that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.TaxDuty deleteTaxDuty(
		com.liferay.order.model.TaxDuty taxDuty)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.deleteTaxDuty(taxDuty);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _taxDutyLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.liferay.order.model.TaxDuty fetchTaxDuty(long taxDutyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.fetchTaxDuty(taxDutyId);
	}

	/**
	* Returns the tax duty with the matching UUID and company.
	*
	* @param uuid the tax duty's UUID
	* @param companyId the primary key of the company
	* @return the matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.TaxDuty fetchTaxDutyByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.fetchTaxDutyByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns the tax duty matching the UUID and group.
	*
	* @param uuid the tax duty's UUID
	* @param groupId the primary key of the group
	* @return the matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.TaxDuty fetchTaxDutyByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.fetchTaxDutyByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns the tax duty with the primary key.
	*
	* @param taxDutyId the primary key of the tax duty
	* @return the tax duty
	* @throws PortalException if a tax duty with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.TaxDuty getTaxDuty(long taxDutyId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.getTaxDuty(taxDutyId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the tax duty with the matching UUID and company.
	*
	* @param uuid the tax duty's UUID
	* @param companyId the primary key of the company
	* @return the matching tax duty
	* @throws PortalException if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.TaxDuty getTaxDutyByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.getTaxDutyByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns the tax duty matching the UUID and group.
	*
	* @param uuid the tax duty's UUID
	* @param groupId the primary key of the group
	* @return the matching tax duty
	* @throws PortalException if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.TaxDuty getTaxDutyByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.getTaxDutyByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns a range of all the tax duties.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tax duties
	* @param end the upper bound of the range of tax duties (not inclusive)
	* @return the range of tax duties
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.liferay.order.model.TaxDuty> getTaxDuties(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.getTaxDuties(start, end);
	}

	/**
	* Returns the number of tax duties.
	*
	* @return the number of tax duties
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getTaxDutiesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.getTaxDutiesCount();
	}

	/**
	* Updates the tax duty in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param taxDuty the tax duty
	* @return the tax duty that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.TaxDuty updateTaxDuty(
		com.liferay.order.model.TaxDuty taxDuty)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.updateTaxDuty(taxDuty);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _taxDutyLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_taxDutyLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _taxDutyLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	/**
	* Adds TaxDuty Item to database
	*
	* @param companyId
	* @param groupId
	* @param taxDutyId
	* @param tax
	* @param duty
	* @param round
	* @return
	* @throws SystemException
	*/
	@Override
	public com.liferay.order.model.TaxDuty addTaxDuty(long companyId,
		long groupId, int tax, int duty, int round)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.addTaxDuty(companyId, groupId, tax, duty,
			round);
	}

	/**
	* Returns TaxDuty for CompanyId and GroupId
	*
	* @param companyId
	* @param groupId
	* @return TaxDuty
	*/
	@Override
	public com.liferay.order.model.TaxDuty getTaxDutybyGroupId(long groupId) {
		return _taxDutyLocalService.getTaxDutybyGroupId(groupId);
	}

	/**
	* Updates TaxDuty Item in database
	*
	* @param companyId
	* @param groupId
	* @param taxDutyId
	* @param tax
	* @param duty
	* @param round
	* @return
	* @throws SystemException
	*/
	@Override
	public com.liferay.order.model.TaxDuty updateTaxDuty(long companyId,
		long groupId, long taxDutyId, int tax, int duty, int round)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _taxDutyLocalService.updateTaxDuty(companyId, groupId,
			taxDutyId, tax, duty, round);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public TaxDutyLocalService getWrappedTaxDutyLocalService() {
		return _taxDutyLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedTaxDutyLocalService(
		TaxDutyLocalService taxDutyLocalService) {
		_taxDutyLocalService = taxDutyLocalService;
	}

	@Override
	public TaxDutyLocalService getWrappedService() {
		return _taxDutyLocalService;
	}

	@Override
	public void setWrappedService(TaxDutyLocalService taxDutyLocalService) {
		_taxDutyLocalService = taxDutyLocalService;
	}

	private TaxDutyLocalService _taxDutyLocalService;
}