/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link FinalItemLocalService}.
 *
 * @author Joshua Cords
 * @see FinalItemLocalService
 * @generated
 */
public class FinalItemLocalServiceWrapper implements FinalItemLocalService,
	ServiceWrapper<FinalItemLocalService> {
	public FinalItemLocalServiceWrapper(
		FinalItemLocalService finalItemLocalService) {
		_finalItemLocalService = finalItemLocalService;
	}

	/**
	* Adds the final item to the database. Also notifies the appropriate model listeners.
	*
	* @param finalItem the final item
	* @return the final item that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.FinalItem addFinalItem(
		com.liferay.order.model.FinalItem finalItem)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.addFinalItem(finalItem);
	}

	/**
	* Creates a new final item with the primary key. Does not add the final item to the database.
	*
	* @param finalItemId the primary key for the new final item
	* @return the new final item
	*/
	@Override
	public com.liferay.order.model.FinalItem createFinalItem(long finalItemId) {
		return _finalItemLocalService.createFinalItem(finalItemId);
	}

	/**
	* Deletes the final item with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param finalItemId the primary key of the final item
	* @return the final item that was removed
	* @throws PortalException if a final item with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.FinalItem deleteFinalItem(long finalItemId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.deleteFinalItem(finalItemId);
	}

	/**
	* Deletes the final item from the database. Also notifies the appropriate model listeners.
	*
	* @param finalItem the final item
	* @return the final item that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.FinalItem deleteFinalItem(
		com.liferay.order.model.FinalItem finalItem)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.deleteFinalItem(finalItem);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _finalItemLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.liferay.order.model.FinalItem fetchFinalItem(long finalItemId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.fetchFinalItem(finalItemId);
	}

	/**
	* Returns the final item with the matching UUID and company.
	*
	* @param uuid the final item's UUID
	* @param companyId the primary key of the company
	* @return the matching final item, or <code>null</code> if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.FinalItem fetchFinalItemByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.fetchFinalItemByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns the final item matching the UUID and group.
	*
	* @param uuid the final item's UUID
	* @param groupId the primary key of the group
	* @return the matching final item, or <code>null</code> if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.FinalItem fetchFinalItemByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.fetchFinalItemByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Returns the final item with the primary key.
	*
	* @param finalItemId the primary key of the final item
	* @return the final item
	* @throws PortalException if a final item with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.FinalItem getFinalItem(long finalItemId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.getFinalItem(finalItemId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the final item with the matching UUID and company.
	*
	* @param uuid the final item's UUID
	* @param companyId the primary key of the company
	* @return the matching final item
	* @throws PortalException if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.FinalItem getFinalItemByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.getFinalItemByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns the final item matching the UUID and group.
	*
	* @param uuid the final item's UUID
	* @param groupId the primary key of the group
	* @return the matching final item
	* @throws PortalException if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.FinalItem getFinalItemByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.getFinalItemByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns a range of all the final items.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of final items
	* @param end the upper bound of the range of final items (not inclusive)
	* @return the range of final items
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.liferay.order.model.FinalItem> getFinalItems(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.getFinalItems(start, end);
	}

	/**
	* Returns the number of final items.
	*
	* @return the number of final items
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getFinalItemsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.getFinalItemsCount();
	}

	/**
	* Updates the final item in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param finalItem the final item
	* @return the final item that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferay.order.model.FinalItem updateFinalItem(
		com.liferay.order.model.FinalItem finalItem)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.updateFinalItem(finalItem);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _finalItemLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_finalItemLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _finalItemLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public com.liferay.order.model.FinalItem addFinalItem(
		com.liferay.order.model.InventoryItem inventoryItem, long companyId,
		long groupId, long userId, long orderId, int quantity,
		double taxPercent, double dutyPercent)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.addFinalItem(inventoryItem, companyId,
			groupId, userId, orderId, quantity, taxPercent, dutyPercent);
	}

	/**
	* Returns all items in an Order
	*
	* @param orderId
	* @param start
	* @param end
	* @return
	* @throws SystemException
	*/
	@Override
	public java.util.List<com.liferay.order.model.FinalItem> getFinalItemsbyOrderId(
		long orderId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.getFinalItemsbyOrderId(orderId);
	}

	/**
	* Returns all items in an Order
	*
	* @param orderId
	* @param start
	* @param end
	* @return
	* @throws SystemException
	*/
	@Override
	public java.util.List<com.liferay.order.model.FinalItem> getFinalItemsbyOrderId(
		long orderId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.getFinalItemsbyOrderId(orderId, start, end);
	}

	/**
	* Return number of items in an Order
	*
	* @param orderId
	* @return
	* @throws SystemException
	*/
	@Override
	public int getFinalItemsCountbyOrderId(long orderId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.getFinalItemsCountbyOrderId(orderId);
	}

	@Override
	public double getTaxSumbyOrderId(long orderId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.getTaxSumbyOrderId(orderId);
	}

	@Override
	public double getDutySumbyOrderId(long orderId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.getDutySumbyOrderId(orderId);
	}

	@Override
	public double getTotalPriceSumbyOrderId(long orderId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _finalItemLocalService.getTotalPriceSumbyOrderId(orderId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public FinalItemLocalService getWrappedFinalItemLocalService() {
		return _finalItemLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedFinalItemLocalService(
		FinalItemLocalService finalItemLocalService) {
		_finalItemLocalService = finalItemLocalService;
	}

	@Override
	public FinalItemLocalService getWrappedService() {
		return _finalItemLocalService;
	}

	@Override
	public void setWrappedService(FinalItemLocalService finalItemLocalService) {
		_finalItemLocalService = finalItemLocalService;
	}

	private FinalItemLocalService _finalItemLocalService;
}