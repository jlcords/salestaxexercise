/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service.messaging;

import com.liferay.order.service.CategoryLocalServiceUtil;
import com.liferay.order.service.ClpSerializer;
import com.liferay.order.service.FinalItemLocalServiceUtil;
import com.liferay.order.service.InventoryItemLocalServiceUtil;
import com.liferay.order.service.OrderLocalServiceUtil;
import com.liferay.order.service.TaxDutyLocalServiceUtil;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;

/**
 * @author Joshua Cords
 */
public class ClpMessageListener extends BaseMessageListener {
	public static String getServletContextName() {
		return ClpSerializer.getServletContextName();
	}

	@Override
	protected void doReceive(Message message) throws Exception {
		String command = message.getString("command");
		String servletContextName = message.getString("servletContextName");

		if (command.equals("undeploy") &&
				servletContextName.equals(getServletContextName())) {
			CategoryLocalServiceUtil.clearService();

			FinalItemLocalServiceUtil.clearService();

			InventoryItemLocalServiceUtil.clearService();

			OrderLocalServiceUtil.clearService();

			TaxDutyLocalServiceUtil.clearService();
		}
	}
}