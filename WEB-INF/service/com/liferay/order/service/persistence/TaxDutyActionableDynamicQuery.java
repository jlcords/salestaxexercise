/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service.persistence;

import com.liferay.order.model.TaxDuty;
import com.liferay.order.service.TaxDutyLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Joshua Cords
 * @generated
 */
public abstract class TaxDutyActionableDynamicQuery
	extends BaseActionableDynamicQuery {
	public TaxDutyActionableDynamicQuery() throws SystemException {
		setBaseLocalService(TaxDutyLocalServiceUtil.getService());
		setClass(TaxDuty.class);

		setClassLoader(com.liferay.order.service.ClpSerializer.class.getClassLoader());

		setPrimaryKeyPropertyName("taxDutyId");
	}
}