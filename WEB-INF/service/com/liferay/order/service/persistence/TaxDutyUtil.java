/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service.persistence;

import com.liferay.order.model.TaxDuty;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the tax duty service. This utility wraps {@link TaxDutyPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Joshua Cords
 * @see TaxDutyPersistence
 * @see TaxDutyPersistenceImpl
 * @generated
 */
public class TaxDutyUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(TaxDuty taxDuty) {
		getPersistence().clearCache(taxDuty);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<TaxDuty> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<TaxDuty> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<TaxDuty> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static TaxDuty update(TaxDuty taxDuty) throws SystemException {
		return getPersistence().update(taxDuty);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static TaxDuty update(TaxDuty taxDuty, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(taxDuty, serviceContext);
	}

	/**
	* Returns all the tax duties where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.order.model.TaxDuty> findByUuid(
		java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the tax duties where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of tax duties
	* @param end the upper bound of the range of tax duties (not inclusive)
	* @return the range of matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.order.model.TaxDuty> findByUuid(
		java.lang.String uuid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the tax duties where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of tax duties
	* @param end the upper bound of the range of tax duties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.order.model.TaxDuty> findByUuid(
		java.lang.String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns the first tax duty in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty findByUuid_First(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first tax duty in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty fetchByUuid_First(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last tax duty in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty findByUuid_Last(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last tax duty in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty fetchByUuid_Last(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the tax duties before and after the current tax duty in the ordered set where uuid = &#63;.
	*
	* @param taxDutyId the primary key of the current tax duty
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a tax duty with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty[] findByUuid_PrevAndNext(
		long taxDutyId, java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUuid_PrevAndNext(taxDutyId, uuid, orderByComparator);
	}

	/**
	* Removes all the tax duties where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUuid(java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of tax duties where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUuid(java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the tax duty where uuid = &#63; and groupId = &#63; or throws a {@link com.liferay.order.NoSuchTaxDutyException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty findByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the tax duty where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty fetchByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the tax duty where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty fetchByUUID_G(
		java.lang.String uuid, long groupId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the tax duty where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the tax duty that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty removeByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of tax duties where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUUID_G(java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the tax duties where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.order.model.TaxDuty> findByUuid_C(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the tax duties where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of tax duties
	* @param end the upper bound of the range of tax duties (not inclusive)
	* @return the range of matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.order.model.TaxDuty> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the tax duties where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of tax duties
	* @param end the upper bound of the range of tax duties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.order.model.TaxDuty> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first tax duty in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty findByUuid_C_First(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first tax duty in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty fetchByUuid_C_First(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last tax duty in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty findByUuid_C_Last(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last tax duty in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty fetchByUuid_C_Last(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the tax duties before and after the current tax duty in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param taxDutyId the primary key of the current tax duty
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a tax duty with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty[] findByUuid_C_PrevAndNext(
		long taxDutyId, java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(taxDutyId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the tax duties where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of tax duties where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns the tax duty where groupId = &#63; or throws a {@link com.liferay.order.NoSuchTaxDutyException} if it could not be found.
	*
	* @param groupId the group ID
	* @return the matching tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty findByGroupId(long groupId)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	* Returns the tax duty where groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param groupId the group ID
	* @return the matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty fetchByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByGroupId(groupId);
	}

	/**
	* Returns the tax duty where groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param groupId the group ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty fetchByGroupId(long groupId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByGroupId(groupId, retrieveFromCache);
	}

	/**
	* Removes the tax duty where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	* @return the tax duty that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty removeByGroupId(long groupId)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().removeByGroupId(groupId);
	}

	/**
	* Returns the number of tax duties where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public static int countByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	* Caches the tax duty in the entity cache if it is enabled.
	*
	* @param taxDuty the tax duty
	*/
	public static void cacheResult(com.liferay.order.model.TaxDuty taxDuty) {
		getPersistence().cacheResult(taxDuty);
	}

	/**
	* Caches the tax duties in the entity cache if it is enabled.
	*
	* @param taxDuties the tax duties
	*/
	public static void cacheResult(
		java.util.List<com.liferay.order.model.TaxDuty> taxDuties) {
		getPersistence().cacheResult(taxDuties);
	}

	/**
	* Creates a new tax duty with the primary key. Does not add the tax duty to the database.
	*
	* @param taxDutyId the primary key for the new tax duty
	* @return the new tax duty
	*/
	public static com.liferay.order.model.TaxDuty create(long taxDutyId) {
		return getPersistence().create(taxDutyId);
	}

	/**
	* Removes the tax duty with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param taxDutyId the primary key of the tax duty
	* @return the tax duty that was removed
	* @throws com.liferay.order.NoSuchTaxDutyException if a tax duty with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty remove(long taxDutyId)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().remove(taxDutyId);
	}

	public static com.liferay.order.model.TaxDuty updateImpl(
		com.liferay.order.model.TaxDuty taxDuty)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(taxDuty);
	}

	/**
	* Returns the tax duty with the primary key or throws a {@link com.liferay.order.NoSuchTaxDutyException} if it could not be found.
	*
	* @param taxDutyId the primary key of the tax duty
	* @return the tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a tax duty with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty findByPrimaryKey(
		long taxDutyId)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPrimaryKey(taxDutyId);
	}

	/**
	* Returns the tax duty with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param taxDutyId the primary key of the tax duty
	* @return the tax duty, or <code>null</code> if a tax duty with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.order.model.TaxDuty fetchByPrimaryKey(
		long taxDutyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(taxDutyId);
	}

	/**
	* Returns all the tax duties.
	*
	* @return the tax duties
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.order.model.TaxDuty> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the tax duties.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tax duties
	* @param end the upper bound of the range of tax duties (not inclusive)
	* @return the range of tax duties
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.order.model.TaxDuty> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the tax duties.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tax duties
	* @param end the upper bound of the range of tax duties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of tax duties
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.order.model.TaxDuty> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the tax duties from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of tax duties.
	*
	* @return the number of tax duties
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static TaxDutyPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (TaxDutyPersistence)PortletBeanLocatorUtil.locate(com.liferay.order.service.ClpSerializer.getServletContextName(),
					TaxDutyPersistence.class.getName());

			ReferenceRegistry.registerReference(TaxDutyUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(TaxDutyPersistence persistence) {
	}

	private static TaxDutyPersistence _persistence;
}