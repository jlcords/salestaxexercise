/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service.persistence;

import com.liferay.order.model.InventoryItem;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the inventory item service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Joshua Cords
 * @see InventoryItemPersistenceImpl
 * @see InventoryItemUtil
 * @generated
 */
public interface InventoryItemPersistence extends BasePersistence<InventoryItem> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link InventoryItemUtil} to access the inventory item persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the inventory items where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching inventory items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.InventoryItem> findByUuid(
		java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the inventory items where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of inventory items
	* @param end the upper bound of the range of inventory items (not inclusive)
	* @return the range of matching inventory items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.InventoryItem> findByUuid(
		java.lang.String uuid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the inventory items where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of inventory items
	* @param end the upper bound of the range of inventory items (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching inventory items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.InventoryItem> findByUuid(
		java.lang.String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first inventory item in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching inventory item
	* @throws com.liferay.order.NoSuchInventoryItemException if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem findByUuid_First(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchInventoryItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first inventory item in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching inventory item, or <code>null</code> if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem fetchByUuid_First(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last inventory item in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching inventory item
	* @throws com.liferay.order.NoSuchInventoryItemException if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem findByUuid_Last(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchInventoryItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last inventory item in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching inventory item, or <code>null</code> if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem fetchByUuid_Last(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the inventory items before and after the current inventory item in the ordered set where uuid = &#63;.
	*
	* @param inventoryItemId the primary key of the current inventory item
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next inventory item
	* @throws com.liferay.order.NoSuchInventoryItemException if a inventory item with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem[] findByUuid_PrevAndNext(
		long inventoryItemId, java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchInventoryItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the inventory items where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUuid(java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of inventory items where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching inventory items
	* @throws SystemException if a system exception occurred
	*/
	public int countByUuid(java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the inventory item where uuid = &#63; and groupId = &#63; or throws a {@link com.liferay.order.NoSuchInventoryItemException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching inventory item
	* @throws com.liferay.order.NoSuchInventoryItemException if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem findByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.order.NoSuchInventoryItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the inventory item where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching inventory item, or <code>null</code> if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem fetchByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the inventory item where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching inventory item, or <code>null</code> if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem fetchByUUID_G(
		java.lang.String uuid, long groupId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the inventory item where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the inventory item that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem removeByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.order.NoSuchInventoryItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of inventory items where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching inventory items
	* @throws SystemException if a system exception occurred
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the inventory items where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching inventory items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.InventoryItem> findByUuid_C(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the inventory items where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of inventory items
	* @param end the upper bound of the range of inventory items (not inclusive)
	* @return the range of matching inventory items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.InventoryItem> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the inventory items where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of inventory items
	* @param end the upper bound of the range of inventory items (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching inventory items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.InventoryItem> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first inventory item in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching inventory item
	* @throws com.liferay.order.NoSuchInventoryItemException if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem findByUuid_C_First(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchInventoryItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first inventory item in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching inventory item, or <code>null</code> if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem fetchByUuid_C_First(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last inventory item in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching inventory item
	* @throws com.liferay.order.NoSuchInventoryItemException if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem findByUuid_C_Last(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchInventoryItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last inventory item in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching inventory item, or <code>null</code> if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem fetchByUuid_C_Last(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the inventory items before and after the current inventory item in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param inventoryItemId the primary key of the current inventory item
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next inventory item
	* @throws com.liferay.order.NoSuchInventoryItemException if a inventory item with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem[] findByUuid_C_PrevAndNext(
		long inventoryItemId, java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchInventoryItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the inventory items where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of inventory items where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching inventory items
	* @throws SystemException if a system exception occurred
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the inventory items where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching inventory items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.InventoryItem> findByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the inventory items where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of inventory items
	* @param end the upper bound of the range of inventory items (not inclusive)
	* @return the range of matching inventory items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.InventoryItem> findByGroupId(
		long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the inventory items where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of inventory items
	* @param end the upper bound of the range of inventory items (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching inventory items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.InventoryItem> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first inventory item in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching inventory item
	* @throws com.liferay.order.NoSuchInventoryItemException if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem findByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchInventoryItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first inventory item in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching inventory item, or <code>null</code> if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last inventory item in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching inventory item
	* @throws com.liferay.order.NoSuchInventoryItemException if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem findByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchInventoryItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last inventory item in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching inventory item, or <code>null</code> if a matching inventory item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the inventory items before and after the current inventory item in the ordered set where groupId = &#63;.
	*
	* @param inventoryItemId the primary key of the current inventory item
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next inventory item
	* @throws com.liferay.order.NoSuchInventoryItemException if a inventory item with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem[] findByGroupId_PrevAndNext(
		long inventoryItemId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchInventoryItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the inventory items where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of inventory items where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching inventory items
	* @throws SystemException if a system exception occurred
	*/
	public int countByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the inventory item in the entity cache if it is enabled.
	*
	* @param inventoryItem the inventory item
	*/
	public void cacheResult(com.liferay.order.model.InventoryItem inventoryItem);

	/**
	* Caches the inventory items in the entity cache if it is enabled.
	*
	* @param inventoryItems the inventory items
	*/
	public void cacheResult(
		java.util.List<com.liferay.order.model.InventoryItem> inventoryItems);

	/**
	* Creates a new inventory item with the primary key. Does not add the inventory item to the database.
	*
	* @param inventoryItemId the primary key for the new inventory item
	* @return the new inventory item
	*/
	public com.liferay.order.model.InventoryItem create(long inventoryItemId);

	/**
	* Removes the inventory item with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param inventoryItemId the primary key of the inventory item
	* @return the inventory item that was removed
	* @throws com.liferay.order.NoSuchInventoryItemException if a inventory item with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem remove(long inventoryItemId)
		throws com.liferay.order.NoSuchInventoryItemException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.liferay.order.model.InventoryItem updateImpl(
		com.liferay.order.model.InventoryItem inventoryItem)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the inventory item with the primary key or throws a {@link com.liferay.order.NoSuchInventoryItemException} if it could not be found.
	*
	* @param inventoryItemId the primary key of the inventory item
	* @return the inventory item
	* @throws com.liferay.order.NoSuchInventoryItemException if a inventory item with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem findByPrimaryKey(
		long inventoryItemId)
		throws com.liferay.order.NoSuchInventoryItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the inventory item with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param inventoryItemId the primary key of the inventory item
	* @return the inventory item, or <code>null</code> if a inventory item with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.InventoryItem fetchByPrimaryKey(
		long inventoryItemId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the inventory items.
	*
	* @return the inventory items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.InventoryItem> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the inventory items.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of inventory items
	* @param end the upper bound of the range of inventory items (not inclusive)
	* @return the range of inventory items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.InventoryItem> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the inventory items.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.InventoryItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of inventory items
	* @param end the upper bound of the range of inventory items (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of inventory items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.InventoryItem> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the inventory items from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of inventory items.
	*
	* @return the number of inventory items
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}