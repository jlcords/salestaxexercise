/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service.persistence;

import com.liferay.order.model.FinalItem;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the final item service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Joshua Cords
 * @see FinalItemPersistenceImpl
 * @see FinalItemUtil
 * @generated
 */
public interface FinalItemPersistence extends BasePersistence<FinalItem> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link FinalItemUtil} to access the final item persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the final items where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching final items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.FinalItem> findByUuid(
		java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the final items where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of final items
	* @param end the upper bound of the range of final items (not inclusive)
	* @return the range of matching final items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.FinalItem> findByUuid(
		java.lang.String uuid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the final items where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of final items
	* @param end the upper bound of the range of final items (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching final items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.FinalItem> findByUuid(
		java.lang.String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first final item in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching final item
	* @throws com.liferay.order.NoSuchFinalItemException if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem findByUuid_First(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchFinalItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first final item in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching final item, or <code>null</code> if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem fetchByUuid_First(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last final item in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching final item
	* @throws com.liferay.order.NoSuchFinalItemException if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem findByUuid_Last(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchFinalItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last final item in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching final item, or <code>null</code> if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem fetchByUuid_Last(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the final items before and after the current final item in the ordered set where uuid = &#63;.
	*
	* @param finalItemId the primary key of the current final item
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next final item
	* @throws com.liferay.order.NoSuchFinalItemException if a final item with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem[] findByUuid_PrevAndNext(
		long finalItemId, java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchFinalItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the final items where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUuid(java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of final items where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching final items
	* @throws SystemException if a system exception occurred
	*/
	public int countByUuid(java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the final item where uuid = &#63; and groupId = &#63; or throws a {@link com.liferay.order.NoSuchFinalItemException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching final item
	* @throws com.liferay.order.NoSuchFinalItemException if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem findByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.order.NoSuchFinalItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the final item where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching final item, or <code>null</code> if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem fetchByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the final item where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching final item, or <code>null</code> if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem fetchByUUID_G(
		java.lang.String uuid, long groupId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the final item where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the final item that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem removeByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.order.NoSuchFinalItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of final items where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching final items
	* @throws SystemException if a system exception occurred
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the final items where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching final items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.FinalItem> findByUuid_C(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the final items where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of final items
	* @param end the upper bound of the range of final items (not inclusive)
	* @return the range of matching final items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.FinalItem> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the final items where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of final items
	* @param end the upper bound of the range of final items (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching final items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.FinalItem> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first final item in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching final item
	* @throws com.liferay.order.NoSuchFinalItemException if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem findByUuid_C_First(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchFinalItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first final item in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching final item, or <code>null</code> if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem fetchByUuid_C_First(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last final item in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching final item
	* @throws com.liferay.order.NoSuchFinalItemException if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem findByUuid_C_Last(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchFinalItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last final item in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching final item, or <code>null</code> if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem fetchByUuid_C_Last(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the final items before and after the current final item in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param finalItemId the primary key of the current final item
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next final item
	* @throws com.liferay.order.NoSuchFinalItemException if a final item with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem[] findByUuid_C_PrevAndNext(
		long finalItemId, java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchFinalItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the final items where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of final items where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching final items
	* @throws SystemException if a system exception occurred
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the final items where orderId = &#63;.
	*
	* @param orderId the order ID
	* @return the matching final items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.FinalItem> findByOrderId(
		long orderId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the final items where orderId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param orderId the order ID
	* @param start the lower bound of the range of final items
	* @param end the upper bound of the range of final items (not inclusive)
	* @return the range of matching final items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.FinalItem> findByOrderId(
		long orderId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the final items where orderId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param orderId the order ID
	* @param start the lower bound of the range of final items
	* @param end the upper bound of the range of final items (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching final items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.FinalItem> findByOrderId(
		long orderId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first final item in the ordered set where orderId = &#63;.
	*
	* @param orderId the order ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching final item
	* @throws com.liferay.order.NoSuchFinalItemException if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem findByOrderId_First(long orderId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchFinalItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first final item in the ordered set where orderId = &#63;.
	*
	* @param orderId the order ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching final item, or <code>null</code> if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem fetchByOrderId_First(
		long orderId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last final item in the ordered set where orderId = &#63;.
	*
	* @param orderId the order ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching final item
	* @throws com.liferay.order.NoSuchFinalItemException if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem findByOrderId_Last(long orderId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchFinalItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last final item in the ordered set where orderId = &#63;.
	*
	* @param orderId the order ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching final item, or <code>null</code> if a matching final item could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem fetchByOrderId_Last(long orderId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the final items before and after the current final item in the ordered set where orderId = &#63;.
	*
	* @param finalItemId the primary key of the current final item
	* @param orderId the order ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next final item
	* @throws com.liferay.order.NoSuchFinalItemException if a final item with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem[] findByOrderId_PrevAndNext(
		long finalItemId, long orderId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchFinalItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the final items where orderId = &#63; from the database.
	*
	* @param orderId the order ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByOrderId(long orderId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of final items where orderId = &#63;.
	*
	* @param orderId the order ID
	* @return the number of matching final items
	* @throws SystemException if a system exception occurred
	*/
	public int countByOrderId(long orderId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the final item in the entity cache if it is enabled.
	*
	* @param finalItem the final item
	*/
	public void cacheResult(com.liferay.order.model.FinalItem finalItem);

	/**
	* Caches the final items in the entity cache if it is enabled.
	*
	* @param finalItems the final items
	*/
	public void cacheResult(
		java.util.List<com.liferay.order.model.FinalItem> finalItems);

	/**
	* Creates a new final item with the primary key. Does not add the final item to the database.
	*
	* @param finalItemId the primary key for the new final item
	* @return the new final item
	*/
	public com.liferay.order.model.FinalItem create(long finalItemId);

	/**
	* Removes the final item with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param finalItemId the primary key of the final item
	* @return the final item that was removed
	* @throws com.liferay.order.NoSuchFinalItemException if a final item with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem remove(long finalItemId)
		throws com.liferay.order.NoSuchFinalItemException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.liferay.order.model.FinalItem updateImpl(
		com.liferay.order.model.FinalItem finalItem)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the final item with the primary key or throws a {@link com.liferay.order.NoSuchFinalItemException} if it could not be found.
	*
	* @param finalItemId the primary key of the final item
	* @return the final item
	* @throws com.liferay.order.NoSuchFinalItemException if a final item with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem findByPrimaryKey(long finalItemId)
		throws com.liferay.order.NoSuchFinalItemException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the final item with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param finalItemId the primary key of the final item
	* @return the final item, or <code>null</code> if a final item with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.FinalItem fetchByPrimaryKey(long finalItemId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the final items.
	*
	* @return the final items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.FinalItem> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the final items.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of final items
	* @param end the upper bound of the range of final items (not inclusive)
	* @return the range of final items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.FinalItem> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the final items.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.FinalItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of final items
	* @param end the upper bound of the range of final items (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of final items
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.FinalItem> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the final items from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of final items.
	*
	* @return the number of final items
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}