/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package com.liferay.order.service.persistence;

import com.liferay.order.model.TaxDuty;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the tax duty service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Joshua Cords
 * @see TaxDutyPersistenceImpl
 * @see TaxDutyUtil
 * @generated
 */
public interface TaxDutyPersistence extends BasePersistence<TaxDuty> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link TaxDutyUtil} to access the tax duty persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the tax duties where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.TaxDuty> findByUuid(
		java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the tax duties where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of tax duties
	* @param end the upper bound of the range of tax duties (not inclusive)
	* @return the range of matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.TaxDuty> findByUuid(
		java.lang.String uuid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the tax duties where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of tax duties
	* @param end the upper bound of the range of tax duties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.TaxDuty> findByUuid(
		java.lang.String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first tax duty in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty findByUuid_First(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first tax duty in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty fetchByUuid_First(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last tax duty in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty findByUuid_Last(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last tax duty in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty fetchByUuid_Last(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the tax duties before and after the current tax duty in the ordered set where uuid = &#63;.
	*
	* @param taxDutyId the primary key of the current tax duty
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a tax duty with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty[] findByUuid_PrevAndNext(
		long taxDutyId, java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the tax duties where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUuid(java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of tax duties where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public int countByUuid(java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the tax duty where uuid = &#63; and groupId = &#63; or throws a {@link com.liferay.order.NoSuchTaxDutyException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty findByUUID_G(java.lang.String uuid,
		long groupId)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the tax duty where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty fetchByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the tax duty where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty fetchByUUID_G(
		java.lang.String uuid, long groupId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the tax duty where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the tax duty that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty removeByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of tax duties where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the tax duties where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.TaxDuty> findByUuid_C(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the tax duties where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of tax duties
	* @param end the upper bound of the range of tax duties (not inclusive)
	* @return the range of matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.TaxDuty> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the tax duties where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of tax duties
	* @param end the upper bound of the range of tax duties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.TaxDuty> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first tax duty in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty findByUuid_C_First(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first tax duty in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty fetchByUuid_C_First(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last tax duty in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty findByUuid_C_Last(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last tax duty in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty fetchByUuid_C_Last(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the tax duties before and after the current tax duty in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param taxDutyId the primary key of the current tax duty
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a tax duty with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty[] findByUuid_C_PrevAndNext(
		long taxDutyId, java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the tax duties where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of tax duties where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the tax duty where groupId = &#63; or throws a {@link com.liferay.order.NoSuchTaxDutyException} if it could not be found.
	*
	* @param groupId the group ID
	* @return the matching tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty findByGroupId(long groupId)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the tax duty where groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param groupId the group ID
	* @return the matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty fetchByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the tax duty where groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param groupId the group ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching tax duty, or <code>null</code> if a matching tax duty could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty fetchByGroupId(long groupId,
		boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the tax duty where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	* @return the tax duty that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty removeByGroupId(long groupId)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of tax duties where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching tax duties
	* @throws SystemException if a system exception occurred
	*/
	public int countByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the tax duty in the entity cache if it is enabled.
	*
	* @param taxDuty the tax duty
	*/
	public void cacheResult(com.liferay.order.model.TaxDuty taxDuty);

	/**
	* Caches the tax duties in the entity cache if it is enabled.
	*
	* @param taxDuties the tax duties
	*/
	public void cacheResult(
		java.util.List<com.liferay.order.model.TaxDuty> taxDuties);

	/**
	* Creates a new tax duty with the primary key. Does not add the tax duty to the database.
	*
	* @param taxDutyId the primary key for the new tax duty
	* @return the new tax duty
	*/
	public com.liferay.order.model.TaxDuty create(long taxDutyId);

	/**
	* Removes the tax duty with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param taxDutyId the primary key of the tax duty
	* @return the tax duty that was removed
	* @throws com.liferay.order.NoSuchTaxDutyException if a tax duty with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty remove(long taxDutyId)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.liferay.order.model.TaxDuty updateImpl(
		com.liferay.order.model.TaxDuty taxDuty)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the tax duty with the primary key or throws a {@link com.liferay.order.NoSuchTaxDutyException} if it could not be found.
	*
	* @param taxDutyId the primary key of the tax duty
	* @return the tax duty
	* @throws com.liferay.order.NoSuchTaxDutyException if a tax duty with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty findByPrimaryKey(long taxDutyId)
		throws com.liferay.order.NoSuchTaxDutyException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the tax duty with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param taxDutyId the primary key of the tax duty
	* @return the tax duty, or <code>null</code> if a tax duty with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.order.model.TaxDuty fetchByPrimaryKey(long taxDutyId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the tax duties.
	*
	* @return the tax duties
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.TaxDuty> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the tax duties.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tax duties
	* @param end the upper bound of the range of tax duties (not inclusive)
	* @return the range of tax duties
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.TaxDuty> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the tax duties.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferay.order.model.impl.TaxDutyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of tax duties
	* @param end the upper bound of the range of tax duties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of tax duties
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.order.model.TaxDuty> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the tax duties from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of tax duties.
	*
	* @return the number of tax duties
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}