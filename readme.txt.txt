Sales-Taxes Coding Exercise

Author: Joshua Cords

This project is a 3-day prototype Liferay Portlet for an Order-Inventory Portlet. It has most key functionality for acting as a cart. There are 4 Portlets: CategoryControl, InventoryControl, TaxDuty, and Order.

Import this project into a liferay Plugin as a portlet.

TODO -for full functionality these items should be implemented/refactored:
-Session Errors should be created when Exceptions are hit
-Permissions should be implemented
-Input Validation
-Better MVC Structure in TaxDuty Portlet
-Using better structures for money