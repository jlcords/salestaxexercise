<%@include file="/html/init.jsp"%>
<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>

<%
	ResultRow row = (ResultRow) request
			.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	InventoryItem item = (InventoryItem) row.getObject();

	long groupId = item.getGroupId();
	String name = InventoryItem.class.getName();
	long inventoryItemId = item.getInventoryItemId();

	String redirect = PortalUtil.getCurrentURL(renderRequest);
%>

<liferay-ui:icon-menu>

	<portlet:renderURL var="editURL">
		<portlet:param name="mvcPath" value="/html/inventorycontrol/edit_inventory_item.jsp" />
		<portlet:param name="inventoryItemId" value="<%= String.valueOf(inventoryItemId) %>" />
		<portlet:param name="redirect" value="<%= redirect %>" />
	</portlet:renderURL>

	<liferay-ui:icon image="edit" url="<%=editURL.toString() %>" />

	<portlet:actionURL name="deleteInventoryItem" var="deleteInventoryItemURL">
		<portlet:param name="inventoryItemId" value="<%= String.valueOf(inventoryItemId) %>" />
		<portlet:param name="redirect" value="<%= redirect %>" />
	</portlet:actionURL>

	<liferay-ui:icon image="delete" url="<%=deleteInventoryItemURL.toString()%>" />
	
</liferay-ui:icon-menu>