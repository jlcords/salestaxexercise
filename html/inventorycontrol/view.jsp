<%@include file="/html/init.jsp"%>

<%
	String redirect = PortalUtil.getCurrentURL(renderRequest);
%>

<portlet:renderURL var="addInventoryItemURL">
	<portlet:param name="mvcPath" value="/html/inventorycontrol/edit_inventory_item.jsp" />
	<portlet:param name="redirect" value="<%=redirect%>" />
</portlet:renderURL>

<aui:button key="add-inventory-item" value="add-inventory-item" onClick='${addInventoryItemURL.toString()}'></aui:button>

<aui:layout>
	<aui:column width="100" id="inventory-items-dynamic-content">
		<liferay-ui:search-container
			emptyResultsMessage="inventory-empty-results-message">
			<liferay-ui:search-container-results
				results="<%= InventoryItemLocalServiceUtil
					.getInventoryItemsByGroupId(
							scopeGroupId, searchContainer.getStart(), 
							searchContainer.getEnd()) %>"
				total="<%= InventoryItemLocalServiceUtil
					.getInventoryItemsCountByGroupId(scopeGroupId) %>" />
	
			<liferay-ui:search-container-row
				className="com.liferay.order.model.InventoryItem"
				keyProperty="inventoryItemId" modelVar="item" 
				escapedModel="<%=true%>">

				<liferay-ui:search-container-column-text name="name"
					property="name" />

				<liferay-ui:search-container-column-text name="quantity"
					property="quantity" />

				<liferay-ui:search-container-column-text name="basePrice"
					value="<%=dollarFormat.format(item.getBasePrice())%>" />
				
				<liferay-ui:search-container-column-text name="categoryId"
					value="<%=CategoryLocalServiceUtil.getCategory(item.getCategoryId()).getName()%>" />
					
				<liferay-ui:search-container-column-text name="imported"
					property="imported" />
					
				<liferay-ui:search-container-column-jsp align="right"
						path="/html/inventorycontrol/inventory_item_actions.jsp" />

			</liferay-ui:search-container-row>
			<liferay-ui:search-iterator />
		</liferay-ui:search-container>
	</aui:column>
</aui:layout>