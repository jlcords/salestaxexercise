<%@include file="/html/init.jsp"%>

<%
InventoryItem item = null;

long inventoryItemId = ParamUtil.getLong(request, "inventoryItemId");

if (inventoryItemId > 0) {
	item = InventoryItemLocalServiceUtil.getInventoryItem(inventoryItemId);
}

List<Category> categories = null;
Category originalCategory = null;
Category defaultCategory = null;

int numCategories = CategoryLocalServiceUtil.getCategoriesCount();
long categoryId = ParamUtil.getLong(request, "categoryId");
long orderId = ParamUtil.getLong(request, "orderId", 0);

if(categoryId > 0){
	originalCategory = CategoryLocalServiceUtil.getCategory(categoryId);
}

if (numCategories > 0) {
	categories = CategoryLocalServiceUtil.getCategories(0,numCategories);
	defaultCategory = categories.get(0);
}
%>
<portlet:renderURL var="viewInventoryItemURL" />

<liferay-ui:header backURL='<%= viewInventoryItemURL %>'
	title='<%= item == null ? "add-item-to-inventory" : "edit-inventory-item"%>' />

<portlet:actionURL name='<%= item == null ? "addInventoryItem" : "updateInventoryItem" %>' 
	var="addInventoryItem" windowState="normal" />

<aui:model-context bean="<%= item %>" model="<%= InventoryItem.class %>" />
<aui:form action="${addInventoryItem}" method="POST" name="fm" >
	<aui:input name="companyId" value="<%= themeDisplay.getCompanyGroupId() %>" type="hidden" />
	<aui:input name="groupId" value="<%= scopeGroupId %>" type="hidden" />
	<aui:input name="userId" value="<%= user.getUserId() %>" type="hidden" />
	<aui:input name="inventoryItemId" type="hidden" />
	<aui:input name="name" inline="true" />
	<aui:input name="quantity" inline="true" />
	<aui:input name="basePrice" inline="true" />
	<aui:select name="categoryId">
	<% 
		for(Category category: categories){
	%>
		<aui:option value='<%= category.getCategoryId() %>'><%= category.getName() %></aui:option>
	<%
		}
	%>
	
	</aui:select>
	<aui:input name="imported" type="checkbox" value='<%= item != null && item.getImported() %>' inline="true" />
	<aui:button type="submit" />
</aui:form>