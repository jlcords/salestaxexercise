<%@include file="/html/init.jsp"%>

<%
	String redirect = PortalUtil.getCurrentURL(renderRequest);
%>

<portlet:renderURL var="addCategoryURL">
	<portlet:param name="mvcPath" value="/html/categorycontrol/edit_category.jsp" />
	<portlet:param name="redirect" value="<%=redirect%>" />
</portlet:renderURL>

<aui:button key="add-category" value="add-category" onClick='${addCategoryURL.toString()}'></aui:button>

<aui:layout>
	<aui:column width="100" id="categories-dynamic-content">
		<liferay-ui:search-container
			emptyResultsMessage="category-empty-results-message">
			<liferay-ui:search-container-results
				results="<%= CategoryLocalServiceUtil.getCategoriesByGroupId(
						scopeGroupId, searchContainer.getStart(),
						searchContainer.getEnd()) %>"
				total="<%= CategoryLocalServiceUtil.getCategoriesCountByGroupId(
						scopeGroupId) %>" />
	
			<liferay-ui:search-container-row
				className="com.liferay.order.model.Category"
				keyProperty="categoryId" modelVar="category" escapedModel="<%=true%>">

				<liferay-ui:search-container-column-text name="name"
					property="name" />
				
				<liferay-ui:search-container-column-text name="taxable"
					property="taxable" />
					
				<liferay-ui:search-container-column-jsp align="right"
						path="/html/categorycontrol/category_actions.jsp" />

			</liferay-ui:search-container-row>

			<liferay-ui:search-iterator />
		</liferay-ui:search-container>
	</aui:column>
</aui:layout>