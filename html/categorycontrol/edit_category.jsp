<%@include file="/html/init.jsp"%>

<%
Category category = null;

long categoryId = ParamUtil.getLong(request, "categoryId");

if (categoryId > 0) {
	category = CategoryLocalServiceUtil.getCategory(categoryId);
}

%>
<portlet:renderURL var="viewCategoryURL" />

<liferay-ui:header backURL='<%= viewCategoryURL %>'
	title='<%= category == null ? "add-category" : "edit-category"%>' />

<portlet:actionURL name='<%= category == null ? "addCategory" : "updateCategory" %>' 
	var="addCategory" windowState="normal" />

<aui:model-context bean="<%= category %>" model="<%= Category.class %>" />
<aui:form action="${addCategory}" method="POST" name="fm" >
	<aui:input name="companyId" value="<%= themeDisplay.getCompanyGroupId() %>" type="hidden" />
	<aui:input name="groupId" value="<%= scopeGroupId %>" type="hidden" />
	<aui:input name="categoryId" type="hidden" />
	<aui:input name="name" inline="true" />
	<aui:input name="taxable" type="checkbox" value='<%= category != null && category.getTaxable() %>' inline="true" />
	<aui:button type="submit" />
</aui:form>