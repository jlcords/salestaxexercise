<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@include file="/html/init.jsp"%>

<%
	ResultRow row = (ResultRow) request
			.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Category category = (Category) row.getObject();

	long groupId = category.getGroupId();
	String name = Category.class.getName();
	long categoryId = category.getCategoryId();

	String redirect = PortalUtil.getCurrentURL(renderRequest);
%>

<liferay-ui:icon-menu>

	<portlet:renderURL var="editURL">
		<portlet:param name="mvcPath" value="/html/categorycontrol/edit_category.jsp" />
		<portlet:param name="categoryId" value="<%= String.valueOf(categoryId) %>" />
		<portlet:param name="redirect" value="<%= redirect %>" />
	</portlet:renderURL>

	<liferay-ui:icon image="edit" url="<%=editURL.toString() %>" />

	<portlet:actionURL name="deleteCategory" var="deleteCategoryURL">
		<portlet:param name="categoryId" value="<%= String.valueOf(categoryId) %>" />
		<portlet:param name="redirect" value="<%= redirect %>" />
	</portlet:actionURL>

	<liferay-ui:icon image="delete" url="<%=deleteCategoryURL.toString()%>" />

</liferay-ui:icon-menu>