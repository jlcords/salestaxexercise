<%@include file="/html/init.jsp"%>
<%@ page import="com.liferay.order.service.TaxDutyLocalServiceUtil" %><%@
page import="com.liferay.order.model.TaxDuty" %>

<%
int tax = ParamUtil.getInteger(request, "tax-percent", 0);
int duty = ParamUtil.getInteger(request, "duty-percent", 0);
int round = ParamUtil.getInteger(request, "round", 0);
long taxDutyId = ParamUtil.getLong(request, "taxDutyId");

TaxDuty taxDuty = null;

//Needs to be refactored
try{
	taxDuty = TaxDutyLocalServiceUtil.getTaxDutybyGroupId(scopeGroupId);
}catch(Exception e){
	taxDuty = TaxDutyLocalServiceUtil.addTaxDuty(
			themeDisplay.getCompanyGroupId(), scopeGroupId, 10, 5, 5);
}

String redirect = PortalUtil.getCurrentURL(renderRequest);

%>
<portlet:renderURL var="viewOrdersURL" />

<liferay-ui:header title="tax-duty" />

<portlet:actionURL name="updateTaxDuty" 
	var="updateTaxDuty" windowState="normal" />
	
<aui:form action="${updateTaxDuty}" method="POST" name="fm" >
	<aui:input name="companyId" value="<%= themeDisplay.getCompanyGroupId() %>" type="hidden" />
	<aui:input name="groupId" value="<%= scopeGroupId %>" type="hidden" />
	<aui:input name="taxDutyId" value="<%= taxDuty.getTaxDutyId() %>" type="hidden" />
	<aui:input name="tax-percent" value="<%= taxDuty.getTax() %>" />
	<aui:input name="duty-percent" value="<%= taxDuty.getDuty() %>" />
	<aui:input name="round" value="<%= taxDuty.getRound() %>" />
	<aui:button type="submit" value="Update" />
</aui:form>