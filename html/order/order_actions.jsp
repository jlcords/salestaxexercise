<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@include file="/html/init.jsp"%>

<%
	ResultRow row = (ResultRow) request
			.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	FinalItem item = (FinalItem) row.getObject();

	long groupId = item.getGroupId();
	String name = FinalItem.class.getName();
	long finalItemId = item.getFinalItemId();

	String redirect = PortalUtil.getCurrentURL(renderRequest);
%>

<liferay-ui:icon-menu>

	<portlet:renderURL var="editURL">
		<portlet:param name="mvcPath" value="/html/order/choose_item.jsp" />
		<portlet:param name="finalItemId" value="<%= String.valueOf(finalItemId) %>" />
		<portlet:param name="redirect" value="<%= redirect %>" />
	</portlet:renderURL>

	<liferay-ui:icon image="edit" url="<%=editURL.toString() %>" />

	<portlet:actionURL name="deleteFinalItem" var="deleteFinalItemURL">
		<portlet:param name="inventoryItemId" value="<%= String.valueOf(finalItemId) %>" />
		<portlet:param name="redirect" value="<%= redirect %>" />
	</portlet:actionURL>

	<liferay-ui:icon image="delete" url="<%=deleteFinalItemURL.toString()%>" />
	


</liferay-ui:icon-menu>
