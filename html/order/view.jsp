<%@include file="/html/init.jsp"%>


<%
	String redirect = PortalUtil.getCurrentURL(renderRequest);
%>
<portlet:renderURL var="addOrderURL">
	<portlet:param name="mvcPath" value="/html/order/edit_order.jsp" />
	<portlet:param name="redirect" value="<%=redirect%>" />
</portlet:renderURL>

<aui:button key="add-order" value="create-new-order"
	onClick='${addOrderURL.toString()}'></aui:button>

<aui:layout>
	<aui:column width="100" id="orders-dynamic-content">
		<liferay-ui:search-container
			emptyResultsMessage="order-empty-results-message">
			<liferay-ui:search-container-results
				results="<%= OrderLocalServiceUtil.getOrdersByUserId(user.getUserId(), searchContainer.getStart(), searchContainer.getEnd()) %>"
				total="<%= OrderLocalServiceUtil.getOrdersCountByUserId(user.getUserId()) %>" />
	
			<liferay-ui:search-container-row
				className="com.liferay.order.model.Order"
				keyProperty="orderId" modelVar="order" escapedModel="<%=true%>">

				<portlet:renderURL windowState="maximized" var="rowURL">
					<portlet:param name="mvcPath" value="/html/order/edit_order.jsp" />
					<portlet:param name="orderId"
						value="<%=String.valueOf(order.getOrderId())%>" />
				</portlet:renderURL>

				<liferay-ui:search-container-column-text name="OrderId"
					value="<%=Long.toString(order.getOrderId())%>" href="<%= rowURL %>" />
					
				<liferay-ui:search-container-column-text name="Date"
					value="<%=order.getCreateDate().toString()%>" />

			</liferay-ui:search-container-row>

			<liferay-ui:search-iterator />
		</liferay-ui:search-container>
	</aui:column>
</aui:layout>