<%@include file="/html/init.jsp"%>


<%
List<InventoryItem> items = null;
InventoryItem originalItem = null;
InventoryItem defaultItem = null;

double defaultPrice = 0;
String defaultCategory = "";
boolean defaultImported = false;


int numItems = InventoryItemLocalServiceUtil.getInventoryItemsCount();
long inventoryItemId = ParamUtil.getLong(request, "inventoryItemId");
long orderId = ParamUtil.getLong(request, "orderId", 0);

if(inventoryItemId > 0){
	originalItem = InventoryItemLocalServiceUtil.getInventoryItem(inventoryItemId);
}

if (numItems > 0) {
	items = InventoryItemLocalServiceUtil.getInventoryItems(0,numItems);
	defaultItem = items.get(0);
	defaultPrice = defaultItem.getBasePrice();
	defaultCategory = CategoryLocalServiceUtil.getCategory(defaultItem.getCategoryId()).getName();
	defaultImported = defaultItem.getImported();
}

%>

<h3><liferay-ui:message key='<%= originalItem == null ? "add-item-to-inventory" : "edit-inventory-item"%>' /></h3>
<portlet:actionURL name='<%= originalItem == null ? "addFinalItem" : "updateFinalItem" %>' 
	var="addItem" windowState="normal" />
	
<aui:form action="${addItem}" method="POST" name="fm" >
	<aui:input name="companyId" value="<%= themeDisplay.getCompanyGroupId() %>" type="hidden" />
	<aui:input name="groupId" value="<%= scopeGroupId %>" type="hidden" />
	<aui:input name="userId" value="<%= user.getUserId() %>" type="hidden" />
	<aui:input name="orderId" value="<%= orderId %>" type="hidden" />
	
	<aui:select name="itemList" id="itemList">
	<%
	for(InventoryItem item: items){
	%>
		<aui:option value='<%= item.getInventoryItemId() %>'><%= item.getName() %></aui:option>
	
	<%
	}
	%>
	</aui:select>
	
	<aui:input readonly="true" name="price" id="price" type="text" value="<%= dollarFormat.format(defaultPrice) %>" />
	<aui:input readonly="true" name="category" id="category" type="text" value="<%= defaultCategory %>" />
	<aui:input disabled="true" name="imported" type="checkbox" id="imported" value="<%= defaultImported %>" />
	
	<aui:input name="quantity" id="quantity" type="text" value="1" />
	
	<aui:button type="submit" name="submit" />
</aui:form>
<portlet:resourceURL var="resourceURL" ></portlet:resourceURL>
<aui:script>


AUI().use('aui-base','aui-io-request','aui-node', function(A){
    A.one("#<portlet:namespace/>itemList").on('change',function(){
    	
    	var inventoryItemId = A.one("#<portlet:namespace/>itemList").val();
        
        A.io.request('<%=resourceURL%>', //requesting call to serverResource method 
                {
            		method: 'POST',
		             data: {<portlet:namespace/>input:inventoryItemId},
		
		             dataType: 'json',
		             on: {
		            	 success: function() {
		               		var item =this.get('responseData');
		               		A.one('#<portlet:namespace />price').empty();
		               		A.one('#<portlet:namespace />category').empty();
		               		A.one('#<portlet:namespace />imported').empty();
		               		
		               		A.one('#<portlet:namespace />price').set('value', item.price);
		               		A.one('#<portlet:namespace />category').set('value', item.category);
		               		A.one('#<portlet:namespace />importedCheckbox').set('checked', item.imported);
							
	          				}
	            	}
            
                });
            });
});
    

</aui:script>