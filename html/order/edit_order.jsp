<%@include file="/html/init.jsp"%>



<%
Order order = null;

long orderId = ParamUtil.getLong(request, "orderId", 0);
double taxSum = 0;
double dutySum = 0;
double totalPrice = 0;

if (orderId > 0) {
	order = OrderLocalServiceUtil.getOrder(orderId);
	taxSum = FinalItemLocalServiceUtil.getTaxSumbyOrderId(orderId);
	dutySum = FinalItemLocalServiceUtil.getDutySumbyOrderId(orderId);
	totalPrice = FinalItemLocalServiceUtil.getTotalPriceSumbyOrderId(orderId);

}else{
	order = OrderLocalServiceUtil.addOrder(themeDisplay.getCompanyGroupId(),
			scopeGroupId, user.getUserId(), false);
	orderId = order.getOrderId();
}

String redirect = PortalUtil.getCurrentURL(renderRequest);

%>
<portlet:renderURL var="viewOrdersURL" />

<liferay-ui:header backURL='<%= viewOrdersURL %>'
	title="edit-order" />

<liferay-portlet:renderURL var="addItemURL" >
	<portlet:param name="mvcPath" value="/html/order/choose_item.jsp" />
	<portlet:param name="orderId" value="<%=Long.toString(order.getOrderId())%>" />
	<portlet:param name="redirect" value="<%=redirect%>" />
</liferay-portlet:renderURL>

<aui:button key="add-item" value="add-item" name="add-item" onClick='${addItemURL.toString()}' ></aui:button>

<aui:layout>
	<aui:column width="100" id="order-items-dynamic-content">
		<liferay-ui:search-container
			emptyResultsMessage="order-items-empty-results-message">
			<liferay-ui:search-container-results
				results="<%= FinalItemLocalServiceUtil.getFinalItemsbyOrderId(orderId, searchContainer.getStart(), searchContainer.getEnd()) %>"
				total="<%= FinalItemLocalServiceUtil.getFinalItemsCountbyOrderId(orderId) %>" />
	
			<liferay-ui:search-container-row
				className="com.liferay.order.model.FinalItem"
				keyProperty="name" modelVar="finalItem" escapedModel="<%=true%>">

				<liferay-ui:search-container-column-text name="Name"
					property="name"  />
					
				<liferay-ui:search-container-column-text name="quantity"
					property="quantity" />

				<liferay-ui:search-container-column-text name="basePrice"
					value='<%= dollarFormat.format(finalItem.getBasePrice()) %>' />
				
				<liferay-ui:search-container-column-text name="categoryId"
					value="<%=CategoryLocalServiceUtil.getCategory(finalItem.getCategoryId()).getName()%>" />
					
				<liferay-ui:search-container-column-text name="imported"
					property="imported" />
					
				<liferay-ui:search-container-column-text name="tax"
					value='<%= dollarFormat.format(finalItem.getTax()) %>' />
					
				<liferay-ui:search-container-column-text name="duty"
					value='<%= dollarFormat.format(finalItem.getDuty()) %>' />
					
				<liferay-ui:search-container-column-text name="totalPrice"
					value='<%= dollarFormat.format(finalItem.getTotalPrice()) %>' />

			</liferay-ui:search-container-row>

			<liferay-ui:search-iterator />
		</liferay-ui:search-container>
	</aui:column>
</aui:layout>

<h3>
&nbsp;&nbsp;<liferay-ui:message key="tax" /> <%= dollarFormat.format(taxSum) %>&nbsp;&nbsp;|&nbsp;
<liferay-ui:message key="duty" /> <%= dollarFormat.format(dutySum) %>&nbsp;&nbsp;|&nbsp;
<liferay-ui:message key="total" /> <%= dollarFormat.format(totalPrice) %>
</h3>

<portlet:actionURL name='confirmOrder' 
	var="confirmOrderURL" windowState="normal" />

<aui:model-context bean="<%= order %>" model="<%= Order.class %>" />
<aui:form action="${confirmOrderURL}" method="POST" name="fm" >
	<aui:input name="companyId" value="<%= themeDisplay.getCompanyGroupId() %>" type="hidden" />
	<aui:input name="groupId" value="<%= scopeGroupId %>" type="hidden" />
	<aui:input name="userId" value="<%= user.getUserId() %>" type="hidden" />
	<aui:input name="confirmed" value="true" type="hidden" />
	<aui:input name="orderId" type="hidden" />
	<aui:button type="submit" value="confirm-order"/>
</aui:form>
