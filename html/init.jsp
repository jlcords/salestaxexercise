<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ddm" prefix="liferay-ddm" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/security" prefix="liferay-security" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<%@ 
page import="java.text.DecimalFormat" %><%@
page import="java.util.List" %>


<%@
page import="com.liferay.portal.kernel.portlet.LiferayWindowState" %><%@ 
page import="com.liferay.portal.kernel.util.ParamUtil" %><%@ 
page import="com.liferay.portal.kernel.util.WebKeys" %><%@
page import="com.liferay.portal.theme.ThemeDisplay" %><%@
page import="com.liferay.portal.util.PortalUtil" %><%@
page import="com.liferay.order.model.Category" %><%@
page import="com.liferay.order.model.FinalItem" %><%@
page import="com.liferay.order.model.InventoryItem" %><%@
page import="com.liferay.order.model.Order" %><%@ 
page import="com.liferay.order.service.CategoryLocalServiceUtil" %><%@ 
page import="com.liferay.order.service.FinalItemLocalServiceUtil" %><%@ 
page import="com.liferay.order.service.InventoryItemLocalServiceUtil" %><%@ 
page import="com.liferay.order.service.OrderLocalServiceUtil" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />
<%
DecimalFormat dollarFormat = new DecimalFormat("$#,###,###.00");
%>